package com.yao.dao;

import com.yao.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ruijie on 2017/12/16.
 */
@Repository
public interface ReaderRepository extends JpaRepository<Book,Long> {

    List<Book> findByReader(String reader);

}
