package com.yao.controller;

import com.yao.config.properties.AmazonProperties;
import com.yao.dao.ReaderRepository;
import com.yao.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by ruijie on 2017/12/16.
 */
@Controller
@RequestMapping("/")
public class ReadingListController {
    @Autowired
    private ReaderRepository readingListRepository;

    @Autowired
    private AmazonProperties amazonProperties;

    @GetMapping("/{reader}")
    public String readersBooks(@PathVariable String reader, Model model){
        List<Book> readingList = readingListRepository.findByReader(reader);
        model.addAttribute("books",readingList);
        return "readingList";
    }

    @PostMapping("/{reader}")
    public String addToReadingList(@PathVariable String reader,Book book){
        book.setReader(reader);
        readingListRepository.save(book);
        return "redirect:/{reader}";
    }

    @GetMapping("/toReadingList")
    public String toReadingList(){
        return "readingList";
    }
}
