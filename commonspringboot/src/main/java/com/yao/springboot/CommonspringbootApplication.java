package com.yao.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommonspringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(CommonspringbootApplication.class, args);
	}
}
