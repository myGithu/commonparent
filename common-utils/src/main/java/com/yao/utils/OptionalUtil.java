package com.yao.utils;

import java.util.Optional;

/**
 * Created by ruijie on 2017/12/1.
 */
public class OptionalUtil {

    public static Optional<Integer> stringToInt(String value) {
        try {
            return Optional.ofNullable(Integer.valueOf(value));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }

}
