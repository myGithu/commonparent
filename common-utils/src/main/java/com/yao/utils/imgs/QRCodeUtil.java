//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yao.utils.imgs;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.thoughtworks.xstream.core.util.Base64Encoder;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;

public class QRCodeUtil {
    private static final int BLACK = -16777216;
    private static final int WHITE = -1;

    private QRCodeUtil() {
    }

    private static Map<EncodeHintType, Object> getHints() {
        Map<EncodeHintType, Object> hints = new HashMap();
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        hints.put(EncodeHintType.MARGIN, 2);
        return hints;
    }

    public static BufferedImage toBufferedImage(BitMatrix bitMatrix) {
        int width = bitMatrix.getWidth();
        int height = bitMatrix.getHeight();
        BufferedImage img = new BufferedImage(width, height, 1);

        for(int x = 0; x < width; ++x) {
            for(int y = 0; y < height; ++y) {
                img.setRGB(x, y, bitMatrix.get(x, y) ? -16777216 : -1);
            }
        }

        return img;
    }

    public static String image2String(BufferedImage imgPath) {
        ByteArrayOutputStream arrayOutputStream = null;

        try {
            arrayOutputStream = new ByteArrayOutputStream();
            ImageIO.write(imgPath, "jpg", arrayOutputStream);
        } catch (IOException var3) {
            var3.printStackTrace();
        }

        Base64Encoder encoder = new Base64Encoder();
        return encoder.encode(arrayOutputStream.toByteArray());
    }

    public static String getBase64QRCode(int width, int height, String contents) throws WriterException {
        BitMatrix bitMatrix = (new MultiFormatWriter()).encode(contents, BarcodeFormat.QR_CODE, width, height, getHints());
        BufferedImage img = MatrixToImageWriter.toBufferedImage(bitMatrix);
        return image2String(img);
    }

    public static void createImageQRCodeToLocalPath(int width, int height, String contents, String path, String name) throws WriterException, IOException {
        BitMatrix bitMatrix = (new MultiFormatWriter()).encode(contents, BarcodeFormat.QR_CODE, width, height, getHints());
        Path path2 = (new File(path, name + ".jpg")).toPath();
        MatrixToImageWriter.writeToPath(bitMatrix, "jpg", path2);
    }

    public static void main(String[] args) {
        try {
            System.out.println(getBase64QRCode(400, 400, "nihao"));
        } catch (WriterException var2) {
            var2.printStackTrace();
        }

    }
}
