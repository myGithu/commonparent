//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yao.utils.config.tenconfig;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class TenConfig {
    public TenConfig() {
    }

    public static String getTenLoginCode() throws UnsupportedEncodingException {
        String backUrl = "http://www.shilesoft.com/common-web/toAuthorize";
        backUrl = URLEncoder.encode(backUrl, "UTF-8");
        String url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxb68e540f341931d4&redirect_uri=" + backUrl + "&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect";
        return url;
    }

    public static void main(String[] args) {
        try {
            System.out.println(getTenLoginCode());
        } catch (UnsupportedEncodingException var2) {
            var2.printStackTrace();
        }

    }
}
