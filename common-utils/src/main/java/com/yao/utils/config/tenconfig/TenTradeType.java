//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yao.utils.config.tenconfig;

public enum TenTradeType {
    JSAPI,
    NATIVE,
    APP;

    private TenTradeType() {
    }
}
