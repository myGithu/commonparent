//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yao.utils.xml;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.core.util.QuickWriter;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.XppDriver;
import com.yao.utils.annotation.XStreamCDATA;
import java.io.Writer;
import java.lang.reflect.Field;

public class XStreamUtil {
    public XStreamUtil() {
    }

    public static <T> String getWXPayXStreamXmlStr(T t) {
        XStream xStream = new XStream(new XppDriver() {
            public HierarchicalStreamWriter createWriter(Writer out) {
                return new PrettyPrintWriter(out) {
                    boolean cdata = false;
                    Class<?> targetClass = null;

                    public void startNode(String name, Class clazz) {
                        super.startNode(name, clazz);
                        if (!name.equals("xml")) {
                            this.cdata = XStreamUtil.needCDATA(this.targetClass, name);
                        } else {
                            this.targetClass = clazz;
                        }

                    }

                    protected void writeText(QuickWriter writer, String text) {
                        if (this.cdata) {
                            writer.write(this.cDATA(text));
                        } else {
                            writer.write(text);
                        }

                    }

                    private String cDATA(String text) {
                        text = "<![CDATA[" + text + "]]>";
                        return text;
                    }
                };
            }
        });
        xStream.processAnnotations(t.getClass());
        return xStream.toXML(t).toString().replace("__", "_");
    }

    private static boolean needCDATA(Class<?> targetClass, String fieldAlias) {
        boolean cdata = false;
        cdata = existsCDATA(targetClass, fieldAlias);
        if (cdata) {
            return cdata;
        } else {
            for(Class superClass = targetClass.getSuperclass(); !superClass.equals(Object.class); superClass = superClass.getSuperclass()) {
                cdata = existsCDATA(superClass, fieldAlias);
                if (cdata) {
                    return cdata;
                }
            }

            return false;
        }
    }

    private static boolean existsCDATA(Class<?> clazz, String fieldAlias) {
        Field[] fields = clazz.getDeclaredFields();
        Field[] var3 = fields;
        int var4 = fields.length;

        for(int var5 = 0; var5 < var4; ++var5) {
            Field field = var3[var5];
            if (field.getAnnotation(XStreamCDATA.class) != null) {
                XStreamAlias xStreamAlias = (XStreamAlias)field.getAnnotation(XStreamAlias.class);
                if (null != xStreamAlias) {
                    if (fieldAlias.equals(xStreamAlias.value())) {
                        return true;
                    }
                } else if (fieldAlias.equals(field.getName())) {
                    return true;
                }
            }
        }

        return false;
    }

    public static <T> T getParseXml(String resXml, T t) {
        XStream xStream = new XStream();
        xStream.alias("t", t.getClass());
        t = (T) xStream.fromXML(resXml);
        return t;
    }

    public static <T> T getParseXml(String xmlStr, Class<T> cls) {
        XStream xStream = new XStream(new DomDriver());
        xStream.processAnnotations(cls);
        T obj = (T) xStream.fromXML(xmlStr);
        return obj;
    }
}
