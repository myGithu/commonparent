//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yao.utils.algorithms;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.StreamUtil;
import com.alipay.api.internal.util.StringUtils;
import com.alipay.api.internal.util.codec.Base64;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;

public class EncryptionAlgorithmUtil {
    public static final String MD5 = "MD5";
    public static final String SHA1 = "SHA1";
    public static final String API_KEY = "desc";
    private static final char[] HEXDIGITS = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public EncryptionAlgorithmUtil() {
    }

    public String getEncryptionAlgorithm(String key, String type) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(type);
            byte[] inputByteArray = key.getBytes();
            messageDigest.update(inputByteArray);
            byte[] resultByteArray = messageDigest.digest();
            return this.byteArrayToHex(resultByteArray);
        } catch (NoSuchAlgorithmException var6) {
            return null;
        }
    }

    public String byteArrayToHex(byte[] byteArray) {
        char[] resultCharArray = new char[byteArray.length * 2];
        int index = 0;
        byte[] var4 = byteArray;
        int var5 = byteArray.length;

        for(int var6 = 0; var6 < var5; ++var6) {
            byte b = var4[var6];
            resultCharArray[index++] = HEXDIGITS[b >>> 4 & 15];
            resultCharArray[index++] = HEXDIGITS[b & 15];
        }

        return new String(resultCharArray);
    }

    public byte[] hexToByteArray(String hexString) {
        if (!hexString.trim().isEmpty() && null != hexString) {
            hexString = hexString.toUpperCase();
            int len = hexString.length() / 2;
            char[] hexChars = hexString.toCharArray();
            byte[] bytes = new byte[len];

            for(int i = 0; i < len; ++i) {
                int pos = i * 2;
                bytes[i] = (byte)(this.charToByte(hexChars[pos]) << 4 | this.charToByte(hexChars[pos + 1]));
            }

            return bytes;
        } else {
            return null;
        }
    }

    private byte charToByte(char ch) {
        return (byte)HEXDIGITS.toString().indexOf(ch);
    }

    public String getConvert(String data, String apiKey) {
        char[] datas = data.toCharArray();
        char[] apiKeys = apiKey.toCharArray();
        int i = 0;

        for(int j = datas.length; i < j; ++i) {
            int k = 0;

            for(int l = apiKeys.length; k < l; ++k) {
                datas[i] ^= apiKeys[k];
            }
        }

        return new String(datas);
    }

    public static String rsa256Sign(String content, String privateKey, String charset) throws AlipayApiException {
        try {
            PrivateKey priKey = getPrivateKeyFromPKCS8("RSA", new ByteArrayInputStream(privateKey.getBytes()));
            Signature signature = Signature.getInstance("SHA256WithRSA");
            signature.initSign(priKey);
            if (StringUtils.isEmpty(charset)) {
                signature.update(content.getBytes());
            } else {
                signature.update(content.getBytes(charset));
            }

            byte[] signed = signature.sign();
            return new String(Base64.encodeBase64(signed));
        } catch (Exception var6) {
            throw new AlipayApiException("RSAcontent = " + content + "; charset = " + charset, var6);
        }
    }

    public static PrivateKey getPrivateKeyFromPKCS8(String algorithm, InputStream ins) throws Exception {
        if (ins != null && !StringUtils.isEmpty(algorithm)) {
            KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
            byte[] encodedKey = StreamUtil.readText(ins).getBytes();
            encodedKey = Base64.decodeBase64(encodedKey);
            return keyFactory.generatePrivate(new PKCS8EncodedKeySpec(encodedKey));
        } else {
            return null;
        }
    }

    public static void main(String[] args) {
    }
}
