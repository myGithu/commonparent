//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yao.utils.str;

import java.util.regex.Pattern;

public class RegexMatchesUtil {
    static final String REG_PATTERN_EMAIL = "^[a-zA-z0-9-_]+@[a-zA-z0-9-_]+[\\.]+[a-zA-z0-9-_]$";
    static final String REG_PATTERN__PHONE = "^1[3,4,5,7,8]\\d{9}$";

    private RegexMatchesUtil() {
    }

    public static boolean checkEmail(String email) {
        return Pattern.matches("^[a-zA-z0-9-_]+@[a-zA-z0-9-_]+[\\.]+[a-zA-z0-9-_]$", email);
    }

    public static boolean checkPhone(String phone) {
        return Pattern.matches("^1[3,4,5,7,8]\\d{9}$", phone);
    }

    public static void main(String[] args) {
        System.out.println(checkPhone("13974557306"));
    }
}
