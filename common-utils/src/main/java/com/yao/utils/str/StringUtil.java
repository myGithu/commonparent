//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yao.utils.str;

import java.util.Random;

public class StringUtil {
    private StringUtil() {
    }

    public static boolean isNumber(String str) {
        char[] ch = str.toCharArray();
        int i = 0;

        for(int j = ch.length; i < j; ++i) {
            if (!Character.isDigit(ch[i])) {
                return false;
            }
        }

        return true;
    }

    public static String getRandomString(int num) {
        String RANDOM_STRING = "QAZXSWEDCVFRTGBNHYUJMKIOLP1234567890qwertyuioplkjhgfdsazxcvbnm";
        StringBuilder sb = new StringBuilder();
        char[] randomChars = RANDOM_STRING.toCharArray();
        Random random = new Random();
        int len = randomChars.length;

        for(int i = 0; i < num; ++i) {
            sb.append(randomChars[random.nextInt(len)]);
        }

        return sb.toString();
    }

    public static void main(String[] args) {
    }
}
