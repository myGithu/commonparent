package com.yao.mappers;

import com.github.abel533.mapper.Mapper;
import com.yao.pojos.User;

import java.util.List;

/**
 * Created by ruijie on 2017/6/2.
 */
public interface UserMapper extends Mapper<User>{
    @Override
    User selectOne(User user);

    @Override
    List<User> select(User user);

    @Override
    int selectCount(User user);

    @Override
    User selectByPrimaryKey(Object o);

    @Override
    int insert(User user);

    @Override
    int insertSelective(User user);

    @Override
    int delete(User user);

    @Override
    int deleteByPrimaryKey(Object o);

    @Override
    int updateByPrimaryKey(User user);

    @Override
    int updateByPrimaryKeySelective(User user);

    @Override
    int selectCountByExample(Object o);

    @Override
    int deleteByExample(Object o);

    @Override
    List<User> selectByExample(Object o);

    @Override
    int updateByExampleSelective(User user, Object o);

    @Override
    int updateByExample(User user, Object o);
}
