package com.yao.java8.shizhan.diqizhang;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.RecursiveTask;
import java.util.stream.LongStream;

/**
 * Created by ruijie on 2017/11/23.
 */
public class Accumulator {
    public long total = 0;

    public void add(long value) {
        total += value;
    }

    public static long sidEffectSum(long n) {
        Accumulator accumulator = new Accumulator();
        LongStream.rangeClosed(1L, n).forEach(accumulator :: add);
        return accumulator.total;
    }
}
