package com.yao.java8.shizhan.dishierzhang;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Locale;
import java.util.stream.Stream;

/**
 * Created by ruijie on 2017/12/8.
 */
public class Test {
    public static void main(String[] args) {
       /* LocalDate date =  LocalDate.of(2017,12,8);
        int year = date.getYear();
        Month month = date.getMonth();
        DayOfWeek dayOfWeek =  date.getDayOfWeek();
        int len = date.lengthOfMonth();
        boolean leap = date.isLeapYear();
        System.out.println(date + " year :" + year + " Month :" + month
                + " DayOfWeek : " + dayOfWeek + "len :" + len + "leap :" + leap);

        System.out.println(LocalDate.now());

        date.get(ChronoField.YEAR);
        date.get(ChronoField.MONTH_OF_YEAR);
        date.get(ChronoField.DAY_OF_MONTH);

        LocalTime.of(13,45,20);

        LocalTime time =  LocalTime.now();

        time.getHour();
        time.getMinute();
        time.getSecond();

        LocalDateTime dt1 =   LocalDateTime.of(2014,Month.DECEMBER,18,15,56,30);
        System.out.println(dt1);
        LocalDateTime.of(date,time);

        System.out.println(date.atTime(13,45,20));

        System.out.println(Instant.now());

        Period tenDays = Period.between(LocalDate.of(2014, 3, 8),
                LocalDate.of(2014, 3, 18));
        System.out.println(tenDays);

        Duration threeMinutes = Duration.ofMinutes(3);
        System.out.println(threeMinutes);


        LocalDate date1 = LocalDate.of(2014, 3, 18);

        //ChronoField.MONTH_OF_YEAR
        LocalDate date = LocalDate.of(2014, 3, 18);
        date = date.with(ChronoField.MONTH_OF_YEAR, 9);
        date = date.plusYears(2).minusDays(10);
        date =  date.withYear(2011);
        System.out.println(date);

        System.out.println(Instant.now().toEpochMilli());


        //秒和纳秒级别  适用于 LocalDateTime   Instant   LocalTime
        System.out.println(Duration.between(LocalDateTime.MAX,LocalDateTime.MIN));

        //ChronoUnit.YEARS
        System.out.println(Period.between(LocalDate.now(),LocalDate.of(2018,12,12)).get(ChronoUnit.YEARS));
        LocalDate date1 = LocalDate.of(2014, 3, 18);
        LocalDate date2 = date1.withYear(2011);
        LocalDate date3 = date2.withDayOfMonth(25);
        LocalDate date4 = date3.with(ChronoField.MONTH_OF_YEAR, 9);

        LocalDate date = LocalDate.now();
        LocalDate date1 = date.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));
        System.out.println(date1);


        LocalDate date = LocalDate.now();

        String str1 = date.format(DateTimeFormatter.BASIC_ISO_DATE);
        System.out.println(str1);
        String str2 = date.format(DateTimeFormatter.ISO_LOCAL_DATE);
        System.out.println(str2);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        LocalDate date1 = LocalDate.of(2014, 3, 18);
        String formattedDate = date1.format(formatter);
        System.out.println(formattedDate);
        LocalDate date2 = LocalDate.parse(formattedDate, formatter);
        System.out.println(date2);

        DateTimeFormatter italianFormatter = DateTimeFormatter.ofPattern("d. MMMM yyyy", Locale.ITALIAN);
        LocalDate localDate =  LocalDate.of(2017,12,12);
        String formattedDate1 = localDate.format(italianFormatter);
        System.out.println(formattedDate1);


        DateTimeFormatter italianFormatter2 = new DateTimeFormatterBuilder()
                .appendText(ChronoField.DAY_OF_MONTH)
                .appendLiteral(". ")
                .appendText(ChronoField.MONTH_OF_YEAR)
                .appendLiteral(" ")
                .appendText(ChronoField.YEAR)
                .parseCaseInsensitive()
                .toFormatter(Locale.ITALIAN);
         formattedDate1 = localDate.format(italianFormatter2);
        System.out.println(formattedDate1);*/
        ZoneId romeZone = ZoneId.of("Asia/");
        Instant instant = Instant.now();
        ZonedDateTime zdt3 = instant.atZone(romeZone);
        System.out.println(zdt3);
    }
}
