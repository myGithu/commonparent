package com.yao.java8.shizhan.dibazhang;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ruijie on 2017/11/28.
 */
public class Debugging {
    public static void main(String[] args) {
        List<Point> points = Arrays.asList(new Point(12, 2), null);
        points.stream().map(Point::getX).forEach(System.out::println);
    }
}
