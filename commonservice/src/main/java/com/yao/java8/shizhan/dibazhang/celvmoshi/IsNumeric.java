package com.yao.java8.shizhan.dibazhang.celvmoshi;

/**
 * Created by ruijie on 2017/11/25.
 */
public class IsNumeric implements ValidationStrategy {
    @Override
    public boolean execute(String s) {
        return s.matches("\\d+");
    }
}
