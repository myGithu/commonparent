package com.yao.java8.shizhan.diwuzhang;


import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.yao.java8.shizhan.diwuzhang.Transaction.*;
import static java.util.Comparator.*;
import static java.util.stream.Collectors.*;

/**
 * Created by ruijie on 2017/11/14.
 */
public class Test {
    public static void main(String[] args) {
        /* 找出2011年的所有交易并按交易额排序（从低到高）*/
        List<Transaction> tr2011_1 = transactions.stream()
                .filter(year -> year.getYear() == 2011)  //给filter传递一个谓词 来选择2011年的交易
                .sorted(comparing(Transaction::getValue))  //按照交易额 进行排序
                .collect(Collectors.toList());
        System.out.println(tr2011_1);

          /* 找出2011年的所有交易并按交易额排序（从高到低）*/
        List<Transaction> tr2011_2 = transactions.stream()
                .filter(year -> year.getYear() == 2011) //给filter传递一个谓词 来选择2011年的交易
                .sorted(comparing(Transaction::getValue).reversed()) //按照交易额 进行排序
                .collect(Collectors.toList());
        System.out.println(tr2011_2);

        /*交易员都在哪些不同的城市工作过*/
        List<String> workCitys_1 = transactions.stream()
                .map(transaction -> transaction.getTrader().getCity()) //提取与交易相关的每 位交易员的所在城市
                .distinct() //只选择互不相同的城市
                .collect(Collectors.toList());
        System.out.println(workCitys_1);

        /*交易员都在哪些不同的城市工作过*/
        Set<String> workCitys_2 = transactions.stream()
                .map(transaction -> transaction.getTrader().getCity()) //提取与交易相关的每 位交易员的所在城市
                .collect(toSet());//使用Set集合去重
        System.out.println(workCitys_2);

        /*查找所有来自于剑桥的交易员，并按姓名排序*/
        List<Trader> traders = transactions.stream()
                .map(Transaction::getTrader) //从交易中提取 所有交易员
                .filter(trader -> trader.getCity().equals("Cambridge")) //仅选择位于剑 桥的交易员
                .distinct() //确保没有任 何重复
                .sorted(comparing(Trader::getName)) //对生成的交易员流按 照姓名进行排序
                .collect(toList());
        System.out.println(traders);

        /*返回所有交易员的姓名字符串，按字母顺序排序*/
        String namesStr_1 = transactions.stream()
                .map(Transaction::getTrader) //提取所有交易员姓名，生成一 个Strings构成的Stream
                .map(Trader::getName)
                .distinct()  //只选择不相同的姓名
                .sorted() //对姓名按字母顺序排序
                .reduce("", (str1, str2) -> str1 + str2); //逐个拼接每个名字，得到一个将所有名字连接起来的String (案效率不高)
        System.out.println(namesStr_1);

         /*返回所有交易员的姓名字符串，按字母顺序排序*/
        String namesStr_2 = transactions.stream()
                .map(Transaction::getTrader) //提取所有交易员姓名，生成一 个Strings构成的Stream
                .map(Trader::getName)
                .distinct()  //只选择不相同的姓名
                .sorted() //对姓名按字母顺序排序
                .collect(joining());//逐个拼接每个名字，得到一个将所有名字连接起来的String (效率高 内部会用到StringBuilder)
        System.out.println(namesStr_2);

        /*有没有交易员是在米兰工作的*/
        boolean milanBased = transactions.stream()
                .anyMatch(transaction -> transaction.getTrader().getName().equals("Milan"));
        System.out.println(milanBased);



        /*打印生活在剑桥的交易员的所有交易额*/
        transactions.stream()
                .filter(transaction -> transaction.getTrader().getCity().equals("Cambridge"))
                .map(Transaction::getValue)
                .forEach(System.out::print);
        System.out.println();

        /*所有交易中，最高的交易额是多少*/
        Optional<Integer> maxValue = transactions.stream()
                .map(Transaction::getValue)
                .reduce(Integer::max);
        System.out.println(maxValue.get());

        /*找到交易额最小的交易*/
        Optional<Transaction> mixTtransaction_1 = transactions.stream().reduce((t1, t2) -> t1.getValue() > t2.getValue() ? t2 : t1);
        System.out.println(mixTtransaction_1.get());

        /*找到交易额最小的交易*/
        Optional<Transaction> mixTtransaction_2 = transactions.stream().min(comparing(Transaction::getValue));
        System.out.println(mixTtransaction_2.get());


        Optional<Integer> sumValue = transactions
                .stream()
                .map(Transaction::getValue)
                .reduce(Integer::sum);
        System.out.println("sumValue: " + sumValue.get());

        System.out.println(transactions.stream().filter(transaction -> transaction.getValue() > 500).count() + " : " + transactions.size());

        transactions.stream().mapToInt(Transaction::getValue).sum();
        OptionalInt x =  transactions.stream().mapToInt(Transaction::getValue).max();
        transactions.stream().mapToInt(Transaction::getValue).boxed();
        transactions.stream().mapToInt(Transaction::getValue).average();

        System.out.println(IntStream.rangeClosed(1,100).filter(b -> b % 2 == 0).sum());
    }

}
