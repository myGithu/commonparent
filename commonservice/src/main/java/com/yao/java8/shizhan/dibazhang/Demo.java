package com.yao.java8.shizhan.dibazhang;

import java.util.stream.IntStream;

/**
 * Created by ruijie on 2017/11/24.
 */
public class Demo {
    public static void main(String[] args) {
        int a = 10;
        Runnable r =  () -> {
          int ax = 2;
            System.out.println(a);
        };

        IntStream.rangeClosed(1,20).boxed().reduce(0,Integer :: sum);


    }

}
