package com.yao.java8.shizhan.dishisizhang;

import com.yao.java8.shizhan.diyizhang.Apple;

import java.util.Comparator;
import java.util.function.Function;

/**
 * Created by ruijie on 2017/12/13.
 */
public class Test {
    public static void main(String[] args) {
        Function<String, Integer> strToInt = Integer::parseInt;
        Comparator<Apple> c = Comparator.comparing(Apple::getWeight);

    }
}
