package com.yao.java8.shizhan.diwuzhang;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by ruijie on 2017/11/14.
 */
public class Demo {
    public static void main(String[] args) {
        List<double[]> pythagoreanTriples = IntStream.rangeClosed(1, 100).boxed()
                .flatMap(a -> IntStream.rangeClosed(a, 100)
                        .mapToObj(b -> new double[]{a, b, Math.sqrt(a * a + b * b)})
                        .filter(t -> t[2] % 1 == 0)).limit(5).collect(Collectors.toList());
        pythagoreanTriples.forEach(x -> System.out.println(x[0] + " : " + x[1] + " : " + x[2]));


        Stream<String> stream = Stream.of("Java 8 ", "Lambda", "In", "Action");
        List<String> strings = stream.map(String::toUpperCase).collect(Collectors.toList());
        strings.forEach(System.out::println);

        int[] numbers = {2, 3, 5, 6, 3, 2, 4, 5, 2, 12, 5, 3, 65, 4, 5, 2, 3, 65, 2};
        IntStream streamx = Arrays.stream(numbers);

        long uniqueWords = 0;
        //你可以使用Files.lines得到一个流，其中的每个元素都是给定文件中的一行
        try (Stream<String> lines = Files.lines(Paths.get("D:/data.txt"), Charset.defaultCharset())) {
            Set<String> stringSet =
                    lines.map(line -> line.split(" ")).flatMap(Arrays::stream)
                    .collect(Collectors.toSet());
            //System.out.println(stringSet + ": " + stringSet.size());
        } catch (IOException e) {
            e.printStackTrace();
        }

       int sum =  Stream.iterate(0,n -> n + 2).limit(10).mapToInt(x -> x).sum();
        System.out.println(sum);

        Stream.iterate(new int[]{0, 1},i -> new int[] {i[1],i[0] + i[1]}).limit(10).map(x -> x[0]).forEach(x ->System.out.print(x + " "));
        System.out.println();

        Stream.generate(Math::random).limit(10).forEach(System.out::println);

    }
}
