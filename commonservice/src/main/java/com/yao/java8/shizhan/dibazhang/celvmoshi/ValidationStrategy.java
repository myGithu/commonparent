package com.yao.java8.shizhan.dibazhang.celvmoshi;

/**
 * Created by ruijie on 2017/11/25.
 */
@FunctionalInterface
public interface ValidationStrategy {
    boolean execute(String s);
}
