package com.yao.java8.shizhan.diyizhang;

/**
 * Created by ruijie on 2017/11/8.
 */
public class AppleFancyFormatter implements Formatter<Apple> {
    @Override
    public String accept(Apple apple) {
        String characteristic  = apple.getWeight() > 150 ? "heavy" : "light";

        return "A " + characteristic + " " + apple.getColor() + " apple";
    }
}
