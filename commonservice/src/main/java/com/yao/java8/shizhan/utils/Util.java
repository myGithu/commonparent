package com.yao.java8.shizhan.utils;

import com.yao.java8.shizhan.diyizhang.Apple;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Created by ruijie on 2017/11/8.
 */
public class Util {
    public static <T> List<T> filter(List<T> list, Predicate<T> predicate) {
        List<T> result = new ArrayList<>();
        for (T t : list) {
            if (predicate.test(t)) {
                result.add(t);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2,8, 3, 4,50,100,74,256,856, 5, 20, 22, 21, 25, 23, 65);
        //List<Integer> list = filter(numbers, (Integer i) -> i % 2 == 0);
      //  System.out.println(list);

      /*  numbers.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });*/
        numbers.sort((i1,i2) -> i2 - i1);

        System.out.println(numbers);

       // Thread t = new Thread(Util::print);
       // t.start();
    }

    public static void print(){
        System.out.println("nihao");
    }
}
