package com.yao.java8.shizhan.diwuzhang;

/**
 * Created by ruijie on 2017/11/14.
 */
public class Trader {

    private final String name;

    private final String city;

    public Trader(String name, String city){
        this.name = name;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    @Override
    public String toString() {
        return "Trader :" + this.name + " in " + this.city;
    }
}
