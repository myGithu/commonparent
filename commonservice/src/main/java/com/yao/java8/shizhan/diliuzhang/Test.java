package com.yao.java8.shizhan.diliuzhang;

import com.yao.java8.shizhan.disizhang.Dish;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

/**
 * Created by ruijie on 2017/11/15.
 */
public class Test {
    public static void main(String[] args) {

        Map<Dish.Type, List<Dish>> typeListMap = new HashMap<>();

        for (Dish dish : Dish.menu) {
            Dish.Type type = dish.getType();
            List<Dish> dishList = typeListMap.computeIfAbsent(type, k -> new ArrayList<>());
            dishList.add(dish);
        }

        System.out.println(typeListMap);
        System.out.println(Dish.menu.stream().collect(groupingBy(Dish::getType)));

      /*   Supplier<HashMap> supplier =  HashMap :: new;
        supplier.get();
       long howManyDishes = Dish.menu.stream().collect(Collectors.counting());
        System.out.println(howManyDishes);
        try {
            Optional<Dish> mostCaloriesDish =  Dish.menu.stream()
                      .collect(Collectors.minBy(Comparator.comparing(Dish :: getCalories)));
            System.out.println(mostCaloriesDish.get().getCalories());
            System.out.println(Dish.menu.stream().mapToInt(Dish::getCalories).sum());
            System.out.println(Dish.menu.stream().collect(Collectors.summingInt(Dish :: getCalories)));
            OptionalDouble y =  Dish.menu.stream().mapToInt(Dish::getCalories).average();
            System.out.println(Dish.menu.stream().mapToInt(Dish::getCalories).average());
            double x =  Dish.menu.stream().collect(Collectors.averagingInt(Dish :: getCalories));
            System.out.println(Dish.menu.stream().collect(Collectors.averagingInt(Dish :: getCalories)));
            IntSummaryStatistics statistics =  Dish.menu.stream().collect(Collectors.summarizingInt(Dish :: getCalories));
            System.out.println(statistics);

            String names = Dish.menu.stream().map(Dish::getName).collect(Collectors.joining(", "));
            System.out.println(names);*/
       /* double y = Dish.menu.stream().collect(Collectors.averagingInt(Dish::getCalories));
        Integer x = Dish.menu.stream().collect(reducing(0, Dish::getCalories, (i, j) -> i + j));
        System.out.println(x);
        Integer m = Dish.menu.stream().map(Dish::getCalories).collect(reducing(0, (i, j) -> i + j));
        System.out.println(m);*/
        //  System.out.println(Dish.menu.stream().collect(reducing(0, Dish::getCalories, (i, j) -> i + j)));
        //Optional<Dish> mostCalorieDish =
        //      Dish.menu.stream().collect(reducing((d1, d2) -> d1.getCalories() > d2.getCalories() ? d1 : d2));
        //System.out.println(mostCalorieDish.get());
        /* } catch (Exception e) {
            e.printStackTrace();
        }*/
       /* Stream<Integer> stream = Arrays.asList(1, 2, 3, 4, 5, 6).stream();
        List<Integer> numbers = stream.reduce(
                new ArrayList<Integer>(),
                (List<Integer> l, Integer e) -> {
                    l.add(e);
                    return l;
                }, (List<Integer> l1, List<Integer> l2) -> {
                    l1.addAll(l2);
                    return l1;
                });

        int k = Dish.menu.stream().collect(reducing(0, Dish::getCalories, Integer::sum));

        Map<Dish.Type, List<Dish>> dishByType = Dish.menu.stream().collect(groupingBy(Dish::getType));
        System.out.println(dishByType);
        List<Dish> dishes = dishByType.get(Dish.Type.OTHER);
        System.out.println(dishes);

        Map<CaloricLevel,List<Dish>> levelListMap =  Dish.menu.stream().collect(groupingBy(dish ->{
            if(dish.getCalories() <= 400)
                return CaloricLevel.DIET;
            else if(dish.getCalories() >= 700)
                return CaloricLevel.FAT;
            else
                return CaloricLevel.NORMAL;
        }));
        System.out.println(levelListMap);

*/
        Map<Dish.Type, Map<CaloricLevel, List<Dish>>> typeMapMap = Dish.menu.stream().collect(groupingBy(Dish::getType, groupingBy(dish -> {
            if (dish.getCalories() <= 400)
                return CaloricLevel.DIET;
            else if (dish.getCalories() >= 700)
                return CaloricLevel.FAT;
            else
                return CaloricLevel.NORMAL;
        })));
        System.out.println(typeMapMap);

        Map<Dish.Type, Long> typeLongMap = Dish.menu.stream().collect(groupingBy(Dish::getType, counting()));
        System.out.println(typeLongMap);

        Map<Dish.Type, Optional<Dish>> mostCaloricByType_1 =
                Dish.menu.stream()
                        .collect(groupingBy(Dish::getType,
                                maxBy(Comparator.comparingInt(Dish::getCalories))));

        System.out.println(mostCaloricByType_1);
        Map<Dish.Type, Integer> mostCaloricByType = Dish.menu.stream().collect(groupingBy(Dish::getType, collectingAndThen(
                maxBy(Comparator.comparingInt(Dish::getCalories)), dish -> dish.get().getCalories())));
        System.out.println(mostCaloricByType);

        Map<Dish.Type, Integer> sumCalories = Dish.menu.stream().collect(groupingBy(Dish::getType, summingInt(Dish::getCalories)));
        System.out.println(sumCalories);


        Map<Dish.Type, Set<CaloricLevel>> typeSetMap = Dish.menu.stream().collect(groupingBy(Dish::getType, mapping(dish -> {
            if (dish.getCalories() <= 400)
                return CaloricLevel.DIET;
            else if (dish.getCalories() >= 700)
                return CaloricLevel.NORMAL;
            else
                return CaloricLevel.FAT;
        }, toCollection(HashSet::new))));

        System.out.println(typeSetMap);

        Map<Boolean, Map<Dish.Type, List<Dish>>> booleanMapMap = Dish.menu.stream().collect(
                partitioningBy(
                        Dish::isBegetarian,
                        groupingBy(Dish::getType))
        );
        System.out.println(booleanMapMap);

        //
        Map<Boolean, Map<Dish.Type, Dish>> mostCaloricPartitionedByVegetarian
                = Dish.menu.stream().collect(partitioningBy(Dish::isBegetarian,
                groupingBy(
                        Dish::getType,
                        collectingAndThen(
                                maxBy(Comparator.comparing(Dish::getCalories)),
                                Optional::get
                        ))));
        System.out.println(mostCaloricPartitionedByVegetarian);


        Map<Boolean, Map<Boolean, Set<Dish>>> booleanMapMap1 = Dish.menu.stream().
                collect(partitioningBy(Dish::isBegetarian, partitioningBy(
                        d -> d.getCalories() > 500, toCollection(HashSet::new))));

        Map<Boolean, Map<Dish.Type, List<Dish>>> booleanMapMap2 =
                Dish.menu.stream().collect(partitioningBy(Dish::isBegetarian, groupingBy(Dish::getType)));

        Map<Boolean, Long> booleanLongMap = Dish.menu.stream().collect(partitioningBy(Dish::isBegetarian, counting()));
        System.out.println(booleanLongMap);


    }


}

enum CaloricLevel {
    DIET, NORMAL, FAT
}
