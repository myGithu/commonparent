package com.yao.java8.shizhan.dijiuzhang;

/**
 * Created by ruijie on 2017/11/30.
 */
public interface A {
    default void hello() {
        System.out.println("Hello from A");
    }
}

interface B{
    default void hello() {
        System.out.println("Hello from B");
    }
}

class C implements B, A {
    public static void main(String... args) {
        new C().hello();
    }

    @Override
    public void hello() {
       B.super.hello();
    }
}
