package com.yao.java8.shizhan.diqizhang;


import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Created by ruijie on 2017/11/22.
 */
public class Demo {
    public static long sequentialSum(long n) {
        return Stream.iterate(1L, i -> i + 1)
                .limit(n).reduce(0L, Long::sum);
    }

    public static long parallelSum(long n) {
        return Stream.iterate(1L, i -> i + 1)
                .limit(n)
                .parallel()
                .reduce(0L, Long::sum);
    }


    public static void main(String[] args) {
        // System.out.println(sequentialSum(6));
        //long startTime1 = System.currentTimeMillis();
        //sequentialSum(10000000);
        //System.out.println(System.currentTimeMillis() - startTime1);
        //long startTime2 =  System.nanoTime();
        //parallelSum(10000000);
        //System.out.println(System.nanoTime() - startTime2);
        System.out.println(countWordsIteratively(SENTENCE));
    }

    static final String SENTENCE =
            " Nel mezzo del   cammin di nostra vita " +
                    "mi ritrovai in una selva oscura" +
                    " ché la dritta via era smarrita ";

    public static int countWordsIteratively(String s){
        int counter = 0;
        boolean lastSpace = true;

        for (Character c : s.toCharArray()) {
            if (Character.isWhitespace(c)){
                lastSpace = true;
            }else{
                if (lastSpace)
                    counter ++;
                lastSpace  = false;
            }
        }
        return counter;
    }


}

class WordCounter{
    private final int counter;

    private final boolean lastSpace;

    public WordCounter(int counter, boolean lastSpace){
        this.counter = counter;

        this.lastSpace = lastSpace;
    }

    public WordCounter accumulate(Character c){
        if (Character.isWhitespace(c)) {
            return lastSpace ? this : new WordCounter(counter, true);
        }else{
            return lastSpace ? new WordCounter(counter + 1, false) : this;
        }
    }

    public WordCounter combine(WordCounter wordCounter){
        return new WordCounter(counter + wordCounter.counter,wordCounter.lastSpace);
    }

    public int getCounter() {
        return counter;
    }
}
