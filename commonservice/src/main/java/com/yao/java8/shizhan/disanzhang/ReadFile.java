package com.yao.java8.shizhan.disanzhang;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by ruijie on 2017/11/8.
 */
public class ReadFile {
    public static String processFile(BufferedReaderProcessor processor) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader("d:/data.txt"))) {
            String resultString = processor.process(br);
            br.close();
            return resultString;
        }
    }

    public static void main(String[] args) {
        try {
           String readString =  processFile((BufferedReader br) -> br.readLine() + br.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
