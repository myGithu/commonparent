package com.yao.java8.shizhan.diyizhang;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruijie on 2017/11/7.
 */
public class Apple {
    private String color;

    private int weight;

    public static List<Apple> inventory;

    public Apple(int weight) {
        this.weight = weight;
    }

    public Apple(String color, int weight) {
        this.color = color;
        this.weight = weight;
    }

    public Apple(){
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }





    public static boolean isGreenApple(Apple apple){
            return "green".equals(apple.getColor());
    }

    public static boolean isHeavyApple(Apple apple){
        return apple.getWeight() > 150;
    }

    public interface Predicate<T>{
        boolean test(T t);
    }

    static List<Apple> filterApples(List<Apple> inventory ,Predicate<Apple> p){
            List<Apple> result = new  ArrayList<>();
        for (Apple apple : inventory) {
            if(p.test(apple)){
                result.add(apple);
            }
        }
        return result;
    }


    public static void main(String[] args) {
        filterApples(inventory,Apple :: isGreenApple);
        filterApples(inventory,Apple :: isHeavyApple);

        filterApples(inventory,(Apple a) -> "green".equals(a.getColor()));
        filterApples(inventory,(Apple a) -> a.getWeight() > 150);

       // inventory.stream().filter((Apple a) -> a.getWeight() > 150).collect();
    }

}
