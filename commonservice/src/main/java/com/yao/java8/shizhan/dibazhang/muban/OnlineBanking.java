package com.yao.java8.shizhan.dibazhang.muban;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by ruijie on 2017/11/28.
 */
public abstract class OnlineBanking {
    public void pricessCustomer(String id) {
        Customer customer = Database.getCustomerWithId(id);
        makeCustomerHappy(customer);
    }

    abstract void makeCustomerHappy(Customer c);
}

class GDBank extends OnlineBanking {

    @Override
    void makeCustomerHappy(Customer c) {
        System.out.println(c);
    }

    public static void main(String[] args) {
        OnlineBanking onlineBanking = new GDBank();
        onlineBanking.pricessCustomer("0002");
    }

}

class OtherBank {
    public void processCustomer(String id, Consumer<Customer> makeCustomerHappy) {
        makeCustomerHappy.accept(Database.getCustomerWithId(id));
    }

    public static void main(String[] args) {
        new OtherBank().processCustomer("0005", customer -> System.out.println(customer));
    }
}


class Customer {
    private final String id;

    private final String name;

    private final String password;

    public Customer(String id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public boolean getById(String id) {
        return this.id.equals(id);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}

class Database {
    private final static List<Customer> customerList = Arrays.asList(
            new Customer("0001", "张三", "123456"),
            new Customer("0002", "李四", "123456"),
            new Customer("0003", "王五", "123456"),
            new Customer("0004", "赵六", "123456"),
            new Customer("0005", "不知道", "123456"),
            new Customer("0006", "自动", "123456")
    );

    public static Customer getCustomerWithId(String id) {
        return customerList.stream().filter(customer -> customer.getById(id)).findFirst().orElse(null);
    }
}
