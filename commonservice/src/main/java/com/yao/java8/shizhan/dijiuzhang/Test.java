package com.yao.java8.shizhan.dijiuzhang;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Created by ruijie on 2017/11/28.
 */
public class Test {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(3, 5, 1, 2, 6);
        numbers.sort(Comparator.naturalOrder());
        System.out.println(numbers);
    }
}
