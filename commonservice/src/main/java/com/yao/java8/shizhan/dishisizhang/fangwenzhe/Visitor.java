package com.yao.java8.shizhan.dishisizhang.fangwenzhe;

/**
 * Created by ruijie on 2017/12/14.
 */
public abstract class Visitor {
    protected  String name;

    public void setName(String name){
        this.name = name;
    }

    public abstract void visitor(Medicine medicine);
}


class Charger extends Visitor{

    @Override
    public void visitor(Medicine medicine) {
        System.out.println("划价员：" + name +"给药" + medicine.getName() +"划价:" + medicine.getPrice());
    }
}

class WorkerOfPharmacy extends Visitor{

    @Override
    public void visitor(Medicine medicine) {
        System.out.println("药房工作者：" + name +"拿药" + medicine.getName());
    }
}
