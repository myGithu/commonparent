package com.yao.java8.shizhan.dibazhang.gongchang;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Created by ruijie on 2017/11/28.
 */

class Product{
}

class Loan extends  Product{
}

class Stock extends Product{
}

class Bond extends Product{
}

public class ProductFactory {
    public static Product createProduct_1(String name){
        switch(name){
            case "loan": return new Loan();
            case "stock": return new Stock();
            case "bond": return new Bond();
            default: throw new RuntimeException("No such product " + name);
        }
    }

    final static Map<String, Supplier<Product>> map = new HashMap<>();

    static {
        map.put("loan", Loan::new);
        map.put("stock", Stock::new);
        map.put("bond", Bond::new);
    }

    public static Product createProduct_2(String name){
        Supplier<Product> p = map.get(name);
        if(p != null) return p.get();
        throw new IllegalArgumentException("No such product " + name);
    }

    public static void main(String[] args) {
        Product p = ProductFactory.createProduct_1("loan");

        Supplier<Product> loanSupplier =  Loan :: new;
        Product product = loanSupplier.get();
        Loan loan = (Loan) loanSupplier.get();

        ProductFactory.createProduct_2("loan");
    }
}
