package com.yao.java8.shizhan.dibazhang.guanchazhe;

/**
 * Created by ruijie on 2017/11/28.
 * <p>
 * 观察者
 */
@FunctionalInterface
public interface Observer {
    void notify(String tweet);

}

/**
 * 不同的观察者
 */
class NYTimes implements Observer {
    public void notify(String tweet) {
        if (tweet != null && tweet.contains("money")) {
            System.out.println("Breaking news in NY! " + tweet);
        }
    }
}

class Guardian implements Observer {
    public void notify(String tweet) {
        if (tweet != null && tweet.contains("queen")) {
            System.out.println("Yet another news in London... " + tweet);
        }
    }
}

class LeMonde implements Observer {
    public void notify(String tweet) {
        if (tweet != null && tweet.contains("wine")) {
            System.out.println("Today cheese, wine and news! " + tweet);
        }
    }
}
