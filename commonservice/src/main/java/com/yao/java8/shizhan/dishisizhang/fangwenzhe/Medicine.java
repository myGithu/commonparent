package com.yao.java8.shizhan.dishisizhang.fangwenzhe;

/**
 * Created by ruijie on 2017/12/14.
 */
public  class Medicine {
    private String name;

    private double price;

    public Medicine(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void accept(Visitor visitor){
        visitor.visitor(this);
    }
}

