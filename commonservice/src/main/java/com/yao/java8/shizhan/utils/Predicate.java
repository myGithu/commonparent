package com.yao.java8.shizhan.utils;

/**
 * Created by ruijie on 2017/11/8.
 */
public interface Predicate<T> {
    boolean test (T t);
}
