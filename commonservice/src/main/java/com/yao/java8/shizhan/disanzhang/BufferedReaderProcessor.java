package com.yao.java8.shizhan.disanzhang;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by ruijie on 2017/11/8.
 */
@FunctionalInterface
public interface BufferedReaderProcessor {
    String process(BufferedReader reader) throws IOException;
}
