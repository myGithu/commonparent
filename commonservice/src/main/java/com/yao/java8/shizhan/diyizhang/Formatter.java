package com.yao.java8.shizhan.diyizhang;

/**
 * Created by ruijie on 2017/11/8.
 */
public interface Formatter<T> {
    String accept(T t);
}
