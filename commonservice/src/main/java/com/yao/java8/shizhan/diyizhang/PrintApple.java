package com.yao.java8.shizhan.diyizhang;

import java.util.List;

/**
 * Created by ruijie on 2017/11/8.
 */
public class PrintApple {
    public static void prettyPrintApple(List<Apple> inventory, Formatter<Apple> formatter){
        for (Apple apple : inventory) {
            String output = formatter.accept(apple);
            System.out.println(output);
        }
    }

    public static void main(String[] args) {
        prettyPrintApple(Apple.inventory,(Apple apple) -> {
            String characteristic  = apple.getWeight() > 150 ? "heavy" : "light";
            return "A " + characteristic + " " + apple.getColor() + " apple";
        });

    }
}
