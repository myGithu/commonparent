package com.yao.java8.shizhan.disanzhang;


import com.yao.java8.shizhan.diyizhang.Apple;
import com.yao.java8.shizhan.utils.Util;
import com.yao.thkinginjava.dishiwuzhang.fanxinjiekou.Generator;

import java.io.Serializable;
import java.util.*;
import java.util.function.*;

/**
 * Created by ruijie on 2017/11/8.
 */
public class Test {

    public static void main(String[] args) {

      /*  Runnable r1 = () -> System.out.println("Hello World 1");

        Runnable r2 = new Runnable() {
            @Override
            public void run() {
                System.out.println("Hello World 2");
            }
        };*/

        // forEach(Arrays.asList(1,5,9,63,5,5,52),(Integer i) -> System.out.println(i));

        //  List<String> list = map(Arrays.asList("123","456789","852963741"),(String s) -> s);
        // System.out.println(list);

        List<String> items = new ArrayList<>();
        items.add("B");
        items.add("C");
        items.add("A");
        items.add("E");
        items.add("D");
       /* forEach(items,s -> {
            if("E".equals(s))
                System.out.println(s);

        });*/
        //输出：A,B,C,D,E
        //items.forEach(item->System.out.println(item));

       /* Map<String, Integer> items = new HashMap<>();
        items.put("A", 10);
        items.put("B", 20);
        items.put("C", 30);
        items.put("D", 40);
        items.put("E", 50);
        items.put("F", 60);
        items.forEach((String k,Integer v)->System.out.println("Item : " + k + " Count : " + v));
        items.forEach((k,v)->{
            System.out.println("Item : " + k + " Count : " + v);
            if("E".equals(k)){
                System.out.println("Hello E");
            }
        });*/

        // Predicate返回了一个boolean
// Consumer返回了一个void
        //Consumer<String> b = s -> s.equals("A");
        //items.sort((o1,o2) -> o1.compareTo(o1));
        // items.sort(String :: compareToIgnoreCase);
        // items.sort((s1,s2) -> s2.compareTo(s1));
     /*   items.sort((s1, s2) -> s2.compareToIgnoreCase(s1));
        //System.out.println(items);
        Function<String, Integer> stringToInteger =
                (String s) -> Integer.parseInt(s);

        Function<String, Integer> stringInteger1 = (Integer::parseInt);

        BiPredicate<List<String>, String> contains =
                (list, element) -> list.contains(element);
        BiPredicate<List<String>, String> contains1 = (List::contains);

        List<Apple> list =  map(Arrays.asList(1,5,9,63,5,5,52),Apple :: new);
        List<Apple> list1 =  map(Arrays.asList(1,5,9,63,5,5,52),integer -> new Apple(integer));
        Comparator<Apple> comparator =  Comparator.comparing(apple -> apple.getWeight());
        Comparator<Apple> comparator1 =  Comparator.comparing(Apple :: getWeight).thenComparing(Apple :: getColor).reversed();*/

        Function<Integer,Integer> f =  (x -> x + 1);
        System.out.println(f.andThen(x -> x * 2).apply(2));


    }

    public static <T> void forEach(List<T> list, Consumer<T> c) {
        for (T t : list) {
            c.accept(t);
        }
    }

    public static <T, R> List<R> map(List<T> list, Function<T, R> function) {
        List<R> result = new ArrayList<>();

        for (T t : list) {
            result.add(function.apply(t));
        }

        return result;
    }



}

interface BiPredicate2<T, U> {
    void test(T t, U u);
}




