package com.yao.java8.shizhan.dishierzhang;

import java.time.DayOfWeek;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAdjuster;

/**
 * Created by ruijie on 2017/12/12.
 */
public class NextWorkingDay implements TemporalAdjuster{


    @Override
    public Temporal adjustInto(Temporal temporal) {
        DayOfWeek dow =  DayOfWeek.of(temporal.get(ChronoField.DAY_OF_WEEK));
        int dayToAdd = 1;
        if (dow == DayOfWeek.FRIDAY)
            dayToAdd = 3;
        else if(dow == DayOfWeek.SATURDAY)
            dayToAdd = 2;
        return temporal.plus(dayToAdd, ChronoUnit.DAYS);
    }


    public  static TemporalAdjuster getNextWorkingDay(){
       return  temporal ->{
           DayOfWeek dow =  DayOfWeek.of(temporal.get(ChronoField.DAY_OF_WEEK));
           int dayToAdd = 1;
           if (dow == DayOfWeek.FRIDAY)
               dayToAdd = 3;
           else if(dow == DayOfWeek.SATURDAY)
               dayToAdd = 2;
           return temporal.plus(dayToAdd, ChronoUnit.DAYS);
        };
    }
}
