package com.yao.java8.shizhan.disizhang;


import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by ruijie on 2017/11/10.
 */
public class Test {
    public static void main(String[] args) {
      /* System.out.println(Dish.menu);
        List<String> dishes = Dish.menu.stream()
                .filter(dish -> {
                    System.out.println("filtering " + dish.getName());
                    return dish.getCalories() > 300;
                })
                .map(dish -> {
                    System.out.println("mapping " + dish.getName());
                    return dish.getName();
                })
                .limit(3)
                .collect(Collectors.toList());
        System.out.println(dishes);*/

     /*List<String> title = Arrays.asList("Java8","In", "Action");
        Stream<String> s = title.stream();
        s.forEach(System.out :: println);
        s.forEach(System.out :: println);*/

       /* List<Dish> vegetarianMenu = Dish.menu.stream().filter(Dish::isBegetarian).collect(Collectors.toList());
        List<Integer> numbers = Arrays.asList(1,5,3,2,10,12,18,15,17,2);
        numbers.stream().filter(integer -> integer % 2 == 0).distinct().forEach(System.out :: println);*/
        //Dish.menu.stream().filter(dish -> dish.getCalories() > 300).skip(2).forEach(System.out ::println);
        // String[] arrayOfWords = {"G","G","a","B"};
        //  List<String> stringStream = Arrays.stream(arrayOfWords).distinct().collect(Collectors.toList());
        // System.out.println(stringStream);


      /*  List<String> list = Arrays.asList("Hello", "World");

        list.stream().map(a -> {
            String[] x = a.split("");
            System.out.println(Arrays.toString(x));
            return Arrays.stream(x);
        }).distinct().collect(Collectors.toList());*/
      /*  List<String[]> list1 =  list.stream().
                map(word ->word.split(""))
                //.map(Arrays::stream)
                .distinct()
                .collect(Collectors.toList());*/
       /* List<String> list1 = list.stream()
                .map(word ->word.split(""))
                .flatMap(Arrays::stream)
                .distinct()
                .collect(Collectors.toList());
        System.out.println(list1);*/

       List<Integer> numbers1 = Arrays.asList(1,2,3);
       List<Integer> numbers2 = Arrays.asList(3,4);

        numbers1.stream().
              flatMap(i -> numbers2.stream().map(j -> new int[]{i,j}))
               .collect(Collectors.toList());
        Optional<Dish> optional = Dish.menu.stream().filter(dish -> Dish.Type.OTHER.equals(dish.getType())).findAny();
        Dish dish =  optional.get();
        System.out.println(dish);
    }
}
