package com.yao.java8.shizhan.dishiyizhang;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * Created by ruijie on 2017/12/1.
 */
public class Shop {
    public static final List<Shop> shops = Arrays.asList(
            new Shop("BestPrice"),
            new Shop("LetsSaveBig"),
            new Shop("MyFavoriteShop"),
            new Shop("BuyItAll")
    );

    private String name;

    public Shop() {
    }

    public Shop(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice(String product) {
        Random random = new Random();
        double price = calculatePrice(product);
        Discount.Code code = Discount.Code.values()[random.nextInt(Discount.Code.values().length)];
        return String.format("%s:%.2f:%s", name, price, code);
    }

    private double calculatePrice(String product) {
        delay();
        return new Random().nextDouble() * product.charAt(0) + product.charAt(1);
    }

    public static void delay() {
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }


    public Future<Double> getPriceAsync(String product) {
        return CompletableFuture.supplyAsync(() -> calculatePrice(product));
    }

    public static CompletableFuture<String> getStrPriceAsync(Shop shop, String product) {
        return CompletableFuture.supplyAsync(
                () -> String.format("%s price is %.2f", shop.getName(), shop.getPrice(product)));
    }

    public static List<String> findPrices(String product) {
        return shops.stream()
                .map(shop -> getStrPriceAsync(shop, product))
                .collect(Collectors.toList())
                .stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());
    }

    public static List<String> findPrices2(String product){
        return shops.stream()
                .map(shop -> shop.getPrice(product))
                .map(Quote :: parse)
                .map(Discount :: applyDiscount)
                .collect(Collectors.toList());
    }

    public static List<String> findPricesAsync(String product){
         List<CompletableFuture<String>> futures =  shops.stream()
                    .map(shop -> CompletableFuture.supplyAsync(
                            () -> shop.getPrice(product)
                    ))
                    .map(future -> future.thenApply(Quote :: parse))
                    .map(future -> future.thenCompose(quote ->
                            CompletableFuture.supplyAsync(
                                    () -> Discount.applyDiscount(quote)
                            )
                    ))
                    .collect(Collectors.toList());
         return futures.stream().map(CompletableFuture :: join).collect(Collectors.toList());
    }


    public static void main(String[] args) {
      /*  Shop shop = new Shop("BestShop");
        long startTime = System.nanoTime();
        Future<Double> futurePrice = shop.getPriceAsync("my favorite product");
        long invocationTime = (System.nanoTime() - startTime) / 1_000_000;
        try {
            System.out.println(futurePrice.get(1, TimeUnit.SECONDS));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        System.out.println("Invocation returned after " + invocationTime
                + " msecs");*/

        long start = System.nanoTime();
        System.out.println(findPrices("myPhone27s"));
        long duration = (System.nanoTime() - start) / 1_000_000;
        System.out.println("Done in " + duration + " msesc");

    }
}
