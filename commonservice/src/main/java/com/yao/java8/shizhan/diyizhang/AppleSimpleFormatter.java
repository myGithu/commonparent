package com.yao.java8.shizhan.diyizhang;

/**
 * Created by ruijie on 2017/11/8.
 */
public class AppleSimpleFormatter implements  Formatter<Apple>{
    @Override
    public String accept(Apple apple) {
        return "An apple of " + apple.getWeight() + "g";
    }
}
