package com.yao.java8.shizhan.diliuzhang;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by ruijie on 2017/11/17.
 */
public class Prime {

    public boolean isPrime(int candidate) {
        int candidateRoot = (int) Math.sqrt((double) candidate);
        return IntStream.rangeClosed(2, candidateRoot).noneMatch(i -> candidate % i == 0);
    }


    public Map<Boolean, List<Integer>> partitionPrimes(int numbers) {
        return IntStream.rangeClosed(2, numbers).boxed().collect(Collectors.partitioningBy(this::isPrime));
    }

    public static  boolean isPrime(List<Integer> primes, int candidate) {
        int candidateRoot = (int) Math.sqrt((double) candidate);
        return  takeWith(primes, i -> i <= candidateRoot).stream().noneMatch(p -> candidate % p == 0);
    }

    public static <A> List<A> takeWith(List<A> list, Predicate<A> p) {
        int i = 0;
        for (A item : list) {
            if (!p.test(item))
                return list.subList(0, i);
            i ++;
        }
        return list;
    }

    public static void main(String[] args) {
        Prime prime = new Prime();
        System.out.println(prime.partitionPrimes(4));
    }

}
