package com.yao.java8.shizhan.dibazhang.celvmoshi;

/**
 * Created by ruijie on 2017/11/25.
 */
public class Validator {
    private final ValidationStrategy strategy;

    public Validator(ValidationStrategy validationStrategy) {
        this.strategy = validationStrategy;
    }

    public boolean validate(String s) {
        return strategy.execute(s);
    }

    public static void main(String[] args) {
        Validator numericValidator = new Validator(new IsNumeric());
        boolean b1 = numericValidator.validate("aaa");

        Validator validator = new Validator(s -> s.matches("[a-z]"));

    }
}
