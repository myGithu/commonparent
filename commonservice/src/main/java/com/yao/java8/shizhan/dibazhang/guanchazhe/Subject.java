package com.yao.java8.shizhan.dibazhang.guanchazhe;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by ruijie on 2017/11/28.
 */
public interface Subject {
    void registerObserver(Observer o);

    void notifyObservers(String tweet);

    void removeObserver(Observer o);
}

class Feed implements Subject {

    private final List<Observer> observers = new ArrayList<>();

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void notifyObservers(String tweet) {
        observers.forEach(observer -> observer.notify(tweet));
    }

    @Override
    public void removeObserver(Observer o) {
        Objects.requireNonNull(o);
        observers.remove(o);
    }
}

class Test {
    public static void main(String[] args) {
        Feed feed = new Feed();
        feed.registerObserver(tweet -> {
            if (tweet != null && tweet.contains("money")) {
                System.out.println("Breaking news in NY! " + tweet);
            }
        });

        feed.registerObserver(tweet -> {
            if (tweet != null && tweet.contains("queen")) {
                System.out.println("Yet another news in London... " + tweet);
            }
        });
        feed.notifyObservers("The queen said her favourite book is Java 8 in Action!");



    }
}
