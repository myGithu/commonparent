package com.yao.java8.shizhan.dijiuzhang;

import java.util.Collection;
import java.util.Iterator;
import java.util.function.Predicate;

/**
 * Created by ruijie on 2017/11/30.
 */


interface Collectionx<E> extends Collection<E>{
    default  boolean removeIf(Predicate<? super E> filter){
        boolean removed = false;
        Iterator<E> each = iterator();
        while (each.hasNext()){
                if (filter.test(each.next())){
                    each.remove();
                    removed = true;
                }
        }
        return removed;
    }
}

public interface Sized {
    int size();

    default  boolean isEmpty(){
        return size() == 0;
    }


}


class SizedClass implements Sized {


    @Override
    public int size() {

        return 0;
    }

    public static void main(String[] args) {
        SizedClass sizedClass = new SizedClass();
        System.out.println(sizedClass.isEmpty());
    }

}