package com.yao.java8.shizhan.dishisanzhang;

import com.yao.thejavaprogramminglanguage.studingdmeo.FirstDemo;

import java.util.stream.LongStream;

/**
 * Created by ruijie on 2017/12/12.
 */
public class Test {
        public static  int facorialIterative(int n){
            int result = 1;
            for (int i = 1; i <= n; i++) {
                result *= i;
            }
            return  result;
        }

        public static long facorialIterative(long n){
            return n == 1 ? 1 : n * facorialIterative(n - 1);
        }

        public static  long factorialStreams(long n){
            return LongStream.rangeClosed(1,n).reduce(1, (a, b) -> a * b);
        }

        //尾递 写法
        public static long factoriaHelper(long acc, long n){
            return n == 1 ? acc : factoriaHelper(acc * n, n - 1);
        }

        public static long factorialTailRecursive(long n){
            return factoriaHelper(1, n);
        }
}
