package com.yao.java8.shizhan.dishisizhang.fangwenzhe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruijie on 2017/12/14.
 */
public class Presciption {
    private List<Medicine> medicineList = new ArrayList<>();


    public void accept(Visitor visitor){
        medicineList.forEach(visitor::visitor);
    }

    public void addMedicine(Medicine medicine){
        medicineList.add(medicine);
    }

    public void removeMedicine(Medicine medicine){
        medicineList.remove(medicine);
    }
}

class Client{
    public static void main(String[] args) {
        Medicine a = new Medicine("板蓝根",11.0D);
        Medicine b = new Medicine("感康",15.01D);
        Medicine c = new Medicine("Vc",150.01D);
        Presciption presciption = new Presciption();

        presciption.addMedicine(a);
        presciption.addMedicine(b);
        presciption.addMedicine(c);

        Visitor charger =  new Charger();
        charger.setName("张三");

        Visitor workerOfPharmacy = new WorkerOfPharmacy();
        workerOfPharmacy.setName("李四");

        presciption.accept(charger);
        System.out.println("------------------------------------");
        presciption.accept(workerOfPharmacy);
    }
}
