package com.yao.springboot.aspect;

import com.yao.springboot.annotation.Action;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.omg.PortableInterceptor.ACTIVE;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.net.SocketTimeoutException;

/**
 * Created by ruijie on 2017/5/24.
 * 编写切面AOP
 *
 */
@Aspect
@Component
public class LogAspect {
    //定义切点
    @Pointcut("@annotation(com.yao.springboot.annotation.Action)")
    public void annotationPointCutAfter(){};
   /* @Pointcut("@annotation(com.yao.springboot.annotation.Action)")
    public void annotationPointCutBefor(){};*/
    @After("annotationPointCutAfter()")
    public void after(JoinPoint joinPoint){
        //joinPoint.getSignature()获取封装了署名信息的对象,在该对象中可以获取到目标方法名,所属类的Class等信息
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Action action = method.getAnnotation(Action.class);
        System.out.println("after:" + action.name());
    }
/*    @Before("annotationPointCutBefor()")
    public void befor(JoinPoint joinPoint){
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Action action = method.getAnnotation(Action.class);
        System.out.println("bofor:" + action.name());
    }*/

    @Before("execution(* com.yao.springboot.service.DemoMethodSerivce.*(..))")
    public void befor(JoinPoint joinPoint){
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        System.out.println("这个是execution方法拦截：" + method.getName());
    }

}
