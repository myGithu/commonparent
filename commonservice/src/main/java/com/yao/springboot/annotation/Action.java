package com.yao.springboot.annotation;

import java.lang.annotation.*;

/**
 * Created by ruijie on 2017/5/24.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Action {
    String name();
}
