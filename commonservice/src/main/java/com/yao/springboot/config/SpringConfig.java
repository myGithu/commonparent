package com.yao.springboot.config;

import com.yao.springboot.service.FunctionService;
import com.yao.springboot.service.UserFunctionService;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;


/**
 * Created by ruijie on 2017/5/24.
 */
@Configuration//声明为配置类
@ComponentScan("com.yao.springboot")//扫描包
@EnableAspectJAutoProxy //让spring开启Aop功能
public class SpringConfig {
/*    @Bean//该注解返回一个bean相当于xml中的<bean></bean>
    public FunctionService functionService(){
        return new FunctionService();
    }
    @Bean
    public UserFunctionService userFunctionService(){
        UserFunctionService userFunctionService= new UserFunctionService();
        userFunctionService.setFunctionService(functionService());
        return  userFunctionService;
    }*/


}

