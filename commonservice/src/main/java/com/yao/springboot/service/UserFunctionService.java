package com.yao.springboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by ruijie on 2017/5/24.
 */
@Service
public class UserFunctionService {
    @Autowired
    private FunctionService functionService;

    /*public void setFunctionService(FunctionService functionService){
        this.functionService = functionService;
    }*/

    public String sayHello(String word){
        return functionService.sayHello(word);
    }
 }
