package com.yao.springboot.service;

import com.yao.springboot.annotation.Action;
import org.aspectj.lang.annotation.After;
import org.springframework.stereotype.Service;

/**
 * Created by ruijie on 2017/5/24.
 */
@Service
public class DemoAnnotationSerivce {
    @Action(name = "这个是注解Aop")
    public void add(){
        System.out.println("我被调用了add!");
    }

  /*  @Action(name = "这个是注解Aop")
    public void update(){
        System.out.println("我被调用了upate!");
    }*/
}
