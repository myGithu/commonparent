package com.yao.springboot;

import com.yao.springboot.config.SpringConfig;
import com.yao.springboot.service.DemoAnnotationSerivce;
import com.yao.springboot.service.DemoMethodSerivce;
import com.yao.springboot.service.UserFunctionService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by ruijie on 2017/5/24.
 */
public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext(SpringConfig.class);
        //UserFunctionService userFunctionService = ac.getBean(UserFunctionService.class);
        //System.out.println(userFunctionService.sayHello("config"));
        DemoAnnotationSerivce demoAnnotationSerivce = ac.getBean(DemoAnnotationSerivce.class);
        demoAnnotationSerivce.add();
        DemoMethodSerivce demoMethodSerivce = ac.getBean(DemoMethodSerivce.class);
        demoMethodSerivce.add();
        //demoAnnotationSerivce.update();
        ac.close();
    }
}
