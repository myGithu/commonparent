package com.yao.javabasicsstuding;


import com.yao.utils.date.DateTimeUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Created by ruijie on 2017/7/7.
 * 使用局部内部类实现闹钟
*/
public class AlarmClock {
    private int delay; //延时
    private  boolean flag; //响的状态

    public AlarmClock() {
    }

    public AlarmClock(int delay, boolean flag) {
        this.delay = delay;
        this.flag = flag;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public void start(){
            class DoClock implements ActionListener{
                @Override
                public void actionPerformed(ActionEvent e) {//使用局部内部类来实现动作监听接口
                   String nowTime =  DateTimeUtil.getStringNowTime();
                   System.out.println("当前时间为：" + nowTime);
                   if (AlarmClock.this.flag){
                       Toolkit.getDefaultToolkit().beep();//使用本地硬件发出蜂鸣声
                   }
                }
            }
            new Timer(AlarmClock.this.delay,new DoClock()).start();
    }
}
