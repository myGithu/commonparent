package com.yao.javabasicsstuding;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ruijie on 2017/7/9.
 */
public class ButtonTest extends JFrame{

    private class ColorAction implements ActionListener{
            private Color backgroud;

            public ColorAction(Color backgroud){
                this.backgroud = backgroud;
            }
        @Override
        public void actionPerformed(ActionEvent e) {
           Panel  panel = new Panel();
           panel.setBackground(backgroud);
        }
    }
}
