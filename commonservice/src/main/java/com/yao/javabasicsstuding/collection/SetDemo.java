package com.yao.javabasicsstuding.collection;

import java.util.*;

/**
 * Created by ruijie on 2017/7/10.
 * Set集合学习
 */
public class SetDemo {
    public static void main(String[] args) {
        setDuplicateRemoval();
    }

    /**
     * 利用set集合去除重复
     */
    private static void setDuplicateRemoval() {
        List<String> arrList = new ArrayList<String>();
        arrList.add("a");
        arrList.add("b");
        arrList.add("c");
        arrList.add("a");
        Set<String> set = new HashSet<String>();
        set.addAll(arrList);
        Iterator<String> it = set.iterator();
        while (it.hasNext()){
            System.out.println(it.next() + "");
        }
    }

}
