package com.yao.javabasicsstuding.collection;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ruijie on 2017/7/10.
 * Map集合学习
 */
public class MapDemo {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("1", "a");
        map.put("2", "b");
        map.put("3", "c");
       /* for (Map.Entry<String, String> entry : map.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            System.out.println("key : " + key + "value : " + value);
        }*/
        map.forEach((key,value) -> System.out.println("key: " + key + " vlaue :" + value));
    }
}
