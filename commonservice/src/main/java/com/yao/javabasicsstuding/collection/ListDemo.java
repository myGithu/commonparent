package com.yao.javabasicsstuding.collection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ruijie on 2017/7/10.
 * List集合学习
 */
public class ListDemo {
    public static void main(String[] args) {
        listAddAndSet();
        List<String> list = new ArrayList<>();
    }

    private static void listAddAndSet() {
        String a = "A", b = "B", c = "C", d = "D", e = "E";
        List<String> arrList = new LinkedList<String>();
        arrList.add(a);
        arrList.add("app");
        arrList.add(d);
        arrList.add("app");
        arrList.add(e);
        arrList.add("app");
        Iterator<String> firStrings =  arrList.iterator();
        System.out.println("修改前的集合元素：");
        while (firStrings.hasNext()){
            System.out.println(firStrings.next() + "");
        }
        arrList.set(1,b);
        arrList.add(2,c);
        Iterator<String> scStrings = arrList.iterator();
        System.out.println("修改后的集合元素：");
        while (scStrings.hasNext()){
            System.out.println(scStrings.next() + "");
        }
        System.out.println(arrList.indexOf("app"));
        System.out.println(arrList.lastIndexOf("app"));
    }

}
