package com.yao.javabasicsstuding;


/**
 * 单例模式学习
 * (1) 将采用单例设计模式的类的构造方法私有化（采用private修饰）。

    (2) 在其内部产生该类的实例化对象，并将其封装成private static类型。

    (3) 定义一个静态方法返回该类的实例。
 * Created by ruijie on 2017/7/6.
 */
public class Singleton {
    /**
     * 方法一
     * 单例模式的实现：饿汉式,线程安全 但效率比较低
     */
    // 将自身的实例对象设置为一个属性,并加上Static和final修饰符  final关键字保证线程安全
   /* private static final Emperor ourInstance = new Emperor();

    //返回实例
    public static Emperor getInstance() {
        return ourInstance;
    }
    // 定义一个私有的构造方法
    private Emperor() {
    }*/
   /*
    方法一就是传说的中的饿汉模式
    优点是：写起来比较简单，而且不存在多线程同步问题，避免了synchronized所造成的性能问题；
    缺点是：当类SingletonTest被加载的时候，会初始化static的instance，静态变量被创建并分配内存空间，从这以后，
            这个static的instance对象便一直占着这段内存（即便你还没有用到这个实例），当类被卸载时，静态变量被摧毁，
            并释放所占有的内存，因此在某些特定条件下会耗费内存。
    */


    /**
     *
     *方法二
     * 单例模式的实现：饱汉式,非线程安全
     *
     */
  /* private  Emperor(){}
   //定义一个SingletonTest类型的变量（不初始化，注意这里没有使用final关键字）
   private static Emperor ourInstance;
    // 定义一个静态的方法（调用时再初始化Emperor，但是多线程访问时，可能造成重复初始化问题）
   public static Emperor getInstance(){
       if (ourInstance == null){
           ourInstance = new Emperor();
       }
       return ourInstance;
   }*/
   /*
    方法二就是传说的中的饱汉模式
    优点是：写起来比较简单，当类Emperor被加载的时候，静态变量static的instance未被创建并分配内存空间，
            当getInstance方法第一次被调用时，初始化instance变量，
            并分配内存，因此在某些特定条件下会节约了内存；
    缺点是：并发环境下很可能出现多个Emperor实例。
   */

    /**
     * 方法三
     * 单例模式的实现：饱汉式,线程安全
     *
     */
  /* private Emperor (){}

   // 定义一个SingletonTest类型的变量（不初始化，注意这里没有使用final关键字）
   private static Emperor ourInstance;

    // 定义一个静态的方法（调用时再初始化Emperor，使用synchronized 避免多线程访问时，可能造成重的复初始化问题）
   public static synchronized Emperor getInstance(){//用synchronized保证线程安全
            if(ourInstance == null){
                ourInstance = new Emperor();
            }
        return ourInstance;
   }*/
   /*
    方法三为方法二的简单优化
    优点是：使用synchronized关键字避免多线程访问时，出现多个Emperor实例。
    缺点是：同步方法频繁调用时，效率略低。
   */

    /**
     * 方法四
     * 单例模式最优方案
     * 线程安全  并且效率高
     *
     */
    private Singleton(){};
    // ?  定义一个静态私有变量(不初始化，不使用final关键字，
    // 使用volatile保证了多线程访问时instance变量的可见性，
    // 避免了instance初始化时其他变量属性还没赋值完时，被另外线程调用)
    //用volatile修饰的变量，线程在每次使用变量的时候，都会读取变量修改后的值。
    // volatile很容易被误用，用来进行原子性操作。
   private static volatile Singleton ourInstance;
   //定义一个共有的静态方法，返回该类型实例
   public static Singleton getInstance(){
       // 对象实例化时与否判断（不使用同步代码块，instance不等于null时，直接返回对象，提高运行效率）
       if (ourInstance == null){
           //同步代码块（对象未初始化时，使用同步代码块，保证多线程访问时对象在第一次创建后，不再重复被创建）
           synchronized (Singleton.class){
               //未初始化，则初始instance变量
                if (ourInstance == null){
                    ourInstance = new Singleton();
                }
           }
       }
       return  ourInstance;
   }
    /*方法四为单例模式的最佳实现。内存占用地，效率高，线程安全，多线程操作原子性*/

    public synchronized void show(){
        System.out.println("aaaa");
    }
    public static void main(String[] args) {
        Singleton emperor = Singleton.getInstance();
        emperor.show();
    }

}
