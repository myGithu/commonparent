package com.yao.javabasicsstuding.exception;

/**
 * Created by ruijie on 2017/7/10.
 */
public class Test {
    public static void showArea(Double r) throws NewException {
        if(r <= 20 ){
            throw new NewException(r);
        }
        System.out.println("圆的面积是：" + 3.14 * r * r);
    }

    public static void main(String[] args) {
        try {
            showArea(2.0D);
        } catch (NewException e) {
            e.printStackTrace();
        }
    }
}
