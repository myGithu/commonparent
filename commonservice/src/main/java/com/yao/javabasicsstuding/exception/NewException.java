package com.yao.javabasicsstuding.exception;

import com.sun.org.apache.bcel.internal.generic.NEW;

/**
 * Created by ruijie on 2017/7/10.
 */
public class NewException extends  Exception {
    public NewException(Double d){
        System.out.println("圆的半径不能小于20");
        System.out.println("圆的半径为：" + d);
    }
}
