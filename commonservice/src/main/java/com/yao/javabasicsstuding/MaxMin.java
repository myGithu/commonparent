package com.yao.javabasicsstuding;

/**
 * Created by ruijie on 2017/7/7.
 * 使用静态内部类求极限值
 */
public class MaxMin {

    public static class Result{
        private  Double max;
        private  Double min;

        public Result() {
        }

        public Result(Double max, Double min) {
            this.max = max;
            this.min = min;
        }

        public Double getMax() {
            return max;
        }

        public void setMax(Double max) {
            this.max = max;
        }

        public Double getMin() {
            return min;
        }

        public void setMin(Double min) {
            this.min = min;
        }

    }
    public static Result getResult(Double[] arr){
        if (arr.length <= 0){
                return null;
        }
        Double max = arr[0];
        Double min = arr[0];
        for (Double aDouble : arr) {
            if (aDouble > max){
                max = aDouble;
            }
            if(aDouble < min){
                min = aDouble;
            }
        }
        return  new Result(max, min);//初始化静态内部类
    }
}
