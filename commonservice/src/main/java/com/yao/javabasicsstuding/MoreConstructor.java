package com.yao.javabasicsstuding;

/**
 * Created by ruijie on 2017/7/9.
 * 反射学习
 */
public class MoreConstructor {
    private  String ss = "值ss";
    String s = "值s";
    int i = 100, i1 = 200, i2 = 300;

    private MoreConstructor() {

    }

    protected MoreConstructor(String s, int i) {
        this.s = s;
        this.i = i;
    }

    public MoreConstructor(String... strings) throws NumberFormatException{
        if (0 < strings.length) {
            this.i = Integer.valueOf(strings[0]);
        }
        if (1 < strings.length) {
            this.i1 = Integer.valueOf(strings[1]);
        }
        if (2 < strings.length) {
            this.i2 = Integer.valueOf(strings[2]);
        }
    }

   /* public void print(){
        System.out.println("ss=" + ss);
        System.out.println("s=" + s);
        System.out.println("i=" + i);
        System.out.println("i1=" + i1);
        System.out.println("i2=" + i2);
    }*/

    static  void staticMethod(){
        System.out.println("执行staticMethod");
    }

    public int publicMethod (int i){
        System.out.println("执行publicMethod");
        return  i * 100;
    }

    protected int protectedMethod(String s, int i) throws  NumberFormatException{
        System.out.println("执行protectedMethod");
        return Integer.valueOf(s) * i;
    }
    private  String privateMethod(String... strings){
        System.out.println("执行privateMethod");
        StringBuilder stringBuilder = new StringBuilder();
        for (int i3 = 0; i3 < strings.length; i3++) {
                stringBuilder.append(strings[i]);
        }
        return stringBuilder.toString();
    }




}
