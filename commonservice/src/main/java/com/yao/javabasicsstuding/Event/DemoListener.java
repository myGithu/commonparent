package com.yao.javabasicsstuding.Event;

import java.util.EventListener;

/**
 *  事件监听接口
 * Created by ruijie on 2017/7/9.
 */
public interface DemoListener extends EventListener {
    public void demoEvent(DemoEvent demoEvent);//绑定事件方法
}
