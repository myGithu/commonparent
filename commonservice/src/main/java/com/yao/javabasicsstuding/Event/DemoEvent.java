package com.yao.javabasicsstuding.Event;

import java.util.EventObject;

/**
 * 事件触发时处理类
 * Created by ruijie on 2017/7/9.
 *
 */
public class DemoEvent extends EventObject{
    private Object object;
    private String name;
    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public DemoEvent(Object source,String name) {
        super(source);
        this.object = source;
        this.name = name;
    }

    public Object getSource() {
        return object;
    }

    public String getName() {
        return name;
    }

    public void say(){
        System.out.println("这个是say()方法！");
    }
}
