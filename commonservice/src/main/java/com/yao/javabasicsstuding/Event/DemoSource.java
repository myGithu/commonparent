package com.yao.javabasicsstuding.Event;

import javax.swing.plaf.basic.BasicOptionPaneUI;
import java.util.Enumeration;
import java.util.Vector;

/**
 * 使用事件的类 (事件源)
 * 该类实现了监听器的添加和监听器方法的执行，并且实现了由于属性的改变而执行事件
 * 在添加、删除、执行监听器的时候都要注意同步问题
 * Created by ruijie on 2017/7/9.
 */
public class DemoSource {
    private Vector repository = new Vector();//同步集合
    private DemoListener demoListener;
    private String name;

    public DemoSource() {
    }

    //注册监听  如果这里没有使用Vector而是使用ArrayList那么请注意同步问题
    public void addDemoListener(DemoListener demoListener){
            repository.addElement(demoListener);//这步要注意同步问题
    }

    //通知处理  如果这里没有使用Vector而是使用ArrayList那么请注意同步问题
    public void notifyDemoEvent(DemoEvent demoEvent){
        Enumeration enumeration =  repository.elements();
        while (enumeration.hasMoreElements()){
             demoListener = (DemoListener) enumeration.nextElement();//获得添加的事件监听
             demoListener.demoEvent(demoEvent);//将事件监听和事件监听进行绑定
        }
    }
    //移除事件监听 如果这里没有使用Vector而是使用ArrayList那么请注意同步问题
    public void removeDemoListener(DemoListener demoListener){
        repository.remove(demoListener);
    }

    public void setName(String sName){//
        boolean flag = false;//触发事件的开关变量
        if(this.name == null && sName != null){
            flag = true;
        }else if(this.name != null && sName == null){
            flag = true;
        }else if(!sName.equals(name)){
            flag = true;
        }
        this.name = sName;
        //如果改变则执行事件
        if (flag){
            //通知事件处理类来处理事件
            this.notifyDemoEvent(new DemoEvent(this,name));
        }

    }

    public String getName(){
        return this.name;
    }

}
