package com.yao.javabasicsstuding.Event;

/**
 *  测试类
 *  测试了由于改变属性而引起的事件发生
 * Created by ruijie on 2017/7/9.
 */
public class DemoTest implements  DemoListener{
    private DemoSource demoSource;

    public DemoTest(String name) {
        demoSource = new DemoSource();
        demoSource.addDemoListener(this);
        System.out.println("添加监听器完毕");
        demoSource.setName(name);
    }

    @Override
    public void demoEvent(DemoEvent demoEvent) {
      //  System.out.println("事件处理方法");
        System.out.println(demoEvent.getName());
        //System.out.println("我是事件触发后要执行的结果！");
    }

    public static void main(String[] args) {
        new DemoTest("niaho1123");
    }
}
