package com.yao.javabasicsstuding;

import org.springframework.util.StringUtils;

import javax.sound.midi.Soundbank;
import java.lang.reflect.*;
import java.text.Normalizer;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ruijie on 2017/6/19.
 */
public class MyApp {
    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException, InstantiationException {
     /*   int a = 8,b = 1;
      *//*  a = a ^ b ;
        b = a ^ b;   任意两个数与另一个数异或运算两次等于他本身
        a = a ^ b;*//*
        a = a ^ b ^ b;
        System.out.println(a);*/
        //  System.out.println(21 << 4 );
       /* System.out.println(21 * 16);*/
       /*int a = 1 ,b = 12;
        System.out.println(a >= b ? a : b);*/

       /* *
        * 去掉break  匹配之后的所有值都会被输出
        *
        *
        *
        * */
      /* int week = 2;
       switch (week){
           case 1 :
               System.out.println("Monday");
               break;
           case 2 :
               System.out.println("Tuesday");
               break;
           case 3 :
               System.out.println("Wednesday");
               break;
           default:
               System.out.println("Sorry,I don't Know");
       }*/

        /**
         * 看VC生成的汇编码，for比while要快一些。for(;;)是直接跳转，while多了两条指令而已。
         不过C++标准里并未规定编译器的实现方式，非VC平台以及以后新版的VC二者速度相比如何还未可知。
         而且说句实话，我蛮讨厌for(;;)代替while(true)的。不直观。
         我也不在乎这么丁点儿效率。至少我写的程序中没遇到需要优化到这种程度的代码。
         1. 两种循环在构造死循环时的区别
         用while构造死循环时，
         一般会使用while(TRUE)来构造死循环；而用for来构造死循环时，则使用for(;;)来构造死循环。
         这两个死循环的区别是：while循环里的条件被看成表达式，因此，当用while构造死循环时，里面的TRUE实际上被看成永远为真的表达式，这种情况容易产生混淆，有些工具软件如PC-Lint就会认为出错了，因此构造死循环时，最好使用for(;;)来进行。
         2. 两种循环在普通循环时的区别
         对一个数组进行循环时，一般来说，如果每轮循环都是在循环处理完后才讲循环变量增加的话，
         使用for循环比较方便；如果循环处理的过程中就要将循环变量增加时，则使用while循环比较方便；
         还有在使用for循环语句时，如果里面的循环条件很长，可以考虑用while循环进行替代，
         使代码的排版格式好看一些。
         *
         */
        /* for (;;){//死循环与while 相对来说 for的效率更高  其实差不多
                System.out.println("a");
           }*/
       /* for (int i = 1; i <= 9; i++){//99
            for (int j=1; j <= i; j++){
                System.out.print(i + "*" + j + "=" +i * j+"\t");
            }
            System.out.print("\n");
        }*/
        // int a[] = {1,2222,356,4,52,8,9};
        /*Arrays.fill(a,2,4,10);
        for (int i = 0,j = a.length; i < j; i++) {
            System.out.println(a[i]);
            }*/
      /*  Arrays.sort(a);
        for (int i = 0,j = a.length; i < j; i++) {
            System.out.println(a[i]);
            }*/
        //  String a = "angkdmsdk" ;
        //   System.out.println(a.replace(" ",""));
        //    System.out.println(a.toUpperCase());
     /*   String a = "Mrkj001";
       String regStr = "^\\p{Upper}\\p{Lower}{3}\\d{3}$";
            System.out.println(a.matches(regStr));
            String regEmail = "^\\w{1,}\\@\\w{0,}\\.{1}\\w{0,}$";//邮箱匹配
            String email = "1@qwer.com";
            System.out.println(email.matches(regEmail));*/
        //  String  regTel = "^1[3,4,5,7,8]\\d{9}$";//手机号码匹配
        // 按指定模式在字符串查找
       /* String line = "This order was placed for QT3000! OK?";
        String pattern = "(\\D*)(\\d+)(.*)";

        // 创建 Pattern 对象
            Pattern r = Pattern.compile(pattern);
        // 现在创建 matcher 对象
            Matcher m = r.matcher(line);
        if (m.find( )) {
            System.out.println("Found value: " + m.group(0) );
            System.out.println("Found value: " + m.group(1) );
            System.out.println("Found value: " + m.group(2) );
            System.out.println("Found value: " + m.group(3) );
        } else {
            System.out.println("NO MATCH");
        }*/
           /* String s = "1234";
            StringBuilder sb = new StringBuilder(s);
            System.out.println(sb.reverse().toString());*/
        /* String c = "a";
         String d = "a";
         String a = new String ("a");
         String b = new String ("a");
         System.out.println(a.equals(b));*/
       /* Student a = new Student("a","10","男");
         Student b = new Student("a","10","男");
         Student c = new Student("a","10","男");
         System.out.println(a.hashCode() + "-----" + b.hashCode() +"-----" + c.hashCode());
         System.out.println(a.equals(b));*/
        // moveDis(3,'A','B','C');
        // Object o = new Object();
        //MyApp.MyApp2 a = new MyApp().new MyApp2();
         /*AlarmClock alarmClock = new AlarmClock(1000,false);
         alarmClock.start();*/
        /* try {
             Double[] d =  new Double[5];
             System.out.println(MaxMin.getResult(d).getMax());
             System.out.println(MaxMin.getResult(d).getMin());
             } catch (Exception e) {
             e.printStackTrace();
             }
         */
        //获取一个对象的 类对象的多种方法
        //Class moreConstructor = MoreConstructor.class;
       /* Class moreConstructor = null;
        try {
            moreConstructor = Class.forName("com.yao.javabasicsstuding.MoreConstructor");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }*/
        MoreConstructor moreConstructor_ = new MoreConstructor();
        Class moreConstructor = moreConstructor_.getClass();
        //visitConstructor(moreConstructor);//利用反射访问对象的构造方法
        //visitFields(moreConstructor_, moreConstructor);//利用反射访问属性
        //visitMethod(moreConstructor_, moreConstructor);//利用反射访问对象的方法

    }

    private static void visitMethod(MoreConstructor moreConstructor_, Class moreConstructor)
            throws IllegalAccessException, InvocationTargetException {
        Method[] methods = moreConstructor.getDeclaredMethods();//获取所有的方法
        for (int i = 0; i < methods.length; i++) {
            Method method = methods[i];
            System.out.println("方法名称：" + method.getName());
            System.out.println("是否允许带有可以变量的参数：" + method.isVarArgs());
            Class[] parameterTypes = method.getParameterTypes();//获取所有的参数类型
            method.setAccessible(true);
            for (Class parameterType : parameterTypes) {
                System.out.println(parameterType + " ");
            }
            if (i == 0) {
                method.invoke(moreConstructor_);
            } else if (i == 1) {
                System.out.println("返回值是：" + method.invoke(moreConstructor_,168));
            } else if (i == 2) {
                System.out.println("返回值是：" + method.invoke(moreConstructor_,"7",5) );
            }else{
                Object[] parameters = new Object[]{new String []{"A","B","C"}};
                System.out.println("返回值是：" + method.invoke(moreConstructor_,parameters));
            }

        }
    }

    private static void visitFields(MoreConstructor moreConstructor_, Class moreConstructor) throws IllegalAccessException {
        Field[] fields = moreConstructor.getDeclaredFields();//获取所有的字段
        for (int i = 0, j = fields.length; i < j; i++) {
            Field field = fields[i];
            System.out.println("属性的名称：" + field.getName());
            Class fieldType = field.getType();
            System.out.println("属性的类型：" + fieldType);
            field.setAccessible(true);//允许访问私有成员变量
            System.out.println("初始值为：" + field.get(moreConstructor_));
            if (int.class.equals(fieldType)) {//给属性设置值
                field.setInt(moreConstructor_, 600);
            }
            if (String.class.equals(fieldType)) {
                field.set(moreConstructor_, "我的值变了");
            }
        }
        //moreConstructor_.print();
    }


    private static void visitConstructor(Class moreConstructor) {//利用反射访问对象的构造方法
        Constructor[] constructors = moreConstructor.getDeclaredConstructors();//获取所有的构造方法
        for (int i = 0, j = constructors.length; i < j; i++) {//遍历所有的构造方法
            Constructor constructor = constructors[i];
            constructor.setAccessible(true);//允许访问私有构造方法
            System.out.println("是否带有可变量产参数" + constructor.isVarArgs());
            /* int modifiers =  constructor.getModifiers(); //获取修饰符参数
             System.out.println(Modifier.isPrivate(modifiers));
             System.out.println(Modifier.isPublic(modifiers));
             System.out.println(Modifier.isProtected(modifiers));*/
            System.out.println("该构造方法的参数依次是：");
            Class[] parameterTypes = constructor.getParameterTypes();//获取构造方法的参数类型
            for (int i1 = 0, k = parameterTypes.length; i1 < k; i1++) {
                System.out.println(" " + parameterTypes[i]);
            }
            MoreConstructor moreConstructor1 = null;//将MoreConstructor对象滞空
            try {//利用反射实例化对象
                if (i == 0) {
                    Object[] parameters = new Object[]{new String[]{"100", "200", "300"}};
                    moreConstructor1 = (MoreConstructor) constructor.newInstance(parameters);
                } else if (i == 1) {
                    moreConstructor1 = (MoreConstructor) constructor.newInstance("7", 5);
                } else {
                    moreConstructor1 = (MoreConstructor) constructor.newInstance();
                }
            } catch (Exception e) {
                System.out.println("再创建对象时抛出异常，下面执行setAccessible()");
            }
            // moreConstructor1.print();
        }
    }

    int c = 100;
    private MyApp2 a = new MyApp2();

    class MyApp2 {

        private String b;

        public void a() {

        }
    }

    /**
     * 汉诺塔问题
     *
     * @param l 盘子个数
     * @param a 柱子A
     * @param b 柱子B
     * @param c 柱子C
     */
    public static void moveDis(int l, char a, char b, char c) {
        if (l == 1) {
            System.out.println("从" + a + "移动1号盘子到" + c);
        } else {
            moveDis(l - 1, a, c, b);
            System.out.println("从" + a + "移动" + l + "号盘子到" + c);
            moveDis(l - 1, c, a, b);
        }
    }
}
