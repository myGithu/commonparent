package com.yao.javabasicsstuding.io;

import java.io.*;

/**
 * Created by ruijie on 2017/7/10.
 * 带有缓冲的IO流
 */
public class BufferedIO {
    private BufferedIO() {
    }

    public static void bufferedWrite(File file, String context) {
        FileWriter fw = null;
        BufferedWriter out = null;
        try {
            fw = new FileWriter(file,true);
            out = new BufferedWriter(fw);
            out.write(context.trim());
            out.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            writerClose(fw, out);
        }
    }

    private static void writerClose(FileWriter fw, BufferedWriter out) {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
    }

    public static String bufferedRead(File file) {
        String str = null;
        StringBuilder context = new StringBuilder();
        FileReader fr = null;
        BufferedReader in = null;
        try {
            fr = new FileReader(file);
            in = new BufferedReader(fr);
            while ((str = in.readLine()) != null) {
                context.append(str.trim() + "\n");
            }
            return context.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            readClose(fr, in);
        }
        return null;
    }

    private static void readClose(FileReader fr, BufferedReader in) {

            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fr != null) {
                try {
                    fr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
    }


}
