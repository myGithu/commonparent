package com.yao.javabasicsstuding.io;

import java.io.File;
import java.io.IOException;

/**
 * Created by ruijie on 2017/7/10.
 */
public class IOTest {
    public static void main(String[] args) {
        //test1();
        //test2();
       // test3();
        //test4();
        try {
            BufferedIO.bufferedWrite(FileDemo.createNewFile("d:/","test.pdf"),"这个是pdf文档！");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void test4() {
        try {
             System.out.println(DataIO.readDataIO(FileDemo.createNewFile("d:/","text.txt")));
         } catch (IOException e) {
             e.printStackTrace();
         }
    }

    private static void test3() {
        try {
             DataIO.dataIOWrite(FileDemo.createNewFile("d:/","text.txt"),"打发阿斯蒂芬阿斯顿dataIOasdf学习中");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void test2() {
        File file = null;
        try {
            file = FileDemo.createNewFile("d:/", "text.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedIO.bufferedWrite(file, "这个是整的吗的吗???zhehsi");
        System.out.println(BufferedIO.bufferedRead(file));
    }

    private static void test1() {
        File file = null;
        try {
            file = FileDemo.createNewFile("d:/", "text.txt");
            FileDemo.write(file, "这个是文件系统?????");
            System.out.println(FileDemo.read(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
       /* System.out.println("文件名是：" + file.getName());
        System.out.println("文件长度是：" + file.length());
        System.out.println("判断文件是否为隐藏文件：" + file.isHidden());*/
    }
}
