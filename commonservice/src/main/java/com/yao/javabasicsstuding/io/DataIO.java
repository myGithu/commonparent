package com.yao.javabasicsstuding.io;

import java.io.*;

/**
 * Created by ruijie on 2017/7/11.
 */
public class DataIO {
    private DataIO() {
    }

    public static void dataIOWrite(File file, String context) {
        FileOutputStream fs = null;
        DataOutputStream out = null;
        try {
            fs = new FileOutputStream(file, true);
            out = new DataOutputStream(fs);
            out.writeUTF(context + "\r\n");
           // out.writeChars(context.trim() + "\r\n");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            writeClose(fs, out);
        }
    }

    private static void writeClose(FileOutputStream fs, DataOutputStream out) {

            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fs != null) {
                try {
                    fs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

    }

    public static String readDataIO(File file) {
        FileInputStream fs = null;
        DataInputStream in = null;
        try {
            fs = new FileInputStream(file);
            in = new DataInputStream(fs);
            /* String str = null;
            StringBuilder context = new StringBuilder();
           while ((str = in.readUTF())!= null){
                System.out.println(str);
                   // context.append(str + "\n");
            }*/
          /*  byte[] bytes = new byte[1024];
            int len = in.read(bytes);
            String context = new String (bytes,0,len);*/
          StringBuilder context = new StringBuilder();
          while (in.available() > 0){
                context.append(in.readUTF());
          }
          return context.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            readClose(fs, in);
        }
        return null;
    }

    private static void readClose(FileInputStream fs, DataInputStream in) {

            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(fs != null){
                try {
                    fs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }



