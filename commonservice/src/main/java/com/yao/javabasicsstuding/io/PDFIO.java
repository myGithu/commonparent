package com.yao.javabasicsstuding.io;


import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by ruijie on 2017/7/19.
 */
public class PDFIO {
    public static void main(String[] args) {
        createPDF();
    }

    public static  void createPDF(){
        Document document = new  Document();
        try {
            PdfWriter.getInstance(document,new FileOutputStream("d:/test.pdf"));
            document.open();
            document.add(new Paragraph("test pdf document!"));
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }finally {
            document.close();
        }
    }

}
