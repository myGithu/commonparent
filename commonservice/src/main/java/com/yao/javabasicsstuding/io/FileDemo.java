package com.yao.javabasicsstuding.io;

import java.io.*;

/**
 * Created by ruijie on 2017/7/10.
 * FileOutputStream（write）和FileInputStream（read）字节输出流和字节输入流
 * 因为汉字是两个字节 操作不好可能会出现乱码现象
 */
public class FileDemo {
    private FileDemo(){}

    public static File createNewFile(String filePath, String fileName) throws IOException {
        File file = new File(filePath, fileName);//创建文件对象
        if (file.exists()) {//判断文件是否存在
            return file;
        }
        file.createNewFile();
        return file;
    }

    /**
     * 使用字节流写入文件
     * @param file 文件
     * @param context 写入文件的内容
     * @throws IOException
     */
    public static void writeFile(File file, String context) throws IOException {
        FileOutputStream out = new FileOutputStream(file);
        byte[] bytes = context.getBytes();//将字节数组中信息写入到文件中
        out.write(bytes);
        out.close();
    }

    /**
     * 使用字节流读取文件
     * @param file 文件
     * @return 获取文件的内容
     * @throws Exception
     */
    public static String readFile(File file) throws IOException {
        FileInputStream in = new FileInputStream(file);
        byte[] bytes = new byte[1024];//创建字节数组
        int len = in.read(bytes);//读取文件中的信息 返回信息的长度
        in.close();
        String context = new String(bytes, 0, len);//将字节数字中的信息转化成String显示
        return context;
    }

    /**
     * 使用字符流写入文件
     * @param file
     * @param context
     * @throws IOException
     */
    public static void write(File file, String context) throws IOException {
        FileWriter out = new FileWriter(file);
        out.write(context);
        out.close();
    }

    /**
     *
     * 使用字符流读取文件
     * @param file
     * @return
     * @throws IOException
     */
    public static String read(File file) throws IOException {
        FileReader in = new FileReader(file);
        char[] chars = new char[1024];
        int len = in.read(chars);
        in.close();
        String context = new String(chars,0,len);
        return  context;
    }


}
