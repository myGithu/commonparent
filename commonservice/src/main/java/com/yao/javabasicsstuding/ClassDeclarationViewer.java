package com.yao.javabasicsstuding;


import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.Date;

/**
 * Created by ruijie on 2017/7/9.
 * 反射学习
 */
public class ClassDeclarationViewer {
    public static void main(String[] args) throws ClassNotFoundException {
        // getClazzViewer();
        //invokeMethod();
        System.out.println(new ClassDeclarationViewer().toString(new Date()));
    }


    /**
     * 利用反射调用方法
     */
    private static void invokeMethod() {
        System.out.println("调用的Math的静态方法sin()");
        try {
            Method method = Math.class.getDeclaredMethod("sin", Double.TYPE);
            Double sin1 = (Double) method.invoke(null, new Double(1.0D));
            System.out.println("sin值为：" + sin1);
            System.out.println("调用String类的非静态方法equals()");
            Method eq =  String.class.getDeclaredMethod("equals", Object.class);//getDeclaredMethod 第二参数是填写该方法的参数类型
            boolean isTrue = (boolean) eq.invoke("明日科技","明日科技");
            System.out.println(isTrue);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 利用反射查看类的成员
     *
     * @throws ClassNotFoundException
     */
    private static void getClazzViewer() throws ClassNotFoundException {
        Class<?> clazz = Class.forName("java.util.ArrayList");
        System.out.println("获取类的标准名称：" + clazz.getCanonicalName());//获取类的全名
        System.out.println("获取类的简称：" + clazz.getSimpleName());
        System.out.println(Modifier.toString(clazz.getModifiers()));
        //输出类的泛型参数
        System.out.println("泛型参数为：");
        TypeVariable<?>[] typeVariables = clazz.getTypeParameters();
        for (TypeVariable<?> variable : typeVariables) {
            System.out.println(variable + "\t");
        }
        System.out.println("该类直接实现的所有接口是：");
        Type[] types = clazz.getGenericInterfaces();
        for (Type type : types) {
            System.out.println(type + "\t");
        }
        System.out.println("该类直接击继承的父类是：");
        Type type = clazz.getGenericSuperclass();
        System.out.println(type);
        System.out.println("输出该类的所有注解：");
        Annotation[] annotations = clazz.getAnnotations();
        for (Annotation annotation : annotations) {
            System.out.println(annotation + "\t");
        }
    }

    /**
     * 利用反射重写toString方法
     * @param object
     * @return
     */
    public String toString(Object object){
        Class clazz = object.getClass();
        StringBuilder sb = new StringBuilder();
        Package packagez =  clazz.getPackage();//获取包名
        sb.append("包名是：" +  packagez + "\t");
        String className = clazz.getSimpleName();//获取类名
        sb.append("类名是：" + className + "\t");
        return  sb.toString();

    }
}
