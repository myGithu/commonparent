package com.yao.tenstuding.tenpay.reqandres;


import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.yao.utils.annotation.XStreamCDATA;

/**
 * 微信支付请求参数 Bean
 * 
 * @author Administrator
 *
 */

@XStreamAlias("xml")
public class TenPayRequest {
	// 微信公众号ID （是）
	@XStreamAlias("appId")
	private String appId;
	// 商品描述 （是）
	@XStreamCDATA
	private String body;
	// 商户号 （是）
	@XStreamAlias("mch_id")
	private String mchId;
	// 随机字符串 < 32 （是）
	@XStreamAlias("nonce_str")
	private String nonceStr;
	// 回调地址 （是）
	@XStreamAlias("notify_url")
	private String notifyUrl;
	// 商品订单号（是）
	@XStreamAlias("out_trade_no")
	private String outTradeNo;
	// 订单生成ip地址 （是）
	@XStreamAlias("spbill_create_ip")
	private String spbillCreateIp;
	// 交易金额 单位分 （是）
	@XStreamAlias("total_fee")
	private String totalFee;
	// 交易类型 JSAPI、NATIVE、APP（是）
	@XStreamAlias("trade_type")
	private String tradeType;
	// 签名（是）
	@XStreamCDATA
	private String sign;

	public TenPayRequest() {
	}

	public TenPayRequest(String appId, String body, String mchId, String nonceStr, String notifyUrl, String outTradeNo,
			String spbillCreateIp, String totalFee, String tradeType) {
		super();
		this.appId = appId;
		this.body = body;
		this.mchId = mchId;
		this.nonceStr = nonceStr;
		this.notifyUrl = notifyUrl;
		this.outTradeNo = outTradeNo;
		this.spbillCreateIp = spbillCreateIp;
		this.totalFee = totalFee;
		this.tradeType = tradeType;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getSpbillCreateIp() {
		return spbillCreateIp;
	}

	public void setSpbillCreateIp(String spbillCreateIp) {
		this.spbillCreateIp = spbillCreateIp;
	}

	public String getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

}
