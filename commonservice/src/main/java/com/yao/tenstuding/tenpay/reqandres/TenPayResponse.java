package com.yao.tenstuding.tenpay.reqandres;

import com.thoughtworks.xstream.annotations.XStreamAlias;
/**
 * 微信支付返回参数 Bean
 * @author Administrator
 *
 */
@XStreamAlias("xml")
public class TenPayResponse {
	// 返回状态码
	@XStreamAlias("return_code")
	private String returnCode;
	// 返回信息
	@XStreamAlias("return_msg")
	private String returnMsg;
	// 公众账号ID
	private String appid;
	// 商户号
	@XStreamAlias("mch_id")
	private String mchId;
	// 随机字符串
	@XStreamAlias("nonce_str")
	private String nonceStr;
	// 签名
	private String sign;
	// 业务结果
	@XStreamAlias("result_code")
	private String resultCode;
	// 二维码链接
	@XStreamAlias("code_url")
	private String codeUrl;
	// 交易类型
	@XStreamAlias("trade_type")
	private String tradeType;
	// 预支付交易会话标识
	@XStreamAlias("prepay_id")
	private String prepayId;

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMsg() {
		return returnMsg;
	}

	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getCodeUrl() {
		return codeUrl;
	}

	public void setCodeUrl(String codeUrl) {
		this.codeUrl = codeUrl;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public String getPrepayId() {
		return prepayId;
	}

	public void setPrepayId(String prepayId) {
		this.prepayId = prepayId;
	}

}
