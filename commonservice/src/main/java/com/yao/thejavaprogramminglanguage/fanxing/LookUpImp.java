package com.yao.thejavaprogramminglanguage.fanxing;

/**
 * Created by ruijie on 2017/7/12.
 */
public class LookUpImp<T> implements  LookUp<T> {
    @Override
    public T find(String name) {
        if(!name.isEmpty()){
            return (T) name;
        }
        return null;
    }

    public static void main(String[] args) {
        LookUp<String> lookUp = new LookUpImp<String>();
        System.out.println(lookUp.find("aaaa"));
    }
}
