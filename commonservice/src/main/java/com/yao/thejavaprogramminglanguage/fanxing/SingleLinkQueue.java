package com.yao.thejavaprogramminglanguage.fanxing;

/**
 * Created by ruijie on 2017/7/12.
 */
public class SingleLinkQueue<E> {
    protected Cell<E> head;
    protected Cell<E> tail;

    public void add(E item) {
        Cell<E> cell = new Cell<E>(item);
        if (tail == null) {
            head = tail = cell;
        } else {
            cell.setNext(cell);
            tail = cell;
        }
    }

    public E remove() {
        if (head == null) {
            return null;
        }
        Cell<E> cell = head;
        head = head.getNext();
        if (head == null){
            tail = null;
        }
        return  cell.getElement();
    }

    public <T>  T[] toArray(T[] arr){
            Object[] tem = arr;
            int i = 0;
            for(Cell<E> c = head; c != null && i < arr.length; c = c.getNext()){
                    tem[i++] = c.getElement();
            }
            return  arr;
    }

}
