package com.yao.thejavaprogramminglanguage.fanxing;

/**
 * Created by ruijie on 2017/7/12.
 */
public interface LookUp<T> {
       T  find(String name);
}
