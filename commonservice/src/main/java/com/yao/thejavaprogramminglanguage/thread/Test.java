package com.yao.thejavaprogramminglanguage.thread;

/**
 * Created by ruijie on 2017/7/16.
 * 练习题一个线程每秒钟打印一次重执行开始到目前为止所运行的时间，，第二个线程每隔15秒打印一条消息，而且每隔一秒钟
 * 会通知消息线程进行消息打印，在不改变的线程的前提下，在增加一个每隔7秒钟打印另一条消息的线程
 */
public class Test {
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (; ; ) {
                    try {
                        synchronized (this) {
                            System.out.println("我是打印的消息！");
                            wait(15000);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (; ; ) {
                    try {

                        synchronized (this) {
                            wait(1000);
                            long endTime = System.currentTimeMillis();
                            System.out.println("项目运行时间：" + (endTime - startTime));
                            notifyAll();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (; ; ) {
                    try {
                        synchronized (this) {
                            System.out.println("我是另一个消息！");
                            wait(7000);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }
}
