package com.yao.thejavaprogramminglanguage.thread;

/**
 * Created by ruijie on 2017/7/16.
 * 死锁
 */
public class Frindly {
    private Frindly partner;
    private String name;

    public Frindly(String name) {
        this.name = name;
    }

    public synchronized void hug() {
        System.out.println(Thread.currentThread().getName() + " in " + this.name + ".hug() trying to" +
                " invoke " + partner.name + ".hugBack()");
        partner.hugBack();
    }

    private synchronized void hugBack() {
        System.out.println(Thread.currentThread().getName() + " in " + name + ".hugBack()");
    }

    public void becomeFrind(Frindly partner) {
        this.partner = partner;
    }

    public static void main(String[] args) {
        Frindly a = new Frindly("a");
        Frindly b = new Frindly("b");
        a.becomeFrind(b);
        b.becomeFrind(a);

        new Thread(new Runnable() {
            @Override
            public void run() {
                a.hug();
            }
        }, "ThreadA").start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                b.hug();
            }
        },"ThreadB").start();
    }

}
