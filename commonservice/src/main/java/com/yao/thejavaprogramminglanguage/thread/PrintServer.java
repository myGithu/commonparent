package com.yao.thejavaprogramminglanguage.thread;

/**
 * Created by ruijie on 2017/7/16.
 */
public class PrintServer extends Thread{
    private int delay;
    private String msg;

    public PrintServer() {
    }

    public PrintServer(int delay) {
        this.delay = delay;
    }

    public PrintServer(int delay, String msg) {
        this.delay = delay;
        this.msg = msg;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public void run() {
        for (;;){
            synchronized (this){
            System.out.println(msg);
            try {
                wait(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        }
    }

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        PrintServer p1 =  new PrintServer(1000);
        PrintServer p2 = new PrintServer(15000,"我要打印一条消息");
        p2.start();
        long endTime = System.currentTimeMillis();
        p1.setMsg("程序运行时间为：" + (endTime - startTime));
        p1.start();
    }
}
