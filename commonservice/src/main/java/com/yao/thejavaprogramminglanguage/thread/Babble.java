package com.yao.thejavaprogramminglanguage.thread;

import java.util.Scanner;

/**
 * Created by ruijie on 2017/7/16.
 */
public class Babble extends  Thread {
    static boolean doYield;
    static int howOften;
    private String word;

    Babble(String whatToSay){
        this.word = whatToSay;
    }

    @Override
    public void run() {
        for (int i = 0; i < howOften; i++) {
            System.out.println(this.word);
            if (doYield){
                Thread.yield();
            }
        }
    }

    public static void main(String[] args) {
        Babble.doYield = true;
        Babble.howOften = 10;
        String[] s = new String[]{String.valueOf(Babble.doYield),String.valueOf(Babble.howOften)};
        for (int i = 0; i < s.length; i++) {
            new Babble(s[i]).start();
        }  
    }
}
