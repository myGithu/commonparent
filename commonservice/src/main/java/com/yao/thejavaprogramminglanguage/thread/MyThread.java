package com.yao.thejavaprogramminglanguage.thread;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ruijie on 2017/7/13.
 */
public class MyThread extends Thread {

    private String wrod;
    private int delay;

    public MyThread() {
    }

    @Override
    public void run() {
        try {
            for (; ;) {
                System.out.println(this.wrod+ ": " + Thread.currentThread().getName());
                System.out.println(wrod + "  ");
                Thread.sleep(delay);
            }
        } catch (InterruptedException e) {
            return;
        }
    }

    public MyThread(String wrod, int delay) {
        this.wrod = wrod;
        this.delay = delay;
    }

    public String getWrod() {
        return wrod;
    }

    public void setWrod(String wrod) {
        this.wrod = wrod;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public static void main(String[] args) {
        new MyThread("ping", 100).start();
        new MyThread("PING", 1000).start();
        String m = Thread.currentThread().getName();
        System.out.println("这个名字有点6：" + m);
    }

}
