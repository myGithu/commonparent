package com.yao.javalanguage.paixu;


/**
 * Created by ruijie on 2017/7/18.
 *
 */
public class BubbleSort {
    //冒泡排序
    public static void bubbleSort(int[] list) {
        boolean needNextPass = true;
        int size = list.length;
        for (int i = 1; i < size && needNextPass; i++) {
                needNextPass = false;
                for (int j = 0; j < size - i; j++ ){
                    if (list[j] > list[j + 1]){
                        int temp = list[j];
                        list[j] = list[j + 1];
                        list[j + 1] = temp;
                        needNextPass = true;
                    }
                }
        }
    }

    public static void main(String[] args) {
        int[] a = {3,34,6,7,8,12,3,4};
        bubbleSort(a);
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
    }
}
