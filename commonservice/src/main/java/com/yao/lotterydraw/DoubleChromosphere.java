package com.yao.lotterydraw;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by ruijie on 2017/11/22.
 * @author yaosongbing
 * @createTime 2017年11月24日10:38:46
 * @version 1.0.0
 * @classDesc 中国福利彩票初级版
 */
public class DoubleChromosphere {


    public static void main(String[] args) {
        System.out.println(getDoubleChromosphere());
    }

    public static Map<String, List<Integer>> getDoubleChromosphere() {
        Map<String, List<Integer>> doubleChromosphere = new HashMap<>();
        List<Integer> redBall = getBall(6, 33);
        List<Integer> blueBall = getBall(1, 16);
        doubleChromosphere.put("redBall", redBall);
        doubleChromosphere.put("blueBall", blueBall);
        return doubleChromosphere;
    }

    public static List<Integer> getBall(int limit, int bound) {
        List<Integer> listBall = IntStream.generate(() -> getRandomNumbers(bound))
                .distinct()
                .limit(limit)
                .sorted()
                .boxed()
                .collect(Collectors.toList());
        return listBall;
    }

    public static int getRandomNumbers(int bound) {
        Random randomRed = new Random();
        return randomRed.nextInt(bound) + 1;
    }

}
