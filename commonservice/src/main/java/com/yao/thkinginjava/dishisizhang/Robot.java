package com.yao.thkinginjava.dishisizhang;

import java.util.List;

/**
 * Created by ruijie on 2017/10/10.
 */
public interface Robot {
    String name();
    String model();
    List<Operation> operations();

    class Test{
        public static void test(Robot r){
            if (r instanceof  Null){
                System.out.println("Null Robot");
            }
            System.out.println(r.name());
            System.out.println(r.model());

            for (Operation operation : r.operations()) {
                System.out.println(operation.description());
                operation.command();
            }
        }
    }
}
