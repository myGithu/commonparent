package com.yao.thkinginjava.dishisizhang;

import javafx.geometry.Pos;

import java.io.Serializable;

/**
 * Created by ruijie on 2017/10/9.
 */
public class Person implements Serializable {
    public final String first;
    public final String last;
    public final String address;

    public Person(String first, String last, String address) {
        this.first = first;
        this.last = last;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person{" +
                "first='" + first + '\'' +
                ", last='" + last + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    public static class NullPerson extends Person implements  Null{
        private NullPerson(){
            super("None","None","None");
        }

        @Override
        public String toString() {
            return "NullPerson";
        }
    }
    public static final Person NULL =  new NullPerson();
}

class Position{
    private String title;
    private Person person;

    public Position(String jobTitle,Person employee){
        this.title = jobTitle;
        this.person = employee;
        if(employee == null){
            person = Person.NULL;
        }
    }
    public Position(String jobTitle){
        this.title = jobTitle;
        this.person = Person.NULL;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
        if (person == null){
            person = Person.NULL;
        }
    }

    @Override
    public String toString() {
        return "Position{" +
                "title='" + title + '\'' +
                ", person=" + person +
                '}';
    }
}
