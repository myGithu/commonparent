package com.yao.thkinginjava.dishisizhang;

import java.util.Map;

/**
 * Created by ruijie on 2017/10/10.
 */
public interface Operation {
        String description();
        void command();
}
