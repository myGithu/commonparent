package com.yao.thkinginjava.dishisizhang;

import org.apache.poi.ss.formula.functions.T;

import java.io.Serializable;

/**
 * Created by ruijie on 2017/9/28.
 */
class Toy{
    Toy(){}

    Toy(int i){

    }
}

class FancyToy extends Toy implements Serializable{
    FancyToy(){
        super(1);
    }
}

public class ToyTest {
    static void printInfo(Class cc){
        System.out.println("Super Class [" + cc.getSuperclass()+ "]  Class.name " + cc.getName() + "is Interface [ " + cc.getInterfaces() +" ]");
        System.out.println("Simple name " + cc.getSimpleName());
        System.out.println("Cannonical name " + cc.getCanonicalName());
    }

    public static void main(String[] args) {
        Class c = null;

        try {
            c = Class.forName("com.yao.thkinginjava.dishisizhang.FancyToy");
        } catch (ClassNotFoundException e) {
            System.out.println("not Class");
            System.exit(1);
        }

        printInfo(c);
    }

}
