package com.yao.thkinginjava.dishisizhang;

/**
 * Created by ruijie on 2017/9/29.
 */
public interface Factory<T> {
    T create();

}
