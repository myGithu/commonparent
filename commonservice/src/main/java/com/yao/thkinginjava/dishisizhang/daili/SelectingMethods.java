package com.yao.thkinginjava.dishisizhang.daili;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by ruijie on 2017/10/9.
 */
class MethodSelector implements InvocationHandler {
    private Object proxied;

    public MethodSelector(Object proxied) {
        this.proxied = proxied;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getName().equals("interesting")) {
            System.out.println("Proxy detected the interesting method");
        }
        return method.invoke(proxied, args);
    }
}

interface SomeMethode {
    void boring1();

    void boring2();

    void interesting(String arg);

    void boring3();
}

class Implementation implements SomeMethode{

    @Override
    public void boring1() {
        System.out.println("boring1()");
    }

    @Override
    public void boring2() {
        System.out.println("boring2()");
    }

    @Override
    public void interesting(String arg) {
        System.out.println("interesting(String arg) arg :" + arg);
    }

    @Override
    public void boring3() {
        System.out.println("boring3()");
    }
}
public class SelectingMethods {
    public static void main(String[] args) {
        SomeMethode someMethode = (SomeMethode) Proxy.newProxyInstance(
                SomeMethode.class.getClassLoader(),
                new Class[]{SomeMethode.class},
                new MethodSelector(new Implementation())
        );
        someMethode.boring1();
        someMethode.boring2();
        someMethode.boring3();
        someMethode.interesting("bonobo");
    }
}
