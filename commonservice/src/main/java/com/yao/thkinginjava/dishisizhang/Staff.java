package com.yao.thkinginjava.dishisizhang;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruijie on 2017/10/10.
 */
public class Staff {
    private List<Position> positionList = new ArrayList<>();

    public void add(String title, Person person) {
        positionList.add(new Position(title,person));
    }

    public void add(String ... titles){
        for (String title : titles) {
            positionList.add(new Position(title));
        }
    }

    public Staff(String ... titles){
        add(titles);
    }

    public boolean positionAvailable(String title){
        for (Position position : positionList) {
            if (title.equals(position.getTitle()) && Person.NULL == position.getPerson()){
                return  true;
            }
        }
        return  false;
    }

    public void fillPosition(String title, Person hire){
        for (Position position : positionList) {
            if (title.equals(position.getTitle()) && Person.NULL == position.getPerson()){
                position.setPerson(hire);
            }
            return ;
        }

        throw  new RuntimeException(
                "Position " + title + "not available"
        );
    }


    public static void main(String[] args) {
        Staff staff = new Staff("President","CTO","Marketing Manager",
        "Product Manager", "Project Lead","Software Engineer","Software Engineer",
                "Software Engineer","Test Engineer", "Technical Writer"
        );
        staff.fillPosition("President",new Person("Me","Last","The top,Lonely At"));
        staff.fillPosition("Project Lead",new Person("Janet","Planner","The Burbs"));
        if (staff.positionAvailable("Software Engineer")){
            staff.fillPosition("Software Engineer",new Person("Bob","Coder","Bright light City"));
        }

        System.out.println(staff);

    }
}
