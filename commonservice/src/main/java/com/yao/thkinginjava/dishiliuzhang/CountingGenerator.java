package com.yao.thkinginjava.dishiliuzhang;

import com.yao.thkinginjava.dishiwuzhang.fanxinjiekou.Generator;

/**
 * Created by ruijie on 2017/10/17.
 */
public class CountingGenerator {
    public static class Boolean implements Generator<java.lang.Boolean> {

        private boolean value = false;

        @Override
        public java.lang.Boolean next() {
            value = !value;
            return value;
        }
    }

    public static class Byte implements Generator<java.lang.Byte> {
        private byte value = 0;

        @Override
        public java.lang.Byte next() {
            return value++;
        }
    }
    static  char[] chars =
            ("qwertyuiopasdfghjklmnbvcxzQWERTYUIOPASDFGHJKLMNBVCXZ").toCharArray();

    public  static  class Character implements Generator<java.lang.Character>{
        int index = -1;
        @Override
        public java.lang.Character next() {
            index = (index + 1) % chars.length;
            return chars[index];
        }
    }

    public static  class String implements Generator<java.lang.String>{
        private int lenght = 7;

        Generator<java.lang.Character> cg = new Character();

        public String(){}

        public String (int lenght){
            this.lenght = lenght;
        }
        @Override
        public java.lang.String next() {
            char[] buf = new char[lenght];
            for (int i = 0; i < lenght; i++) {
               buf[i]  = cg.next();
            }
            return new java.lang.String(buf);
        }
    }
}
