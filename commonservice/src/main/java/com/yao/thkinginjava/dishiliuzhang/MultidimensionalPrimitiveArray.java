package com.yao.thkinginjava.dishiliuzhang;

import java.util.Arrays;

/**
 * Created by ruijie on 2017/10/17.
 */
public class MultidimensionalPrimitiveArray {
    public static void main(String[] args) {
        int[][] a = {
                {1,2,3},
                {4,5,6}
        };
        System.out.println(Arrays.deepToString(a));
    }
}
