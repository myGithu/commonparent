package com.yao.thkinginjava.dishiliuzhang;

import com.yao.thkinginjava.dishiqizhang.CollectionData;
import com.yao.thkinginjava.dishiwuzhang.fanxinjiekou.Generator;

import java.lang.reflect.Array;

/**
 * Created by ruijie on 2017/10/17.
 */
public class Generated {
    public  static <T> T[] array(T[] a, Generator<T> gen){
        return  new CollectionData<T>(gen,a.length).toArray(a);
    }
   public static <T> T[] array(Class<T> type, Generator<T> gen, int size){
        T[] a = (T[]) Array.newInstance(type,size);
        return new CollectionData<T>(gen,size).toArray(a);
    }
}
