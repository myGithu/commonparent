package com.yao.thkinginjava.dibazhang;

/**
 * Created by ruijie on 2017/9/1.
 *
 * 不是很懂
 */


class Shared{
    private int refcount = 0;
    private static long counter = 0;
    private final long id = counter ++ ;

    public Shared(){
        System.out.println(" Creating " + this);
    }

    public void addRef(){
        refcount ++ ;
    }

    protected void disopse(){
        if(-- refcount == 0){
            System.out.println("Dispose " + this);
        }
    }

    public String toString(){
        return  " Shared " + id;
    }

}

class Composing{
    private Shared shared;
    private static int counter = 0;
    private final long id = counter ++;

    public Composing(Shared shared){
        System.out.println(" Creating " + this);
        this.shared =  shared;
        this.shared.addRef();
    }

    protected  void dispose(){
        System.out.println(" disposing " + this);
        shared.disopse();
    }


    public String toString(){//toString 方法会自动调用
        return  "Composing " + id;
    }
}


public class ReferenceCounting {
    public static void main(String[] args) {
        Shared shared = new Shared();
        Composing[] composings = {
                new Composing(shared), new Composing(shared), new Composing(shared),
                new Composing(shared), new Composing(shared), new Composing(shared),
                new Composing(shared), new Composing(shared), new Composing(shared)
        };

        for (Composing composing : composings) {
            composing.dispose();
        }
    }
}
