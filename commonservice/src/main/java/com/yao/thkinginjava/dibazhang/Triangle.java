package com.yao.thkinginjava.dibazhang;

/**
 * Created by ruijie on 2017/8/23.
 */
public class Triangle extends  ShapeAbstruct implements Shape {
    @Override
    public void show() {
        System.out.println("Triangle");
    }

    @Override
    public void draw() {
        System.out.println("Triangle draw()");
    }

    @Override
    public void erase() {
        System.out.println("Triangle erase");
    }
}
