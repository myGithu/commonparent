package com.yao.thkinginjava.dibazhang;

/**
 * Created by ruijie on 2017/8/23.
 * 向上转型
 */
public class Cycle {
        public void ride(){
            System.out.println("Cycle  ride()");
        }

        public void ride2(){
            System.out.println("Cycle  ride2()");
        }
}

class  Unicycle extends Cycle{
    @Override
    public void ride() {
        System.out.println("Unicycle  ride()");
    }
}

class Bicycle extends  Cycle{
    @Override
    public void ride() {
        System.out.println("Bicycle  ride()");
    }
}

class Tricycle extends  Cycle{
    @Override
    public void ride() {
        System.out.println("Tricycle  ride()");
    }
}
