package com.yao.thkinginjava.dibazhang;

/**
 * Created by ruijie on 2017/9/1.
 */
class Meal{
    private  int age;

    Meal(){
        System.out.println("Meal()" + age);
    }
}

class Bread {
    Bread(){
        System.out.println("Bread()");
    }
}

class Cheese{
    Cheese(){
        System.out.println(" Cheese()");
    }
}


class Lunch extends Meal{
    Lunch(){
        System.out.println("Lunch()");
    }
}

class ProtableLunch extends Lunch{
    ProtableLunch(){
        System.out.println(" ProtableLunch()");
    }
}


public class Sandwich extends  ProtableLunch{
    private  Bread bread = new Bread();
    private Cheese cheese = new Cheese();
    public Sandwich(){
        System.out.println("Sandwich()");
    }

    public static void main(String[] args) {
        new Sandwich();
    }

}
