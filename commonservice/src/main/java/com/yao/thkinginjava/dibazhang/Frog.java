package com.yao.thkinginjava.dibazhang;

/**
 * Created by ruijie on 2017/9/1.
 */
class Characteristic{
    private String s;
    Characteristic(String s){
        this.s = s;
        System.out.println("Create Characteristic " +  s);
    }

    protected  void dispose(){
        System.out.println("disposing Charateristic " + s);
    }

}


class Description{
    private String s;

    Description(String s ){
        this.s = s;
        System.out.println("Create Description " + s);
    }

    protected  void dispose(){
        System.out.println("disposing Description " + s);
    }
}



class LivingCreature{
    private Characteristic characteristic = new Characteristic("is alive");
    private Description description = new Description("Basic Living Creature");


    LivingCreature(){
        System.out.println("LivingCreature()");
    }

    protected void dispose(){
        System.out.println("LivingCreature dispose");
        characteristic.dispose();
        description.dispose();
    }
}

class Animal extends LivingCreature{
    private Characteristic characteristic = new Characteristic("has  heart");
    private Description description = new Description("Animal not  vegetable");

    Animal(){
        System.out.println(" Animal()");
    }


    @Override
    protected void dispose() {
        System.out.println("Animal dispose");
        characteristic.dispose();
        description.dispose();
        super.dispose();
    }
}


class Amphibian extends  Animal{
    private Characteristic characteristic = new Characteristic("can live in  water");
    private Description description = new Description("both  water and  land");

    Amphibian(){
        System.out.println("Amphibian()");
    }

    protected  void dispose(){
        System.out.println("Amphibian dispose");
        characteristic.dispose();
        description.dispose();
        super.dispose();
    }
}

public class Frog extends  Amphibian{
    private Characteristic characteristic = new Characteristic("Croaks");
    private Description description = new Description("Eats Bugs");

    Frog(){
        System.out.println("Frog()");
    }

    protected  void dispose(){
        System.out.println("Frog dispose");
        characteristic.dispose();
        description.dispose();
        super.dispose();
    }

    public static void main(String[] args) {
        Frog frog = new Frog();
        System.out.println("88");
        frog.dispose();
    }

}
