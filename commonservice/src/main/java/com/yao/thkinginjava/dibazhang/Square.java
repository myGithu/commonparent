package com.yao.thkinginjava.dibazhang;

import java.io.Serializable;

/**
 * Created by ruijie on 2017/8/23.
 */
public class Square extends ShapeAbstruct implements Shape ,Serializable{


    @Override
    public void draw() {
        System.out.println("Square draw()");
    }

    @Override
    public void erase() {
        System.out.println("Square erase");
    }
}
