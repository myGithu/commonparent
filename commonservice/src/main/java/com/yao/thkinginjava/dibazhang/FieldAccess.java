package com.yao.thkinginjava.dibazhang;

/**
 * Created by ruijie on 2017/9/2.
 */
class Super {
    public int filed = 0;
    public int getFiled(){
        return  this.filed;
    }

}


class Sub extends  Super{
    public int field = 1;
    public int getField(){
        return  this.field;
    }

    public int getSuperFiled(){
        return  super.filed;
    }
}

public class  FieldAccess{
    public static void main(String[] args) {
        Super s = new Sub();
        System.out.println("s.filed=" + s.filed + " s.getFiled=" + s.getFiled());
        Sub  sub = new Sub();
        System.out.println("sub.field = " + sub.field + " sub.getField() = "+ sub.getField() + " sub.getSuperFiled()" + sub.getSuperFiled());
    }
}
