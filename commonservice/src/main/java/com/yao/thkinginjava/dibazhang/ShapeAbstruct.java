package com.yao.thkinginjava.dibazhang;


import java.io.Serializable;

/**
 * Created by ruijie on 2017/8/23.
 */
public abstract class ShapeAbstruct implements  Shape,Serializable,Cloneable{


    public void show(){
        System.out.println("ShapeAbstruct");
    }

    @Override
    public void draw() {
        System.out.println("ShapeAbstruct draw()");
    }

    @Override
    public void erase() {
        System.out.println("ShapeAbstruct erase()");
    }
}
