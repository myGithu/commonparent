package com.yao.thkinginjava.dibazhang;

/**
 * Created by ruijie on 2017/8/23.
 */
public interface Shape {
     void show();

     void draw();

     void erase();
}
