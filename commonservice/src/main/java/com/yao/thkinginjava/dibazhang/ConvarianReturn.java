package com.yao.thkinginjava.dibazhang;

/**
 * Created by y on 2017/9/11.
 * 协变返回类型 ：导出类中被覆盖的方法可以返回基类方法的返回类型的某种导出类型
 */

class Grain{
    @Override
    public String toString() {
        return "Grain";
    }
}

class Wheat extends Grain{
    @Override
    public String toString() {
        return "Wheat";
    }
}

class Mill{
    Grain process(){
        return  new Grain();
    }
}

class WheatMill extends Mill{
    Wheat process(){
        return new Wheat();
    }
}

public class ConvarianReturn {

    public static void main(String[] args) {
        Mill mill = new Mill();
        Grain g =  mill.process();
        System.out.println(g);
        mill = new WheatMill();
        g  =  mill.process();
        System.out.println(g);

    }
}
