package com.yao.thkinginjava.dibazhang;

/**
 * Created by y on 2017/9/11.
 */
class Userful{
    public void f(){}

    public void g(){}
}

class MoreUserful extends  Userful{
    @Override
    public void f() {
    }

    @Override
    public void g() {
    }

    public void a(){}

    public void b(){}
}


public class RTTI {

    public static void main(String[] args) {
        Userful[] userful = {new Userful(),new MoreUserful()};
        userful[0].f();
        userful[1].g();

        ((MoreUserful)userful[1]).a();
        ((MoreUserful)userful[0]).b();
    }

}
