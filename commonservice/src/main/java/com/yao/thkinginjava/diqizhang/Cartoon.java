package com.yao.thkinginjava.diqizhang;

/**
 * Created by ruijie on 2017/8/21.
 */

class Art{
    Art(){
        System.out.println("constructor Art()");
    }
}

class Drawing extends Art{
    Drawing(){
        System.out.println("constructor Drawing()");
    }
}

public class Cartoon extends Drawing{
    Cartoon(){
        System.out.println("constructor Cartoon()");
    }

    public static void main(String[] args) {
        Cartoon cartoon = new Cartoon();

    }

}
