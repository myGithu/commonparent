package com.yao.thkinginjava.diqizhang;

/**
 * Created by ruijie on 2017/8/21.
 * <p>
 * 代理类
 */
public class SpaceShipDelegation {
    private String name;
    private SpaceShipControls controls = new SpaceShipControls();

    public SpaceShipDelegation(String name) {
        this.name = name;
    }

    public void up(int velocity) {
        controls.up(velocity);
    }

    public void down(int velocity) {
        controls.down(velocity);
    }

    public void left(int velocity) {
        controls.left(velocity);
    }

    public void right(int velocity) {
        controls.right(velocity);
    }

    public void forword(int velocity) {
        controls.forword(velocity);
    }

    public void back(int velocity) {
        controls.back(velocity);
    }
}
