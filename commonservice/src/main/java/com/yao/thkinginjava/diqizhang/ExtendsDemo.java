package com.yao.thkinginjava.diqizhang;

/**
 * Created by ruijie on 2017/8/21.
 */
public class ExtendsDemo {
    private int age;
    private String name;


     ExtendsDemo() {
    }

    public ExtendsDemo(int age) {
       this(age,null);
    }

    public ExtendsDemo(int age, String name) {
        this.age = age;
        this.name = name;
    }

     int getAge() {
        return age;
    }

     void setAge(int age) {
        this.age = age;
    }

    protected String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

}

