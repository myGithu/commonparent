package com.yao.thkinginjava.dijiuzhang;

/**
 * Created by ruijie on 2017/9/14.
 * 初步工厂
 */
public class Factories {
    public static void serviceConsumer(ServiceFactory serviceFactory){
        Service service = serviceFactory.getService();
        service.method1();
        service.method2();
    }

    public static void main(String[] args) {
        serviceConsumer(new ImplementationlFactory());
        serviceConsumer(new Implementation2Factory());
    }
}

interface  Service{
    void method1();
    void method2();
}

interface ServiceFactory{
    Service getService();
}


class Implementationl implements Service{

    @Override
    public void method1() {
        System.out.println("Implementationl.method1()");
    }

    @Override
    public void method2() {
        System.out.println("Implementationl.method2()");
    }
}

class ImplementationlFactory implements ServiceFactory{

    @Override
    public Service getService() {
        return new Implementationl();
    }
}


class Implementation2 implements  Service{

    @Override
    public void method1() {
        System.out.println("Implementation2.method1()");
    }

    @Override
    public void method2() {
        System.out.println("Implementation2.method2()");
    }
}

class Implementation2Factory implements ServiceFactory{

    @Override
    public Service getService() {
        return new Implementation2();
    }
}