package com.yao.thkinginjava.dijiuzhang;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Created by ruijie on 2017/9/12.
 */
class Processor{
    public String name(){
        return getClass().getSimpleName();
    }

    Object process(Object input){
        return  input;
    }
}

class Upcase extends Processor{
    String process(Object input){
        return  ((String)input).toUpperCase();
    }
}

class Downcase extends Processor{
    @Override
    String process(Object input) {
        return ((String)input).toLowerCase();
    }
}

class Splitter extends Processor{
    @Override
    Object process(Object input) {
        return Arrays.toString (((String)input).split(" "));
    }
}

public class Apply {
    public static String s = "Disagrement with beliefs is by definditon incorrect";
    public static void process(Processor processor, Object object){
        System.out.println("Using Processor " + processor.name());
        System.out.println(processor.process(object));
    }

    public static void main(String[] args) {
        process(new Upcase(),s);
        process(new Downcase(),s);
        process(new Splitter(),s);
    }
}
