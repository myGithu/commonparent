package com.yao.thkinginjava.dijiuzhang;

/**
 * Created by ruijie on 2017/9/12.
 */
abstract  class Instrument{
    private int i;
    public abstract void  play(int n);
    public String what(){
        return  "Instrument";
    }
    public abstract void adjust();
}


class Wind extends Instrument{


    @Override
    public void play(int n) {
        System.out.println("wind.play(), n=" + n);
    }

    @Override
    public void adjust() {

    }

    @Override
    public String what() {
        return "Wind";
    }
}

class Stringed extends Instrument{
    @Override
    public void play(int n) {
        System.out.println("Stringed.play(), n=" + n);
    }

    @Override
    public String what() {
        return "Stringed";
    }

    @Override
    public void adjust() {

    }
}

class Percussion extends Instrument{


    @Override
    public void play(int n) {
        System.out.println("Percussion.paly(), n=" + n);
    }

    @Override
    public String what() {
        return  "Percussion";
    }

    @Override
    public void adjust() {

    }
}

class Brass extends  Wind{
    @Override
    public void play(int n) {
        System.out.println("Brass.paly(), n=" + n);
    }

    @Override
    public void adjust() {
        System.out.println("Brass.adjust()");
    }
}


class Woodwind extends Wind{
    @Override
    public void play(int n) {
        System.out.println("Woodwind.paly(), n= " + n);
    }

    @Override
    public String what() {
       return  "Woodwind";
    }
}

abstract  class Test{
    public abstract void f();

}

public class Music4 {
    static void tune(Instrument i){
        i.play(2);
    }

    static void tuneAll(Instrument[] e){
        for (Instrument instrument : e) {
            tune(instrument);
        }
    }

    public static void main(String[] args) {
                Instrument[] Instruments = {
                        new Wind(),
                        new Percussion(),
                        new Stringed(),
                        new Brass(),
                        new Woodwind()
                };
                tuneAll(Instruments);
    }

}
