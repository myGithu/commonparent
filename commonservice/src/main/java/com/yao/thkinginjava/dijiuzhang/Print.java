package com.yao.thkinginjava.dijiuzhang;

/**
 * Created by ruijie on 2017/9/12.
 */
abstract class P{
    public abstract  void print();
    P(){
        print();
    }
}


class P1 extends  P{
    private int i = 4;

    @Override
    public void print() {
        System.out.println("P1.i = " + i);
    }
}

class P2 extends  P{
    private int i = 5;
    @Override
    public void print() {
        System.out.println("P2.i=" + i);
    }
}

public class Print {
    public static void main(String[] args) {
        P p1 = new P1();
        p1.print();
    }
}
