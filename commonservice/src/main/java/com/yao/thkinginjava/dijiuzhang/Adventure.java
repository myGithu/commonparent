package com.yao.thkinginjava.dijiuzhang;

/**
 * Created by ruijie on 2017/9/13.
 */
public class Adventure {
    public static void t(CanFight canFight){
        canFight.fight();
    }

    public static void u(CanSwim canSwim){
        canSwim.swim();
    }

    public static void v(CanFly canFly){
        canFly.fly();
    }

    public static void w (ActionCharacter character){
        character.fight();
    }

    public static void main(String[] args) {
        Hero hero = new Hero();
        t(hero);
        u(hero);
        v(hero);
        w(hero);
    }
}

interface CanFight{
    void fight();
}

interface CanSwim{
    void swim();
}

interface CanFly{
    void fly();
}

class ActionCharacter{
    public void fight(){
        System.out.println("ActionCharacter.fight()");
    }
}

class Hero extends  ActionCharacter implements CanSwim, CanFight, CanFly{

    @Override
    public void fly() {
        System.out.println("Hero.fly()");
    }

    @Override
    public void swim() {
        System.out.println("Hero.swim()");
    }
}
