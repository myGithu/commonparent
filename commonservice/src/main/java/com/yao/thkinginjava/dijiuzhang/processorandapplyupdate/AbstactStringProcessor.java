package com.yao.thkinginjava.dijiuzhang.processorandapplyupdate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ruijie on 2017/9/13.
 */
public abstract class AbstactStringProcessor implements   Processor {
    @Override
    public String name() {
        return getClass().getSimpleName();
    }
    public abstract String process(Object input);


    public static String s = "if she weighs the same as a duck , she's made of wood";

    public static void main(String[] args) {
        Apply.process(new Upcase(),s);
        Apply.process(new Downcase(),s);
        Apply.process(new Splitter(),s);
        Apply.process(new Reverse(),s);
    }
}


class Upcase extends  AbstactStringProcessor{

    @Override
    public String process(Object input) {
        return ((String)input).toUpperCase();
    }
}

class Downcase extends  AbstactStringProcessor{

    @Override
    public String process(Object input) {
        return ((String)input).toLowerCase();
    }
}


class Splitter extends AbstactStringProcessor{

    @Override
    public String process(Object input) {
        return Arrays.toString(((String)input).split(" "));
    }
}

class Reverse extends  AbstactStringProcessor{

    @Override
    public String process(Object input) {
        return new StringBuffer((String) input).reverse().toString();
    }
}
