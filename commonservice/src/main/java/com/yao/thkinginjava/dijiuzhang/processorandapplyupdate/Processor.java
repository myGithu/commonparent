package com.yao.thkinginjava.dijiuzhang.processorandapplyupdate;

/**
 * Created by ruijie on 2017/9/13.
 */
public interface Processor {
    String name();
    Object process(Object input);
}
