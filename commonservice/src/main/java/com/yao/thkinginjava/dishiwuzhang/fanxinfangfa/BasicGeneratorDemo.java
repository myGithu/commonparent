package com.yao.thkinginjava.dishiwuzhang.fanxinfangfa;

import com.yao.thkinginjava.dishiwuzhang.fanxinjiekou.Generator;

/**
 * Created by ruijie on 2017/10/11.
 */
public class BasicGeneratorDemo {
    public static void main(String[] args) {
        Generator<CountedObject> gen = BasicGenerator.create(CountedObject.class);
        for (int i = 0; i < 5; i++) {
            System.out.println(gen.next());
        }
    }

}
