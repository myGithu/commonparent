package com.yao.thkinginjava.dishiwuzhang;

/**
 * Created by ruijie on 2017/10/10.
 */
public class ThreeTuple<A,B,C> extends TwoTuple<A,B>{
        public final C third;

        public ThreeTuple(A a,B b,C c){
            super(a,b);
            this.third = c;
        }

    @Override
    public String toString() {
        return "ThreeTuple{" +
                "first=" + first +
                ", second=" + second +
                ", third=" + third +
                '}';
    }
}
