package com.yao.thkinginjava.dishiwuzhang.shipeiqi;

/**
 * Created by ruijie on 2017/10/13.
 * 适配器模式实例
 *
 *
 */
public interface GermanChargingSocket {
    /**
     * 德国插座充电接口
     */
        void charge();
}
