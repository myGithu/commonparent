package com.yao.thkinginjava.dishiwuzhang;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by ruijie on 2017/10/10.
 */
public class RandomList<T> {
    private ArrayList<T> storage = new ArrayList<T>();

    private Random rand = new Random(47);

    public void add(T item){
        storage.add(item);
    }

    public T select(){
        return  storage.get(rand.nextInt(storage.size()));
    }
}
