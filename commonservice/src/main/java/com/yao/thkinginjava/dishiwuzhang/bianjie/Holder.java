package com.yao.thkinginjava.dishiwuzhang.bianjie;

/**
 * Created by ruijie on 2017/10/12.
 */
public class Holder<T> {
    private T value;

    public Holder(){}

    public Holder(T value){
        this.value = value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        return value.equals(obj);
    }

    public static void main(String[] args) {
        Holder<Apple> appleHolder = new Holder<>(new Apple());
        Apple apple =  appleHolder.getValue();
        appleHolder.setValue(apple);

        Holder<? extends Fruit> fruit = appleHolder;
        Fruit fruit1 = fruit.getValue();
        apple = (Apple) fruit.getValue();



        Orange o = (Orange) fruit.getValue();
        System.out.println(fruit.equals(o));
        System.out.println(o);
    }
}
