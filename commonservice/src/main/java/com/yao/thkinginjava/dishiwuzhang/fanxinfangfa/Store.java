package com.yao.thkinginjava.dishiwuzhang.fanxinfangfa;


import com.yao.thkinginjava.dishiwuzhang.fanxinjiekou.Generator;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by ruijie on 2017/10/11.
 */

class Product{
    private final int id;

    private String description;

    private double price;

    public Product (int IDnumber,String descr,double price){
        id = IDnumber;

        this.description = descr;

        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", price=" + price +
                '}';
    }

    public void priceChange(double change){
        price += change;
    }

    public static Generator<Product> generator = new Generator<Product>() {
        private Random rand = new Random(47);

        @Override
        public Product next() {
            return new Product(rand.nextInt(1000),"Test",Math.round(rand.nextDouble() * 1000D) + 0.99D);
        }
    };

}


class Shelf extends ArrayList<Product>{
    public Shelf(int nProducts){
        Generators.fill(this,Product.generator,nProducts);
    }
}

class Aisle extends  ArrayList<Shelf>{
    public Aisle(int nShelfves,int nProducts){
        for (int i = 0; i < nShelfves; i++) {
            add(new Shelf(nProducts));
        }
    }
}

class CheckoutStand{}


class Office{}


public class Store extends ArrayList<Aisle>{
    private ArrayList<CheckoutStand>  checkoutStands =
                        new ArrayList<>();

    private Office office = new Office();

    public Store(int nAisles, int nShelves, int nProducts){
        for (int i = 0; i < nAisles; i++) {
            add(new Aisle(nShelves,nProducts));
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        for (Aisle shelves : this) {
            for (Shelf shelf : shelves) {
                for (Product product : shelf) {
                    result.append(product);
                    result.append("\n");

                }
            }
        }
        return result.toString();
    }


    public static void main(String[] args) {
        System.out.println(new Store(14,5,10));
    }
}
