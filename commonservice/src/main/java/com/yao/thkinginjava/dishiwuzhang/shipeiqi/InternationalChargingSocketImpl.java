package com.yao.thkinginjava.dishiwuzhang.shipeiqi;

/**
 * Created by ruijie on 2017/10/13.
 */
public class InternationalChargingSocketImpl implements InternationalChargingSocket {
    @Override
    public void charge() {
        System.out.println("正在使用国际充电接口给手机充电");
    }
}
