package com.yao.thkinginjava.dishiwuzhang.shipeiqi;

/**
 * Created by ruijie on 2017/10/13.
 * 中间连接插座（适配器）
 * 既可以连接德国插座标准
 * 也可以连接国际插座标准
 *
 * 适配器模式的三个特点：
 *
 * 1    适配器对象实现原有接口
 * 2    适配器对象组合一个实现新接口的对象（这个对象也可以不实现一个接口，只是一个单纯的对象）
 * 3    对适配器原有接口方法的调用被委托给新接口的实例的特定方法
 */
public class SocketAdapter implements GermanChargingSocket{
    private InternationalChargingSocket chargingSocket;

    public SocketAdapter(){}

    public SocketAdapter(InternationalChargingSocket chargingSocket){
        this.chargingSocket = chargingSocket;
    }

    @Override
    public void charge() {
           chargingSocket.charge();
    }
}
