package com.yao.thkinginjava.dishiwuzhang.fanxinfangfa;

import com.yao.thkinginjava.dishiwuzhang.fanxinjiekou.Coffee;
import com.yao.thkinginjava.dishiwuzhang.fanxinjiekou.CoffeeGenerator;
import com.yao.thkinginjava.dishiwuzhang.fanxinjiekou.Fibonacci;
import com.yao.thkinginjava.dishiwuzhang.fanxinjiekou.Generator;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by ruijie on 2017/10/11.
 */
public class Generators {
    public static <T> Collection<T> fill(Collection<T> coll, Generator<T> gen, int n){
        for (int i = 0; i < n; i++) {
            coll.add(gen.next());
        }
        return coll;
    }


    public static void main(String[] args) {
        Collection<Coffee> coffees = fill(new ArrayList<>(),new CoffeeGenerator(),4);
        for (Coffee coffee : coffees) {
            System.out.println(coffee);
        }
        Collection<Integer> integers = fill(new ArrayList<>(),new Fibonacci(),12);
        for (Integer integer : integers) {
            System.out.print(integer + " ");
        }
    }
}
