package com.yao.thkinginjava.dishiwuzhang.shipeiqi;

/**
 * Created by ruijie on 2017/10/13.
 */
public interface InternationalChargingSocket {
    /**
     * 国际充电插座标准
     */
     void charge();
}
