package com.yao.thkinginjava.dishiwuzhang.fanxinfangfa;

import java.util.*;

/**
 * Created by ruijie on 2017/10/11.
 */
class Frob{}


class Fnorkle{};

class Quark<Q>{}

class Particle<POSITION,MOMENTUM>{}


public class LostInformation {
    public static void main(String[] args) {
        List<Frob> list = new ArrayList<>();
        Map<Frob,Fnorkle> map = new HashMap<>();
        Quark<Fnorkle> quark = new Quark<>();
        Particle<Long,Double> p = new Particle<>();
        System.out.println(Arrays.toString(list.getClass().getTypeParameters()));
        System.out.println(Arrays.toString(map.getClass().getTypeParameters()));
        System.out.println(Arrays.toString(quark.getClass().getTypeParameters()));
        System.out.println(Arrays.toString(p.getClass().getTypeParameters()));


    }
}
