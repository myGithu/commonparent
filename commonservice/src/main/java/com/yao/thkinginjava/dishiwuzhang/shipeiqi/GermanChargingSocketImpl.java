package com.yao.thkinginjava.dishiwuzhang.shipeiqi;

/**
 * Created by ruijie on 2017/10/13.
 */
public class GermanChargingSocketImpl implements GermanChargingSocket {
    @Override
    public void charge() {
        System.out.println("正在使用德国插口充电");
    }
}
