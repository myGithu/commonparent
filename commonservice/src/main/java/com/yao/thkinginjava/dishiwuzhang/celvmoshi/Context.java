package com.yao.thkinginjava.dishiwuzhang.celvmoshi;

/**
 * Created by ruijie on 2017/10/13.
 *
 * 使用策略的环境
 */

public class Context {
    private Strategy strategy;//策略对象

    public Context(){}

    public Context(Strategy strategy){
        this.strategy = strategy;
    }

    public void contextInterface(){//执行策略方法
        strategy.strategyInterface();
    }
}
