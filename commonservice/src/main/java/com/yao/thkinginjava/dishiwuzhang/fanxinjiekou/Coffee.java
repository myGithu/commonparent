package com.yao.thkinginjava.dishiwuzhang.fanxinjiekou;

/**
 * Created by ruijie on 2017/10/10.
 */
public class Coffee {
    private static long counter = 0;

    private final long id = counter ++ ;

    @Override
    public String toString() {
        return getClass().getSimpleName() + " : " + id ;
    }
}
