package com.yao.thkinginjava.dishiwuzhang.fanxinjiekou;

import java.util.Iterator;

/**
 * Created by ruijie on 2017/10/10.
 */
public class IterableFibonacci extends Fibonacci implements  Iterable<Integer> {
    private int n;

    public IterableFibonacci(int count){
        n = count;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            @Override
            public boolean hasNext() {
                return n > 0;
            }

            @Override
            public Integer next() {
                n --;
                return IterableFibonacci.super.next();
            }

            @Override
            public void remove() {
                throw  new UnsupportedOperationException();
            }
        };
    }

    public static void main(String[] args) {
        for (Integer integer : new IterableFibonacci(18)) {
            System.out.println(integer + " ");
        }
    }
}
