package com.yao.thkinginjava.dishiwuzhang.celvmoshi.demo;

/**
 * Created by ruijie on 2017/10/13.
 *
 * 初级会员没有折扣
 */
public class PrimaryMenberStrategy implements MemberStrategy {
    @Override
    public double calcPrice(double booksPrice) {
        System.out.println("初级会员没有折扣！");
        return booksPrice;
    }
}
