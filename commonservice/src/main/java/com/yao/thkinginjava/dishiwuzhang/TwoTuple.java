package com.yao.thkinginjava.dishiwuzhang;

/**
 * Created by ruijie on 2017/10/10.
 */
public class TwoTuple<A,B> {
    public final A first;
    public final B second;

    public TwoTuple(A a,B b){
        this.first = a;
        this.second = b;
    }

    @Override
    public String toString() {
        return "TwoTuple{" +
                "first=" + first +
                ", second=" + second +
                '}';
    }
}

