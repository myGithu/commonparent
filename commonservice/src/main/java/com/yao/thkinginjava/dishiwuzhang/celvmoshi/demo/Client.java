package com.yao.thkinginjava.dishiwuzhang.celvmoshi.demo;

/**
 * Created by ruijie on 2017/10/13.
 */
public class Client {
    public static void main(String[] args) {
        BooksPrice booksPrice = new BooksPrice(new IntermediateMemberStrategy());
        System.out.println(booksPrice.quote(200));
    }
}
