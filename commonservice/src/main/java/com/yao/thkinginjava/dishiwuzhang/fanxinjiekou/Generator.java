package com.yao.thkinginjava.dishiwuzhang.fanxinjiekou;

/**
 * Created by ruijie on 2017/10/10.
 */
public interface Generator<T> {
    T next();
}
