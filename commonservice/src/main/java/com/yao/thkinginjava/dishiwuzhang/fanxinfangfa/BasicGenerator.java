package com.yao.thkinginjava.dishiwuzhang.fanxinfangfa;

import com.yao.thkinginjava.dishiwuzhang.fanxinjiekou.Generator;

/**
 * Created by ruijie on 2017/10/11.
 */
public class BasicGenerator<T> implements Generator<T> {
    private Class<T>  type;


    public BasicGenerator(Class<T> type){
            this.type = type;
    }

    @Override
    public T next() {
        try {
            return type.newInstance();
        } catch (Exception e) {
           throw new RuntimeException(e);
        }
    }

    public static <T> Generator<T> create(Class<T> type){
        return new BasicGenerator<T>(type);
    }

    public static <T> T getBean(Class<T> beanCls){
        return create(beanCls).next();
    }


    public static void main(String[] args) {
      //  Generator<Generators> generator =  BasicGenerator.create(Generators.class);
        //System.out.println(generator.next());
        BasicGenerator.getBean(Generators.class);
    }
}
