package com.yao.thkinginjava.dishiwuzhang.celvmoshi.demo;

/**
 * Created by ruijie on 2017/10/13.
 *
 *  高级会员20%的折扣
 */
public class AdvancedMemberStrategy implements MemberStrategy {
    @Override
    public double calcPrice(double booksPrice) {
        System.out.println("高级会员20%折扣");
        return booksPrice * 0.8;
    }
}
