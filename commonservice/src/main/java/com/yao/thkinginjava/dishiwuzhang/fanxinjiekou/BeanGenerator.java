package com.yao.thkinginjava.dishiwuzhang.fanxinjiekou;

import org.apache.poi.ss.formula.functions.T;

/**
 * Created by ruijie on 2017/10/11.
 */
public class BeanGenerator<T> implements Generator<T>{
    private Class<?> bean;


    public BeanGenerator(Class<?> beanCls){
        this.bean = beanCls;
    }

    public T getBean() throws InstantiationException, IllegalAccessException {

        return createBean();
    }

    @Override
    public T next() {
        if (bean == null){
            throw new RuntimeException();
        }
        try {
            return createBean();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    private T createBean() throws IllegalAccessException, InstantiationException {
        return (T) bean.newInstance();
    }


    public static void main(String[] args) {
        BeanGenerator<Coffee> coffeeBeanGenerator = new BeanGenerator<>(Breve.class);
        try {
            Coffee coffee = coffeeBeanGenerator.getBean();
            System.out.println(coffee);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
