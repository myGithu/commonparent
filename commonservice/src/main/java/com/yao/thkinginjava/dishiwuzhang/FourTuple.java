package com.yao.thkinginjava.dishiwuzhang;

/**
 * Created by ruijie on 2017/10/10.
 */
public class FourTuple<A,B,C,D> extends ThreeTuple<A,B,C> {
    public final D fourth;

    FourTuple(A a,B b,C c,D d){
        super(a,b,c);
        this.fourth = d;
    }

    @Override
    public String toString() {
        return "FourTuple{" +
                "first=" + first +
                ", second=" + second +
                ", third=" + third +
                ", fourth=" + fourth +
                '}';
    }
}
