package com.yao.thkinginjava.dishiwuzhang.shipeiqi;

/**
 * Created by ruijie on 2017/10/13.
 */
public class GermanHotel {//客户端旅馆
    private GermanChargingSocket chargingSocket;

    public GermanHotel(){}

    public GermanHotel(GermanChargingSocket chargingSocket){
        this.chargingSocket = chargingSocket;
    }

    public void charge(){
        chargingSocket.charge();
    }

    public static void main(String[] args) {
      //  GermanHotel hotel = new GermanHotel(new GermanChargingSocketImpl());
        GermanHotel hotel = new GermanHotel(new SocketAdapter(new InternationalChargingSocketImpl()));
        hotel.charge();
    }

}
