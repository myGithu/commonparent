package com.yao.thkinginjava.dishiwuzhang.celvmoshi.demo;

/**
 * Created by ruijie on 2017/10/13.
 */
public class BooksPrice {
    private MemberStrategy memberStrategy;////持有一个具体的策略对象

    public BooksPrice(MemberStrategy memberStrategy){
        this.memberStrategy = memberStrategy;
    }


    /**
     * 计算图书的价格
     * @param booksPrices  图书的原价
     * @return 计算出打折后的价格
     */
    public double quote(double booksPrices){
        return memberStrategy.calcPrice(booksPrices);
    }

    public double sumPrice(double booksPrices, int booksCount){
        return memberStrategy.calcPrice(booksPrices) * booksCount;
    }
}
