package com.yao.thkinginjava.dishiwuzhang.celvmoshi.demo;

/**
 * Created by ruijie on 2017/10/13.
 * 中级会员10%的折扣
 */
public class IntermediateMemberStrategy implements  MemberStrategy{

    @Override
    public double calcPrice(double booksPrice) {
        System.out.println("中级会员10%的折扣");
        return booksPrice * 0.9;
    }
}
