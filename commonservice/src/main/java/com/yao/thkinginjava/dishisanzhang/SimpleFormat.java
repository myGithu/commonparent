package com.yao.thkinginjava.dishisanzhang;

/**
 * Created by ruijie on 2017/9/27.
 */
public class SimpleFormat {
    public static void main(String[] args) {
        int x = 5;
        double y = 5.33254D;
        System.out.println(" Row 1 : [ " + x +" " + y + "]");
        System.out.printf("Row 1 : [%d %f]\n",x,y);
    }
}
