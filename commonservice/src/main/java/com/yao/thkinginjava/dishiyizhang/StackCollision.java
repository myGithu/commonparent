package com.yao.thkinginjava.dishiyizhang;

/**
 * Created by ruijie on 2017/9/21.
 */
public class StackCollision {
    public static void main(String[] args) {
        Stack<String> stack = new Stack<>();
        for (String s : "My dog has fleas".split(" ")) {
            stack.push(s);
        }
        System.out.println("stack" + stack);
        java.util.Stack<String> stringStack = new java.util.Stack<>();
        for (String s : "My dog has fleas".split(" ")) {
            stringStack.push(s);
        }
        //System.out.println(stringStack.firstElement());
        System.out.println("stringStack" + stringStack);
    }
}
