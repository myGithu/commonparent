package com.yao.thkinginjava.dishiyizhang;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

/**
 * Created by ruijie on 2017/9/21.
 */
public class SetOfInteger {
    public static void main(String[] args) {
        Random random = new Random(47);
        Set<Integer> intSet = new HashSet<>();
        for (int i = 0; i < 10000; i++) {
            intSet.add(random.nextInt(30) + 1);
        }

        System.out.println(intSet);
    }
}
