package com.yao.thkinginjava.dishiyizhang;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by ruijie on 2017/9/23.
 * 任何实现这个借口的对象都可以使用forEach语法
 */
public class IterableClass implements Iterable<String> {

    protected String[] words = ("And that is how we know the Earth to be banana-shaped.").split(" ");

    @Override
    public Iterator<String> iterator() {
        return new Iterator<String>() {
            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < words.length;
            }

            @Override
            public String next() {
                return words[index ++ ];
            }
        };
    }

    public static void main(String[] args) {
        for (String s : new IterableClass()) {
            System.out.println( s +  " ");
        }
    }
}
