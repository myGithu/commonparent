package com.yao.thkinginjava.dishiyizhang;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by ruijie on 2017/9/20.
 */
class Snow{}

class Powder extends  Snow{}

class Light extends Powder{}

class Heavy extends  Powder{}

class Crusty extends  Snow{}

class Slush extends Snow{}

public class AsListInference {
    public static void main(String[] args) {
        List<Snow> snowList = Arrays.asList(
                new Crusty(),new Slush(),new Powder()
        );
        List<Snow> snows = new ArrayList<Snow>();
        Collections.addAll(snows,new Light(),new Heavy(),new Slush());
        List<Snow> snowList1 = Arrays.<Snow>asList(
          new Light(),new Heavy()
        );
    }

}
