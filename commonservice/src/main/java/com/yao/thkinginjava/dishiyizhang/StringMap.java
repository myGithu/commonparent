package com.yao.thkinginjava.dishiyizhang;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by ruijie on 2017/9/23.
 */
public class StringMap {
    public static void main(String[] args) {
        Map<String,String> map = new HashMap<String,String>();
        map.put("My Cat","Molly");
        map.put("My Dog","Ginger");
        map.put("My Hamster","Bosco");
        System.out.println(map);
        String dog = map.get("My Dog");
        System.out.println(dog);
        System.out.println(map.containsKey("My Dog"));
        System.out.println(map.containsValue(dog));
        Set<String> set = map.keySet();
        for (Map.Entry<String, String> stringStringEntry : map.entrySet()) {
            String k = stringStringEntry.getKey();
            String v = stringStringEntry.getValue();
            System.out.println("k = " + k + " : v = " + v);
        }        
    }
}
