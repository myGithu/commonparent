package com.yao.thkinginjava.dishiyizhang;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

/**
 * Created by ruijie on 2017/9/23.
 * Queue队列是个先进先出的容器
 */
public class QueueDemo {
    static void printQ(Queue queue){
        while (queue.peek() != null){
            System.out.print(queue.remove() + " ");

        }
        System.out.println();
    }

    public static void main(String[] args) {
        Queue<Integer> queue = new LinkedList<>();
        Random rarn = new Random(47);
        for (int i = 0; i < 10; i++) {
            queue.offer(rarn.nextInt(i + 10));
        }
      printQ(queue);
        Queue<Character> qc = new LinkedList<>();
        for (char c : "Brontosaurus".toCharArray()) {
            qc.offer(c);
        }
       printQ(qc);
    }
}
