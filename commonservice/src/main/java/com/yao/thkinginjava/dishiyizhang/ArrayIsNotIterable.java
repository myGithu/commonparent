package com.yao.thkinginjava.dishiyizhang;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by ruijie on 2017/9/23.
 * 数组不是一个Iterable
 */
public class ArrayIsNotIterable {
        static <T> void test(Iterable<T> ib){
            for (T t : ib) {
                System.out.println( t + " ");
            }
        }

    public static void main(String[] args) {
        test(Arrays.asList(1,2,3));
        String[] strings = {"A","B","C"};
        //test(strings);
        test(Arrays.asList(strings));
    }
}
