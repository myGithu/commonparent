package com.yao.thkinginjava.dishiyizhang;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ruijie on 2017/9/20.
 */
public class SimpleIteration {
    public static void main(String[] args) {
        List<String> list = Arrays.<String>asList(
                "A","B","C","D"
        );
        Iterator<String> it = list.iterator();
        for (;it.hasNext();){
            String s = it.next();
            System.out.println("s: " + s);
        }

       /* for (String s : list) {
            System.out.println("s = " + s);
        }*/

    }
}
