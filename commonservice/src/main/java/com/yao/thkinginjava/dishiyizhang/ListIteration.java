package com.yao.thkinginjava.dishiyizhang;

import java.util.*;

/**
 * Created by ruijie on 2017/9/20.
 */
public class ListIteration {
    public static void main(String[] args) {
        List<String> list = Arrays.<String>asList(
                "A","B","C","D"
        );
        ListIterator<String> listIteration = list.listIterator();
        while (listIteration.hasNext()){
            System.out.println("s = " + listIteration.next());
        }
        while (listIteration.hasPrevious()){//反向列表 和 hasNext相对

            System.out.println("s1 = " + listIteration.previous());
        }
    }
}
