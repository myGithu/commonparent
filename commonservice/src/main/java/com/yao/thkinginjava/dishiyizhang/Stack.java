package com.yao.thkinginjava.dishiyizhang;

import java.util.LinkedList;

/**
 * Created by ruijie on 2017/9/21.
 * 栈容器是一个先进后出的容器
 */
public class Stack<T> {
    private LinkedList<T> storage = new LinkedList<T>();
    public void push(T v){
        storage.addFirst(v);
    }
    public T peek(){
        return  storage.getFirst();
    }

    public T pop(){
        return  storage.removeFirst();
    }

    public boolean empty(){
        return  storage.isEmpty();
    }

    @Override
    public String toString() {
        return storage.toString();
    }
}
