package com.yao.thkinginjava.dishiyizhang;

/**
 * Created by ruijie on 2017/9/21.
 */
public class StackTest {
    public static void main(String[] args) {
        Stack<String> stack = new Stack<>();
        for (String s : "My dog has fleas".split(" ")) {
            stack.push(s);
        }

        while (!stack.empty()){
            System.out.println(stack.pop() + "  ");
        }
        System.out.println(stack);
    }
}
