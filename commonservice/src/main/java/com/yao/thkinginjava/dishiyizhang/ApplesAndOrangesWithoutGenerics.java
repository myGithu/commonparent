package com.yao.thkinginjava.dishiyizhang;

import java.util.ArrayList;

/**
 * Created by ruijie on 2017/9/20.
 */
class Apple{
    private static long counter;
    private final long id = counter ++;

    public long id(){
        return  id;
    }
}

class Orange{}

public class ApplesAndOrangesWithoutGenerics {
    public static void main(String[] args) {
        ArrayList<Apple> apples = new ArrayList<Apple>();
        for (int i = 0; i < 3; i++) {
            apples.add(new Apple());
        }
        for (int i = 0; i < apples.size(); i++) {
            System.out.println(apples.get(i).id());
        }
    }
}
