package com.yao.thkinginjava.dishiyizhang;

import org.thymeleaf.expression.Arrays;

import java.util.*;

/**
 * Created by ruijie on 2017/9/23.
 */
public class InterfaceVsiterator {
 public static void display(Iterator<String> it){
    for (;it.hasNext();){
        String s = it.next();
        System.out.println("s = " + s);
    }
 }
 public static void display(Collection<String> strings){
     for (String s : strings) {
         System.out.println("s = " + s);
     }
 }

    public static void main(String[] args) {
        List<String> list = java.util.Arrays.asList("a b c d e d g h i i k".split(" "));
        Set<String> set = new HashSet<>(list);
        Map<String,String> map = new  LinkedHashMap<>();
        map.values().iterator();
    }

}
