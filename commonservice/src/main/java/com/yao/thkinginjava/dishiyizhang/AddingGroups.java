package com.yao.thkinginjava.dishiyizhang;

import java.util.*;

/**
 * Created by ruijie on 2017/9/20.
 */
public class AddingGroups {
    public static void main(String[] args) {
        Collection<Integer> collection
                = new ArrayList<Integer>(Arrays.asList(1,2,3,4,5,6));
        Integer[] moreInts = {10,11,12,13,14,15,16};
        collection.addAll(Arrays.asList(moreInts));
        Collections.addAll(collection,20,21,22,23,24,25);
        Collections.addAll(collection,moreInts);
        List<Integer> list = Arrays.asList(30,31,32,33,34);
        list.set(1,100);

        System.out.println(Arrays.asList());
    }
}
