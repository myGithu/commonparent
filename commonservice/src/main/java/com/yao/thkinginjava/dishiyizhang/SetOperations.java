package com.yao.thkinginjava.dishiyizhang;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ruijie on 2017/9/21.
 */
public class SetOperations {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        Collections.addAll(set,"A B C D E F G H I J K L".split(" "));
        set.add("M");
        System.out.println("H : " + set.contains("H"));
        System.out.println("N : " + set.contains("N"));

        Set<String> set1 = new HashSet<>();
        Collections.addAll(set1,"H I J K L".split(" "));
        System.out.println("set1 in set: " + set.containsAll(set1));
        set.remove("H");
        System.out.println("set = " + set);
        System.out.println("set1 in set: " + set.containsAll(set1));
        set.removeAll(set1);
        System.out.println("set = " + set);

    }
}
