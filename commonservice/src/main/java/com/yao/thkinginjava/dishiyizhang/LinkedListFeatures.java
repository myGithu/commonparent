package com.yao.thkinginjava.dishiyizhang;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * Created by ruijie on 2017/9/21.
 */
public class LinkedListFeatures {
    public static void main(String[] args) {
        LinkedList<String> linkedList =
                new LinkedList<>(Arrays.asList("A","B","C","D","E"));
        System.out.println("linedList = " + linkedList);
        System.out.println("linkedList.getFirst() = " + linkedList.getFirst());
        System.out.println("linkedList.element() = " + linkedList.element());
        System.out.println("linkedList.peek() = " + linkedList.peek());
        System.out.println("linkedList.remove() = " + linkedList.remove());
        System.out.println("linkedList.removeFirst() = " + linkedList.removeFirst());
        System.out.println("linkedList.poll() = " + linkedList.poll());
        System.out.println("linkedList = " + linkedList);
        System.out.println("linkedList.add(F) = " + linkedList.add("F"));
        System.out.println("linkedList = " + linkedList);
        System.out.println(" linkedList.offer(G) = " + linkedList.offer("G"));
        System.out.println("linkedList = " + linkedList);
        linkedList.addLast("H");
        System.out.println("linkedList = " + linkedList);
        System.out.println("linkedList.removeLast() = " + linkedList.removeLast());
        System.out.println("linkedList = " + linkedList);
    }
}
