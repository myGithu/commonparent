package com.yao.thkinginjava.dishiqizhang;

import java.util.Collections;

/**
 * Created by ruijie on 2017/10/17.
 */
public class Person1 implements Comparable<Person1> {
    private int age;

    private String name;


    public Person1(){}

    public Person1(int age, String name){
        this.age = age;
        this.name = name;
    }

    @Override
    public int compareTo(Person1 o) {
        return this.age - o.age;
    }

    @Override
    public String toString() {
        return name + " : " + age;
    }

    public static void main(String[] args) {

    }
}
