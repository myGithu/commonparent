package com.yao.thkinginjava.dishiqizhang;

/**
 * Created by ruijie on 2017/10/19.
 */
public class Groundhog {
    protected int number;

    public Groundhog(int n){
        number = n;
    }

    @Override
    public String toString() {
        return "Groundhog #" + number;
    }
}
