package com.yao.thkinginjava.dishiqizhang.disanjie;

import com.yao.thkinginjava.dishiqizhang.dierjie.Countries;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by ruijie on 2017/10/19.
 */
public class CollectionMethods {
    public static void main(String[] args) {
        Collection<String> collection = new ArrayList<>();
        collection.add("ten");
        collection.add("eleven");
        System.out.println(collection);
        Object[] array = collection.toArray();
        String[] strs = collection.toArray(new String[0]);
        System.out.println(Collections.max(collection));
        System.out.println(Collections.min(collection));
    }
}
