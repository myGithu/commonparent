package com.yao.thkinginjava.dishiqizhang;

import java.util.*;

/**
 * Created by ruijie on 2017/10/24.
 */
public class ReadOnly {
    static Collection<String> data = new ArrayList<>();

    public static void main(String[] args) {
        data.add("A");
        data.add("B");
        data.add("C");
        data.add("E");
        data.add("D");

        Collection<String> c = Collections.unmodifiableCollection(new ArrayList<>(data));
        System.out.println(c);

        List<String> a = Collections.unmodifiableList(new ArrayList<>(data));
        System.out.println(a);

        ListIterator<String> iterator = a.listIterator();
        System.out.println(iterator.hasNext() + " : " + iterator.next());


    }
}
