package com.yao.thkinginjava.dishiqizhang;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ruijie on 2017/10/24.
 */
public class ListSortSearch {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("A");
        list.add("C");
        list.add("B");
        list.add("E");
        list.add("D");
        System.out.println(list);
        Collections.sort(list);
        System.out.println(list);
        System.out.println(Collections.binarySearch(list,"D"));

    }
}
