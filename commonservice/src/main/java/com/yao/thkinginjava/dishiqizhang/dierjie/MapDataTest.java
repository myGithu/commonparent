package com.yao.thkinginjava.dishiqizhang.dierjie;

import com.yao.thkinginjava.dishiwuzhang.fanxinjiekou.Generator;

import java.util.Iterator;

/**
 * Created by ruijie on 2017/10/18.
 */
class Letters implements Generator<Pair<Integer,String>>{
    private int size = 9;

    private int number = 1;

    private char letter = 'A';



    @Override
    public Pair<Integer, String> next() {
        return new Pair<>(number ++, " " + letter ++);
    }


    public Iterator<Integer> iterator(){

        return  new Iterator<Integer>() {
            @Override
            public boolean hasNext() {
                return number < size;
            }

            @Override
            public Integer next() {
                return number ++;
            }
        };
    }
}

public class MapDataTest {
    public static void main(String[] args) {
        System.out.println(MapData.map(new Letters(),11));
        //System.out.println(MapData.map(new Letters().iterator()));
    }
}
