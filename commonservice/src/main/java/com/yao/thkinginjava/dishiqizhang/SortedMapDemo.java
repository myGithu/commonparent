package com.yao.thkinginjava.dishiqizhang;

import java.util.Iterator;
import java.util.TreeMap;

/**
 * Created by ruijie on 2017/10/19.
 */
public class SortedMapDemo {
    public static void main(String[] args) {
        TreeMap<Integer,String> sortedMap = new TreeMap<>();
        sortedMap.put(1,"A");
        sortedMap.put(2,"B");
        sortedMap.put(3,"C");
        sortedMap.put(4,"D");
        System.out.println(sortedMap);
        Integer low = sortedMap.firstKey();
        Integer high = sortedMap.lastKey();
        System.out.println(low);
        System.out.println(high);

        Iterator<Integer> it = sortedMap.keySet().iterator();
        while (it.hasNext()){
            System.out.println(it.next());
        }
    }
}
