package com.yao.thkinginjava.dishiqizhang;

import com.yao.thkinginjava.dishiwuzhang.fanxinjiekou.Generator;

import java.util.ArrayList;

/**
 * Created by ruijie on 2017/10/17.
 */
public class CollectionData<T> extends ArrayList<T> {
    public CollectionData(Generator<T> gen,int quantity){
        for (int i = 0; i < quantity; i++) {
            add(gen.next());
        }
    }

    public static <T> CollectionData<T> list(Generator<T> gen, int quantity){
        return new CollectionData<>(gen,quantity);
    }
}
