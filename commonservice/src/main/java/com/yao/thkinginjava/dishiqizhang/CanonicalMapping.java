package com.yao.thkinginjava.dishiqizhang;


/**
 * Created by ruijie on 2017/10/24.
 */
class Element{
    private String ident;

     Element(String id){
        this.ident = id;
    }

    @Override
    public String toString() {
            return  ident;
    }

    @Override
    public int hashCode() {
        return ident.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj  instanceof Element &&   ident.equals(((Element) obj).ident);
    }
}

class Key extends Element{

    Key(String id) {
        super(id);
    }
}

class Value extends Element{

    Value(String id) {
        super(id);
    }
}

public class CanonicalMapping {

    public static void main(String[] args) {
            int size = 1000;
            if (args.length > 0){

            }
    }

}
