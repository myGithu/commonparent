package com.yao.thkinginjava.dishiqizhang;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by ruijie on 2017/10/21.
 */
public class Utilities {
    static List<String> list = Arrays.asList(
            "A B C D E F G H Z".split(" ")
    );

    public static void main(String[] args) {
        System.out.println(list);
        System.out.println(Collections.singleton("Four"));
        System.out.println("'list' disjoint(Four) ? :" + Collections.disjoint(list,Collections.singleton("Four")));
        System.out.println("max:" + Collections.max(list));
        System.out.println("min:" + Collections.min(list));

        System.out.println("max w/ comparator: " + Collections.max(list,String.CASE_INSENSITIVE_ORDER));
        System.out.println("min w/ comparator: " + Collections.min(list,String.CASE_INSENSITIVE_ORDER));

        List<String> subList = Arrays.asList("B C D".split(" "));
        System.out.println("indexOfSubList : " + Collections.indexOfSubList(list,subList));


    }


}
