package com.yao.thkinginjava.dishiqizhang.disanjie;

import java.util.LinkedList;

/**
 * Created by ruijie on 2017/10/19.
 */
public class Deque<T> {
    private LinkedList<T> deque = new LinkedList<T>();
    public void addFirst(T e){
        deque.addFirst(e);
    }

    public void addLast(T e){
        deque.addLast(e);
    }

    public T getFirst(){
        return deque.getFirst();
    }

    public T getLast(){
        return deque.getLast();
    }

    public T removeFirst(){
        return deque.removeFirst();
    }

    public T removeLast(){
        return deque.removeLast();
    }

    public int size(){
        return deque.size();
    }

    @Override
    public String toString() {
        return deque.toString();
    }
}
