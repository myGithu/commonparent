package com.yao.thkinginjava.dishiqizhang;

import java.util.Random;

/**
 * Created by ruijie on 2017/10/19.
 */
public class Prediction {
    private static Random rand = new Random(47);

    private boolean shadow = rand.nextDouble() > 0.5;

    @Override
    public String toString() {
        if (shadow){
            return "Six more weeks of Winter!";
        }
        return "Early Spring";
    }
}
