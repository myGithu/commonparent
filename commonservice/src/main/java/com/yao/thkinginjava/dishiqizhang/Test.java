package com.yao.thkinginjava.dishiqizhang;

/**
 * Created by ruijie on 2017/10/20.
 */
public abstract class Test<C> {
        String name;

        public Test(String name){
            this.name = name;
        }

        abstract int test(C container, TestParam tp);
}
