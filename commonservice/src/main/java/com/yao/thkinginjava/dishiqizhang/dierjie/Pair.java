package com.yao.thkinginjava.dishiqizhang.dierjie;

/**
 * Created by ruijie on 2017/10/18.
 */
public class Pair<K,V> {
    public final K key;

    public final V value;

    public Pair(K k, V v){
        this.key = k;
        this.value = v;
    }
}
