package com.yao.thkinginjava.dishiqizhang.dierjie;

import java.util.*;

/**
 * Created by ruijie on 2017/10/18.
 */
public class Countries {
    public static final String [][] DATA = {
            {"ALGERIA","Algiers"},{"ANGOLA","Luanda"},
            {"BENIN","Porto-Novo"},{"BOTSWANA","Gabernoe"},
            {"BURKINA FASO","Ouagadougou"},{"BURUNDI","Bujumbura"},
            {"Cameroon","Yaounde"},{"CAPE VERDE","Praia"},
    };

    private static class FlyweightMap extends AbstractMap<String,String>{

        @Override
        public Set<Map.Entry<String, String>> entrySet() {
            return null;
        }

        private static class Entry implements Map.Entry<String,String>{
            int index;

            Entry(int index){
                this.index = index;
            }

            @Override
            public boolean equals(Object obj) {
                return DATA[index][0].equals(obj);
            }

            @Override
            public String getKey() {
                return DATA[index][0];
            }

            @Override
            public String getValue() {
                return DATA[index][1];
            }

            @Override
            public String setValue(String value) {
                throw  new UnsupportedOperationException();
            }

            @Override
            public int hashCode() {
                return DATA[index][0].hashCode();
            }
        }

        static class EntrySet extends AbstractSet<Map.Entry<String,String>>{
            private int size;

            EntrySet(int size){
                if (size < 0){
                    this.size = 0;
                }else  if(size > DATA.length){
                    this.size = DATA.length;
                }else{
                    this.size = size;
                }
            }

            private class Iter implements  Iterator<Map.Entry<String,String>>{
                private Entry entry = new Entry(-1);
                @Override
                public boolean hasNext() {
                    return entry.index < size - 1;
                }

                @Override
                public Map.Entry<String, String> next() {
                    entry.index ++;
                    return entry;
                }

                @Override
                public void remove() {
                    throw  new UnsupportedOperationException();
                }

            }

            @Override
            public Iterator<Map.Entry<String, String>> iterator() {
                return new Iter();
            }

            @Override
            public int size() {
                return size;
            }
        }


    }

}
