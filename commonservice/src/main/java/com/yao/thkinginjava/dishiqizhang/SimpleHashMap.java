package com.yao.thkinginjava.dishiqizhang;

import java.util.*;

/**
 * Created by ruijie on 2017/10/20.
 */
public class SimpleHashMap<K,V> extends AbstractMap<K,V> {
    static final int SIZE = 997;

    LinkedList<MapEntry<K,V>>[] buckets = new LinkedList[SIZE];

    @Override
    public V put(K key, V value) {
        V oldValue = null;
        int index = Math.abs(key.hashCode());
        if(buckets[index] == null) {
            buckets[index] = new LinkedList<>();
        }

        LinkedList<MapEntry<K,V>> bucket = buckets[index];
        MapEntry<K,V> pair = new MapEntry<>(key,value);

        boolean  found = false;
        ListIterator<MapEntry<K,V>> it = bucket.listIterator();
        while (it.hasNext()){
            MapEntry<K,V> iPair = it.next();
            if (iPair.getKey().equals(key)){
                oldValue = iPair.getValue();
                it.set(pair);
                found = true;
                break;
            }
        }
        if (!found){
            buckets[index].add(pair);
        }
        return oldValue;
    }

    @Override
    public V get(Object key) {
        int index = Math.abs(key.hashCode());

        if(buckets[index] == null) {
            return null;
        }

        for (MapEntry<K, V> ipair : buckets[index]) {
            if (ipair.getKey().equals(key)){
                return ipair.getValue();
            }
        }
        return  null;

    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Map.Entry<K,V>> set = new HashSet<>();

        for (LinkedList<MapEntry<K, V>> bucket : buckets) {
            if (bucket == null)
                continue;
            for (MapEntry<K, V> mpair : bucket) {
                set.add(mpair);
            }
        }
        return set;
    }

    public static void main(String[] args) {
        SimpleHashMap<String,String> map = new SimpleHashMap<>();
        map.put("A","a");
        map.put("A","a");
        map.put("B","b");
        map.put("C","c");
        map.put("C","c");
        map.put("D","d");
        map.put("E","e");
        map.put("F","f");
        map.put("H","h");
        map.put("G","g");
        map.put("J","j");
        map.put("J","j");
        map.put("I","i");
        System.out.println(map);
        System.out.println(map.get("A"));
        System.out.println(map.entrySet());
    }
}
