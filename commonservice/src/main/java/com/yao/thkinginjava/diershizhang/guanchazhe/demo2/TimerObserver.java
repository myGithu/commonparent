package com.yao.thkinginjava.diershizhang.guanchazhe.demo2;

/**
 * Created by ruijie on 2017/11/2.
 */
public interface TimerObserver {
    /**
     * 主题对象只做一件事情，就是更新当前时间
     *
     * @param stamp
     */
    public void update(long stamp);
}
