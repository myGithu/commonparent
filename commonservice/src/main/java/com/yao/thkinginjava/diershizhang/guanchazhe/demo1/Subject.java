package com.yao.thkinginjava.diershizhang.guanchazhe.demo1;


import com.yao.thkinginjava.diershizhang.guanchazhe.demo1.Observer;

/**
 * Created by ruijie on 2017/11/2.
 */
public interface Subject {
    void registerObserver(Observer o);

    void removeObserver(Observer o);

    void notifyObservers();
}
