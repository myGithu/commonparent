package com.yao.thkinginjava.diershizhang.guanchazhe;

import com.yao.thkinginjava.diershizhang.guanchazhe.demo3.StudentObserver;
import com.yao.thkinginjava.diershizhang.guanchazhe.demo3.TeacherSubject;

/**
 * Created by ruijie on 2017/11/3.
 */
public class Test {
    public static void main(String[] args) {
        TeacherSubject subject = new TeacherSubject();
        StudentObserver zs = new StudentObserver("张三",subject);
        StudentObserver ls = new StudentObserver("李四",subject);
        StudentObserver ww = new StudentObserver("王五",subject);

        subject.setHomework("奥数题");
        subject.setHomework("英语题");
        subject.setHomework("语文题");
    }
}
