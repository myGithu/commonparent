package com.yao.thkinginjava.diershizhang.guanchazhe.demo3;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruijie on 2017/11/3.
 */
public class TeacherSubject implements Subject {
    private List<Observer> observers = new ArrayList<>();

    private String info;

    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        int i = observers.indexOf(o);
        if (i >= 0) {
            observers.remove(o);
        }
    }

    @Override
    public void notifyObserver() {
        if (observers == null || observers.size() == 0) {
            return;
        }

        for (int i = 0; i < observers.size(); i++) {
            Observer observer = observers.get(i);
            observer.update(info);
        }
    }

    public void setHomework(String info){
        this.info = info;
        System.out.println("今天的作业是" + info);
        notifyObserver();
    }

}
