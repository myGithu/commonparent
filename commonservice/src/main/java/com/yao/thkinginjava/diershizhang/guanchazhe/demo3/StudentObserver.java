package com.yao.thkinginjava.diershizhang.guanchazhe.demo3;

/**
 * Created by ruijie on 2017/11/3.
 */
public class StudentObserver implements Observer {
    /**
     * 保存一个Subject的引用,以后如果可以想取消订阅,
     * 有了这个引用会比较方便
     */
    private TeacherSubject subject;

    /**
     * 学生的姓名,用来标识不同的学生对象学生的姓名,
     * 用来标识不同的学生对象
     */
    private String name;


    public StudentObserver(String name, TeacherSubject subject){
        this.name = name;
        this.subject = subject;
        //每新建一个学生对象,默认添加到观察者的行列
        subject.addObserver(this);
    }

    @Override
    public void update(String info) {
        System.out.println(name + " 得到的作业 " + info);
    }

}
