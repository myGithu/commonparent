package com.yao.thkinginjava.diershizhang;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruijie on 2017/11/2.
 */
public class TableCreator {
    public static void main(String[] args) throws Exception {
        if(args.length < 1){
            System.out.println("arguments: annotated classes");
            System.exit(0);
        }

        for (String className : args) {
            Class<?> cl =  Class.forName(className);
            DBTable dbTable =  cl.getAnnotation(DBTable.class);
            if (dbTable == null){
                System.out.println("No DBTable annotations in class " + className);
                continue;
            }
            String tableName = dbTable.name();

            if (tableName.length() < 1){
                tableName = cl.getSimpleName().toUpperCase();
            }

            List<String>  columnDefs = new ArrayList<>();

            for (Field field : cl.getDeclaredFields()) {
                String colunmName = null;
                Annotation[] anns =  field.getAnnotations();
                if (anns.length < 1){
                    continue;
                }

                if(anns[0] instanceof  SQLInteger){
                    SQLInteger sInt = (SQLInteger) anns[0];
                    if (sInt.name().length() < 1){
                        colunmName = field.getName().toUpperCase();
                    }else{
                        className = sInt.name();
                    }
                    columnDefs.add(className);
                    columnDefs.add(" INT");
                    columnDefs.add(getConstraints(sInt.constraints()));
                }

                if(anns[0] instanceof  SQLString){
                    SQLString sString = (SQLString) anns[0];
                    if (sString.name().length() < 1){
                        colunmName = field.getName().toUpperCase();
                    }else{
                        colunmName = sString.name();
                    }
                }

            }
        }

    }

    private static String getConstraints(Constraints con){
        String constraints = "";
        if (!(con.allowNull())){
            constraints += " NOT NULL";
        }
        if (con.primaryKey()){
            constraints += " PRIMARY KEY";
        }
        if (con.unique()){
            constraints += " UNIQE";
        }
        return  constraints;
    }
}
