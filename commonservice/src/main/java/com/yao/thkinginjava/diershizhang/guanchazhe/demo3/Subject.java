package com.yao.thkinginjava.diershizhang.guanchazhe.demo3;

/**
 * Created by ruijie on 2017/11/3.
 * 主题（被观察者）
 */
public interface Subject {
    /**
     *  添加观察者
     * @param o
     */
    void addObserver(Observer o);

    /**
     * 移除观察者
     * @param o
     */
    void removeObserver(Observer o);

    /**
     * 当主题方法改变时,这个方法被调用,通知所有的观察者
     */
    void notifyObserver();

}
