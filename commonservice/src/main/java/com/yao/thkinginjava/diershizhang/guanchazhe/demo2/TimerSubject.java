package com.yao.thkinginjava.diershizhang.guanchazhe.demo2;

import com.yao.thkinginjava.diershizhang.guanchazhe.demo2.TimerObserver;

/**
 * Created by ruijie on 2017/11/2.
 */
public interface TimerSubject {
    /**
     * 为新的观察者实现注册服务
     *
     * @param o
     *          观察者
     */
    void registerObserver(TimerObserver o);

    /**
     * 移除某一个观察者对象
     *
     * @param o
     *          观察者
     */
    void removeObserver(TimerObserver o);

    void notifyObservers();


}
