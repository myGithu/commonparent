package com.yao.thkinginjava.diershizhang.guanchazhe.demo1;

/**
 * Created by ruijie on 2017/11/2.
 */
public interface Observer {
    void update (float temperature, float humidity, float pressure);
}
