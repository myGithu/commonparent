package com.yao.thkinginjava.diershizhang.guanchazhe.demo2;

/**
 * Created by ruijie on 2017/11/3.
 */
public class CountDownTimer implements TimerObserver,TimerDisplayable {
    private String mName;

    private long mStartStamp;

    private long mCountdownStamp;

    private long mCurrentStamp = 0L;

    public CountDownTimer(String name, long countdown){
            this.mStartStamp  = System.currentTimeMillis();
            this.mName = name;
            this.mCountdownStamp = countdown;
    }

    @Override
    public void display() {
        if(mCurrentStamp - mStartStamp <= mCountdownStamp){
            System.out.println(getName() + " 还剩" + ((mCountdownStamp - (mCurrentStamp - mStartStamp)) / 1000) + " s");
        }
    }

    @Override
    public void update(long stamp) {
        mCurrentStamp = stamp;
        display();
    }

    public String getName() {
        return mName;
    }

    public boolean isDone(){
        return mCurrentStamp - mStartStamp >= mCountdownStamp;
    }
}
