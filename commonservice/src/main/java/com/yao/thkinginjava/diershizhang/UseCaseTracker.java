package com.yao.thkinginjava.diershizhang;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ruijie on 2017/11/1.
 */
public class UseCaseTracker {
    public static void trackUseCases(List<Integer> useCaese,Class<?> cl){
        for (Method method : cl.getDeclaredMethods()) {
            UseCase useCase =  method.getAnnotation(UseCase.class);
            if (useCaese != null){
                System.out.println("Found Use Case:" + useCase.id() + " " + useCase.description());
            }
        }
        for (Integer integer : useCaese) {
            System.out.println("Warning : Missing use case-" + integer);
        }
    }

    public static void main(String[] args) {
        List<Integer>  useCases = new ArrayList<>();
        Collections.addAll(useCases,47,48,49,50);
        trackUseCases(useCases,PasswordUtils.class);
    }
}
