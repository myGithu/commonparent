package com.yao.thkinginjava.diershizhang.guanchazhe.demo2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruijie on 2017/11/3.
 */
public class NagaTimer implements TimerSubject {
    private long mCurrentStamp = 0L;

    private List<TimerObserver> mObservers = null;

    public NagaTimer(){
        if (mObservers == null) {
            mObservers = new ArrayList<>();
        }
    }

    @Override
    public void registerObserver(TimerObserver o) {
        if (o != null) {
            mObservers.add(o);
        }
    }

    @Override
    public void removeObserver(TimerObserver o) {
        if (o == null){
            return;
        }
        mObservers.remove(o);
    }

    @Override
    public void notifyObservers() {
        if(mObservers == null || mObservers.size() == 0){
            return;
        }
        for (int i = 0; i < mObservers.size(); i++) {
              CountDownTimer countDownTimer = (CountDownTimer) mObservers.get(i);
                if (countDownTimer.isDone()){
                    removeObserver(mObservers.get(i));
                }else{
                    countDownTimer.update(mCurrentStamp);
                }
        }
    }

    private void measurementsChanged(){
        notifyObservers();
    }

    public void setMeasurements(long currentStamp){
        mCurrentStamp = currentStamp;
        measurementsChanged();
    }
}
