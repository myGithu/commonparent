package com.yao.thkinginjava.diershizhang.guanchazhe.demo3;

/**
 * Created by ruijie on 2017/11/3.
 * 观察者
 */
public interface Observer {

    //当主题（被观察者）改变时，会将一个String类型的字符串作为参数传入
    public void update(String info);
}
