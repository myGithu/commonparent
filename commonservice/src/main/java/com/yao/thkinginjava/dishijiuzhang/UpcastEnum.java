package com.yao.thkinginjava.dishijiuzhang;

/**
 * Created by ruijie on 2017/10/28.
 */
enum Search {
    HITHER, YON
}

public class UpcastEnum {
    public static void main(String[] args) {
        Search[] searches = Search.values();
        Enum e = Search.HITHER;

        for (Enum anEnum : e.getClass().getEnumConstants()) {
            System.out.println(anEnum);
        }
    }
}
