package com.yao.thkinginjava.dishijiuzhang;

/**
 * Created by ruijie on 2017/10/28.
 */
public enum  OzWitch {
   WEST("Miss Gulch,aka the Wicked Witch of the West",1),
    NORTH("Glinda, the Good Witch of the North",2),
    EAST("Wicked Withc of the East, wearer of the Ruby Sloppers, crushed by Dororthy's house",3),
    SOUTH("Good by inference,but missing ",4);


    private String description;
    private  int i;
    private OzWitch(String description,int i){
        this.description = description;
        this.i = i;
    }

    public String getDescription() {
        return description;
    }

    public int getI() {
        return i;
    }

    public static void main(String[] args) {
        for (OzWitch witch : OzWitch.values()) {
            System.out.println(witch + " : " + witch.getDescription() + " : " + witch.getI());
        }
    }
}
