package com.yao.thkinginjava.dishijiuzhang;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by ruijie on 2017/10/31.
 */
public enum  ConstantSpecificMehtod {
   DATE_TIME{
     String getInfo(){
        return DateFormat.getDateInstance().format(new Date());
     }
   },

   CLASSPATH{
     String getInfo(){
         return System.getenv("CLASSPATH");
     }
   },
    VERSION{
        String getInfo(){
            return System.getProperty("java.version");
        }
    };

    abstract String getInfo();

    public static void main(String[] args) {
        for (ConstantSpecificMehtod mehtod : values()) {
            System.out.println(mehtod.getInfo());
        }
    }
}

