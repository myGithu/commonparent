package com.yao.thkinginjava.dishijiuzhang.zerenlian;

/**
 * Created by ruijie on 2017/10/31.
 */
abstract class Handler{
    private Handler nextHandler;

    public Handler getNextHandler(){
        return this.nextHandler;
    }

    public void setNextHandler(Handler nextHandler){
        this.nextHandler = nextHandler;
    }

    public abstract void doHandler();

}


public class ConcreteHandler extends Handler {
    @Override
    public void doHandler() {
        if(getNextHandler() != null){
            System.out.println("还有责任链");
            getNextHandler().doHandler();
        }else{
            System.out.println("我自己处理" + toString());
        }
    }
}
