package com.yao.thkinginjava.dishijiuzhang;

/**
 * Created by ruijie on 2017/11/1.
 */
public enum RoShamBo2 implements Competitor<RoShamBo2>{
    PAPER(OutCome.DRAW, OutCome.LOSE, OutCome.WIN),
    SCISSORS(OutCome.WIN,OutCome.DRAW,OutCome.LOSE),
    ROCK(OutCome.LOSE, OutCome.WIN, OutCome.DRAW);

    private OutCome vPAPER,vSCISSORS,vROCK;

    RoShamBo2(OutCome paper,OutCome scissors, OutCome rock){
        this.vPAPER = paper;
        this.vSCISSORS = scissors;
        this.vROCK = rock;
    }

    public OutCome compete(RoShamBo2 it){
        switch (it){
            default:
            case PAPER: return vPAPER;
            case SCISSORS: return vSCISSORS;
            case ROCK: return vROCK;
        }
    }

    public static void main(String[] args) {
        RoShaBo.play(RoShamBo2.class,10);
    }

}
