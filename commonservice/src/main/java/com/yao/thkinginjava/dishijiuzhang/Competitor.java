package com.yao.thkinginjava.dishijiuzhang;

/**
 * Created by ruijie on 2017/11/1.
 */
public interface Competitor<T extends Competitor<T>> {
    OutCome compete(T competitor);
}
