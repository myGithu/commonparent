package com.yao.thkinginjava.dishijiuzhang.zerenlian;

/**
 * Created by ruijie on 2017/10/31.
 * 责任链模式是一种对象的行为模式。在责任链模式里，很多对象由每一个对象对其下家的引用而连接起来形成一条链。
 * 请求在这个链上传递，直到链上的某一个对象决定处理此请求。
 * 发出这个请求的客户端并不知道链上的哪一个对象最终处理这个请求，
 * 这使得系统可以在不影响客户端的情况下动态地重新组织和分配责任。
 *
 *
 * 在以下条件下可考虑使用Chain of Responsibility：
 *    1 有多个的对象可以处理一个请求，哪个对象处理该请求运行时刻自动确定。
 *    2 你想在不明确指定接受者的情况下，想过个对象中的一个提交一个请求。
 *    3 可处理一个请求的对象集合应该被动态指定。
 *
 *
 */

abstract class ConsumeHandler{
    private ConsumeHandler nextHandler;

    public ConsumeHandler getNextHandler() {
        return nextHandler;
    }

    public void setNextHandler(ConsumeHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    /** user申请人 free报销费用 */
    public abstract  void doHandler(String user,double free);

}

class ProjectHandler extends  ConsumeHandler{

    @Override
    public void doHandler(String user, double free) {
        if(free < 500){

            if(user.equals("lwx")){
                System.out.println("给予报销：" + free);
            }else{
                System.out.println("销售通不过");
            }

        }else{
            if(getNextHandler() != null){
                getNextHandler().doHandler(user,free);
            }
        }
    }
}

class DeptHandler extends ConsumeHandler{

    @Override
    public void doHandler(String user, double free) {
        if (free < 1000){
            if(user.equals("zy")){
                System.out.println("给予报销：" + free);
            }else{
                System.out.println("报销不通过");
            }
        }else{
            if(getNextHandler() != null){
                getNextHandler().doHandler(user,free);
            }
        }
    }
}


class GeneraHandler extends ConsumeHandler{

    @Override
    public void doHandler(String user, double free) {
            if(free >= 1000){
                if (user.equals("lwxzy")){
                    System.out.println("给予报销：" + free);
                }else{
                    System.out.println("报销不通过");
                }
            }else{
                if(getNextHandler() != null){
                    getNextHandler().doHandler(user,free);
                }
            }
    }
}

public class ChainOfResponsibility {

    public static void main(String[] args) {
        ProjectHandler projectHandler = new ProjectHandler();

        DeptHandler deptHandler = new DeptHandler();

        GeneraHandler generaHandler = new GeneraHandler();

        projectHandler.setNextHandler(deptHandler);
        deptHandler.setNextHandler(generaHandler);

        projectHandler.doHandler("lwx",450);
        projectHandler.doHandler("lwx",650);
        projectHandler.doHandler("zy",600);
        projectHandler.doHandler("zy",1500);
        projectHandler.doHandler("lwxzy",1500);
    }


}
