package com.yao.thkinginjava.dishijiuzhang;

/**
 * Created by ruijie on 2017/10/28.
 */
public enum  SpaceShip {
    SCOUT,CARGO,TRANSPORT,CRUISER,BATTLESHIP,MOTHERSIP;

    @Override
    public String toString() {
        String id = name();
        String lower = id.substring(1).toLowerCase();
        return  id.charAt(0) + lower;
    }

    public static void main(String[] args) {
        for (SpaceShip ship : values()) {
            System.out.println(ship);
        }
    }
}
