package com.yao.thkinginjava.dishijiuzhang;

import java.util.EnumSet;

/**
 * Created by ruijie on 2017/10/31.
 */
public class EnumSets {
    public static void main(String[] args) {
        EnumSet<AlarmPoints> points = EnumSet.noneOf(AlarmPoints.class);

        points.add(AlarmPoints.BATHROOM);
        System.out.println(points);

        points.addAll(EnumSet.of(AlarmPoints.STAIR1,AlarmPoints.STAIR2,AlarmPoints.KITCHEN));

        System.out.println(points);
    }


}
