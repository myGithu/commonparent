package com.yao.thkinginjava.dishijiuzhang;

import java.util.Random;

/**
 * Created by ruijie on 2017/10/28.
 */
public class Enums {
    private static Random rand = new Random(47);

    public static <T extends Enum<T>> T random(Class<T> ec){
        return random(ec.getEnumConstants());
    }

    public static <T> T random(T[] values){
        return values[rand.nextInt(values.length)];
    }
}
