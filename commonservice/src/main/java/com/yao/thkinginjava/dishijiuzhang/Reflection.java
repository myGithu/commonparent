package com.yao.thkinginjava.dishijiuzhang;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by ruijie on 2017/10/28.
 */
public class Reflection {
    public static Set<String> analyze(Class<?> enumClass){
        System.out.println("----- Analyzing " + enumClass + "-----");
        System.out.println("Interfaces:");

        for (Type type : enumClass.getGenericInterfaces()) {
            System.out.println(type);
        }

        System.out.println("Base:" + enumClass.getSuperclass());
        System.out.println("Methods: ");

        Set<String> methods = new TreeSet<>();

        for (Method method : enumClass.getMethods()) {
            methods.add(method.getName());
        }
        System.out.println(methods);
        return methods;
    }

    public static void main(String[] args) {
       // Set<String> exploreMethods = analyze();
    }
}



