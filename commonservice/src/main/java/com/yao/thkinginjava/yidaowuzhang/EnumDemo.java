package com.yao.thkinginjava.yidaowuzhang;

import java.util.Arrays;

/**
 * Created by ruijie on 2017/8/18.
 */
public class EnumDemo {
    private Spicness spicness;

    EnumDemo(Spicness spicness){
        this.spicness = spicness;
    }
    public void setSpicness(Spicness spicness) {
        this.spicness = spicness;

    }

    public void f() {
        switch (spicness) {
            case HOT:
                System.out.println(Arrays.toString(Spicness.values()));
                System.out.println("HOT");
                break;
            case NOT:
                System.out.println("NOT");
                break;
            case MILD:
                System.out.println("MILD");
                break;
            case FLAMING:
                System.out.println("FLAMING");
                break;
            default:
                System.out.println("default");
        }

    }

    public static void main(String[] args) {
        new EnumDemo(Spicness.HOT).f();

    }
}


enum Spicness {
    NOT, MILD, HOT, FLAMING,
}
