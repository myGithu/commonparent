package com.yao.thkinginjava.yidaowuzhang;

/**
 * Created by ruijie on 2017/8/11.
 */


class Book {
    boolean checkedOut = false;

    Book(boolean checkedOut) {
        this.checkedOut = checkedOut;
    }

    void checkedIn() {
        this.checkedOut = false;
    }

    @Override
    protected void finalize() throws Throwable {
        if (checkedOut) {
            super.finalize();
            System.out.println("Error : checkedOut");
        }
    }
}

public class FinalizeDemo {
    public static void main(String[] args) {
        Book b = new Book(true);
        Runtime.runFinalizersOnExit(true);//强制调用finalize方法 但是这个方法会让程序不安全
    }
}




