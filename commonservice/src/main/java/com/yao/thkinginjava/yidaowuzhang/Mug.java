package com.yao.thkinginjava.yidaowuzhang;

/**
 * Created by ruijie on 2017/8/16.
 */
public class Mug {
    Mug(int marker) {
        System.out.println("Mug(" + marker + ")");
    }

    void f(int marker) {
        System.out.println("f(" + marker + ")");
    }

    public static void main(String[] args) {
        new Mugs(44);
    }

}

class Mugs {
    Mug m1;
    Mug m2;

    {
        m1 = new Mug(1);
        m2 = new Mug(2);
    }

    Mugs(){
        System.out.println("Mugs()");
    }

    Mugs(int marker){
        System.out.println("Mugs(" + marker + ")");
    }

}
