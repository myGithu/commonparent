package com.yao.thkinginjava.yidaowuzhang;

/**
 * Created by ruijie on 2017/8/12.
 */
public class StaticInitDemo {
}

class Cup {
    Cup(int marker) {
        System.out.println("Cup(" + marker + ")");
    }

    void f(int marker) {
        System.out.println("f(" + marker + ")");
    }
}

class Cups {
    static Cup c1;
    static Cup c2;

    static {
        c2 = new Cup(2);
        c1 = new Cup(1);
    }

    Cups() {
        System.out.println("Cups()");
    }

}

class main{
    public static void main(String[] args) {
        Cups.c2.f(1);
    }
}

