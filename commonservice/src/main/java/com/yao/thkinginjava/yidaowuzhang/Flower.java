package com.yao.thkinginjava.yidaowuzhang;

/**
 * Created by ruijie on 2017/8/9.
 */
public class Flower {
        int   petalCount = 0;
        String s = "initial value";

        Flower(int petals){
            this.petalCount = petals;
        }

        Flower(String s){
            this.s = s;
        }
        Flower(String s,int petals){
            this(s);
            this.petalCount = petals;
        }
}
