package com.yao.thkinginjava.yidaowuzhang;


import static com.yao.utils.date.DateTimeUtil.*;

/**
 * Created by ruijie on 2017/8/9.
 */
public class ThisDemo {
    public static void main(String[] args) {
        System.out.println(getStringNowDateYMD());
        new Person().eat(new Apple());
    }
}

class Person {
    public void eat(Apple apple) {
        Apple apple1 = apple.getApple();
        System.out.println("123456");
    }
}

class Apple {
    Apple getApple() {
        return Peeler.peel(this);
    }
}

class Peeler {
    static Apple peel(Apple apple) {
        return apple;
    }
}


