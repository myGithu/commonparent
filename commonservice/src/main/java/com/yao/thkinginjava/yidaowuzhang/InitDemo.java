package com.yao.thkinginjava.yidaowuzhang;

/**
 * Created by ruijie on 2017/8/12.
 */
public class InitDemo {
    private int age;
    private double a;
    private boolean state;

    public void print() {
        System.out.println("age:" + age);
        System.out.println("a:" + a);
        System.out.println("state:" + state);

    }

    public static void main(String[] args) {
        InitDemo i = new InitDemo();
        i.print();
    }
}
