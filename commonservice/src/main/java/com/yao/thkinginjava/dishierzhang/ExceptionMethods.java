package com.yao.thkinginjava.dishierzhang;

/**
 * Created by ruijie on 2017/9/26.
 */
public class ExceptionMethods {
    public static void main(String[] args) {
        try {
            throw  new Exception("My Exception");
        } catch (Exception e) {
            System.out.println("Caught Exception");
            System.out.println("getMessage()" + e.getMessage());
            e.printStackTrace(System.out);
        }
    }
}
