package com.yao.thkinginjava.dishierzhang;

/**
 * Created by ruijie on 2017/9/27.
 */

class DynamicFieldsException extends  Exception{}

public class DynamicFields {
    private Object[][] fields;

    public DynamicFields(int initialSize){
        fields = new Object[initialSize][2];
        for (int i = 0; i < initialSize; i++) {
            fields[i] = new Object[]{null,null};
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Object[] field : fields) {
            sb.append(field[0]);
            sb.append(": ");
            sb.append(field[1]);
            sb.append("\n");

        }
        return sb.toString();
    }
    private int hasField(String id){
        for (int i = 0; i < fields.length; i++) {
            if(id.equals(fields[i][0])){
                return i;
            }
        }
        return -1;
    }

    private int getFieldNumber(String id) throws NoSuchFieldException {
        int fieldNum = hasField(id);
        if(fieldNum == -1){
            throw  new NoSuchFieldException();
        }
        return fieldNum;
    }
   
    private int makeField(String id){
        for (int i = 0; i < fields.length; i++) {
            if(fields[i][0] == null){
                fields[i][0] = id;
                return  i;
            }
        }
        Object[][] tmp = new Object[fields.length + 1][2];
        for (int i = 0; i < fields.length; i++) {
            tmp[i] = fields[i];
        }
        for (int i = 0; i < fields.length; i++) {
            tmp[i] = new Object[]{null,null};
        }
        fields = tmp;
        return makeField(id);
    }

    public Object getField(String id) throws NoSuchFieldException {
        return fields[getFieldNumber(id)][1];
    }

    public Object setFields(String id,Object value) throws DynamicFieldsException {
        if(value == null){
            DynamicFieldsException dfe = new DynamicFieldsException();
            dfe.initCause(new NullPointerException());
            throw  dfe;
        }

        int fieldNumber = hasField(id);
        if(fieldNumber == -1){
            fieldNumber = makeField(id);
        }
        Object result = null;
        try {
            result =  getField(id);
        } catch (NoSuchFieldException e) {
            //e.printStackTrace();
            throw  new RuntimeException(e);
        }
        fields[fieldNumber][1] = value;
        return result;
    }

    public static void main(String[] args) {
        DynamicFields df = new DynamicFields(3);
        System.out.println(df);

        try {
            df.setFields("d","A value for d");
            df.setFields("number",47);
            df.setFields("number2",48);
            System.out.println(df);
        } catch (DynamicFieldsException e) {
            e.printStackTrace();
        }
    }

}
