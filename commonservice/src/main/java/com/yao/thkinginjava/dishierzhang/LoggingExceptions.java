package com.yao.thkinginjava.dishierzhang;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Logger;

class LoggingException extends  Exception{
    private static Logger logger = Logger.getLogger("LoggingException");

    public  LoggingException(){
        StringWriter stringWriter = new StringWriter();
        printStackTrace(new PrintWriter(stringWriter));
        logger.severe(stringWriter.toString());
    }
}

public class LoggingExceptions {
    public static void main(String[] args) {
        try {
            throw new LoggingException();
        } catch (LoggingException e) {
            System.err.println("Cayght" + e);
        }
    }
}
