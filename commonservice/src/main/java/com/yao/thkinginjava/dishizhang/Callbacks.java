package com.yao.thkinginjava.dishizhang;

/**
 * Created by ruijie on 2017/9/19.
 */
interface  Incrementable{
    void increment();
}

class Calleel implements  Incrementable{
    private int i = 0;
    @Override
    public void increment() {
        i ++;
        System.out.println(i);
    }
}

class MyIncrement{
    public void increment(){
        System.out.println("Other operation");
    }
    static void f(MyIncrement myIncrement){
        myIncrement.increment();
    }
}


class Callee2 extends  MyIncrement{
    private int i =  0;
    public void increment(){
        super.increment();
        i ++ ;
        System.out.println(i);
    }

    private class Clusure implements  Incrementable{

        @Override
        public void increment() {
            Callee2.this.increment();
        }


    }

    Incrementable getCallbackReference(){
        return  new Clusure();
    }
}

class Caller{
    private Incrementable callbackReference;
    Caller(Incrementable incrementable){
        this.callbackReference = incrementable;
    }

     void go(){
        callbackReference.increment();
     }
}

public class Callbacks {
    public static void main(String[] args) {
        Calleel calleel = new Calleel();
        Callee2 callee2 = new Callee2();
        MyIncrement.f(callee2);
        Caller Caller1 = new Caller(calleel);
        Caller Caller2 = new Caller(callee2.getCallbackReference());
        Caller1.go();
        Caller1.go();
        Caller2.go();

    }
}
