package com.yao.thkinginjava.dishizhang;

import static com.yao.thkinginjava.dishizhang.Parcel4.*;

/**
 * Created by ruijie on 2017/9/15.
 */
class Parcel4{
    private class PContents implements  Contents{
        private  int i;

        private PContents(int i){
            this.i = i;
        }

        @Override
        public int value() {
            return i;
        }
    }

    protected class PDestination implements  Destination{

        private String label;

        PDestination(){

        }

        PDestination(String whereTo){
            this.label = whereTo;
        }


        @Override
        public String readLabel() {
            return this.label;
        }
    }

    public Contents contents(int value){
        return  new PContents(value);
    }

    public Destination destination(String whereTo){
        return  new PDestination(whereTo);
    }
}

public class TestParcel{

    public static void main(String[] args) {
        Parcel4 parcel4 = new Parcel4();
        Contents contents = parcel4.contents(20);
        Destination destination =  parcel4.destination("山哈");
        System.out.println(contents.value());
        System.out.println(destination.readLabel());
    }

}
