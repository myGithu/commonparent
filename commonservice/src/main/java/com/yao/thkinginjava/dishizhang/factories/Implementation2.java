package com.yao.thkinginjava.dishizhang.factories;

/**
 * Created by ruijie on 2017/9/16.
 */
public class Implementation2 implements Service {
    private  Implementation2(){};

    public static SerivceFactory factory = new SerivceFactory() {
        @Override
        public Service getService() {
            return new Implementation2();
        }
    };


    @Override
    public void method1() {
        System.out.println("Implementation2.method1");
    }

    @Override
    public void method2() {
        System.out.println("Implementation2.method2");
    }
}
