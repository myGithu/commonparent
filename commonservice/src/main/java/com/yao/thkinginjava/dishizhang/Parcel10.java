package com.yao.thkinginjava.dishizhang;

/**
 * Created by ruijie on 2017/9/16.
 */
public class Parcel10 {
    public Destination destination(String dest, float price){
            return  new Destination() {
                private int cost;
                {
                    cost = Math.round(price);
                    if(cost > 100){
                        System.out.println(" Over budget");
                    }
                }

                private String lable = dest;
                @Override
                public String readLabel() {
                    return lable;
                }
            };
    }

    public static void main(String[] args) {
        Parcel10 parcesl10 = new Parcel10();
        Destination destination = parcesl10.destination("湖南",101.02f);
    }
}
