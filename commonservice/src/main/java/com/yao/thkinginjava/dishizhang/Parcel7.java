package com.yao.thkinginjava.dishizhang;

/**
 * Created by ruijie on 2017/9/16.
 */
public class Parcel7 {
    public Contents contents(){
        return new Contents() {
            private int i = 11;

            @Override
            public int value() {
                return i;
            }
        };
    }

    public static void main(String[] args) {
        Parcel7 parcel7 = new Parcel7();
        System.out.println(parcel7.contents().value());

    }
}
