package com.yao.thkinginjava.dishizhang.factories;




/**
 * Created by ruijie on 2017/9/16.
 */
public class Implementation1 implements  Service {
    private Implementation1(){}

    public static SerivceFactory factory = new SerivceFactory() {
        @Override
        public Service getService() {
            return new Implementation1();
        }
    };

    @Override
    public void method1() {
        System.out.println("Implementation1.method1");
    }

    @Override
    public void method2() {
        System.out.println("Implementation1.method2");
    }
}
