package com.yao.thkinginjava.dishizhang;

/**
 * Created by ruijie on 2017/9/15.
 */
public class Parcel1 {
    class Contents{
        private int i = 11;
        public int value(){
            return  i;
        }
    }

    class  Destination{
        private String lable;
        Destination(String whereTo){
            this.lable = whereTo;
        }
        String readLable(){
            return lable;
        }
    }

    public Destination to(String s){
        return  new Destination(s);
    }

    public Contents contents(){
        return  new Contents();
    }

    public void ship(String dest){
        Contents contents = new Contents();
        Destination destination = new Destination(dest);
        System.out.println(destination.readLable());
        System.out.println(contents.value());
    }

    public static void main(String[] args) {
        Parcel1 parcel1 = new Parcel1();
        parcel1.ship("Tasmanis");
        Parcel1 parcel11 = new Parcel1();
        Parcel1.Contents contents = parcel11.contents();
        Parcel1.Destination destination = parcel11.to("Borneo");
        destination.readLable();
        contents.value();

    }

}
