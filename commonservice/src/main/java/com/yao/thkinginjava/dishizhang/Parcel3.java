package com.yao.thkinginjava.dishizhang;

/**
 * Created by ruijie on 2017/9/15.
 */
public class Parcel3 {
    class Contents{
        private int i = 11;
        public int value(){
            return i;
        }
    }

    class Destination{
        private String label;
        Destination(String whereTo){
            this.label = whereTo;
        }

        String readLable(){
            return label;
        }
    }

    public static void main(String[] args) {
        Parcel3 parcel3 = new Parcel3();
        Parcel3.Contents contents = parcel3.new Contents();
        Parcel3.Destination destination = parcel3.new Destination("Tasmain");
        System.out.println(contents.value());
        System.out.println(destination.readLable());

    }

}
