package com.yao.thkinginjava.dishizhang;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruijie on 2017/9/19.
 */
public class Controller {
    private List<Event> eventList = new ArrayList<Event>();

    public void addEvent(Event event){
        eventList.add(event);
    }
    
    public void run(){
        while (eventList.size() > 0){
            for (Event event : new ArrayList<Event>(eventList)) {
                System.out.println(event);
                event.action();
                System.out.println(event);
            }
        }
    }
}
