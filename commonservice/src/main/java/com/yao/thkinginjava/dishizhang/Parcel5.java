package com.yao.thkinginjava.dishizhang;

/**
 * Created by ruijie on 2017/9/16.
 */
public class Parcel5 {
    public Destination destination(String s){
        class PDestination implements Destination{
            private String label;

            private PDestination(String whereTo){
                this.label = whereTo;
            }

            @Override
            public String readLabel() {
                return label;
            }
        }
        return  new PDestination(s);
    }

    public static void main(String[] args) {
        Parcel5 parcel5 = new Parcel5();
        Destination destination = parcel5.destination("山海");
        System.out.println(destination.readLabel());
    }
}
