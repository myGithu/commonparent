package com.yao.thkinginjava.dishizhang;

/**
 * Created by ruijie on 2017/9/19.
 */
public abstract class Event{
    private long evenTime;
    protected  final long delayTime;

    public Event(long delayTime){
        this.delayTime = delayTime;
    }

    public void start(){
        evenTime = System.nanoTime() + delayTime;
    }

    public boolean ready(){
        return System.nanoTime() >= evenTime;
    }

    public abstract  void action();
}
