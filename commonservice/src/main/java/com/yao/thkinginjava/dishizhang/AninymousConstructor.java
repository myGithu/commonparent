package com.yao.thkinginjava.dishizhang;

import com.sun.tools.doclets.formats.html.SourceToHTMLConverter;

/**
 * Created by ruijie on 2017/9/16.
 */

abstract  class Base{
    public  Base(int i){
        System.out.println("Base constructor.i = " + i);
    }
    public abstract void f();
}

public class AninymousConstructor {
        public static Base getBase(int i){
            return new Base(i) {
                {
                    System.out.println("Inside instance initializer");
                }
                @Override
                public void f() {
                    System.out.println("In anonymous f()");
                }
            };
        }


    public static void main(String[] args) {
        Base base = getBase(10);
        base.f();
    }
}
