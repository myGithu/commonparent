package com.yao.thkinginjava.dishizhang;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ruijie on 2017/9/15.
 */
interface Selector{
    boolean end();
    Object current();
    void next();
    Object outer();
}


public class Sequence {
    private Object[] items;
    private int next = 0;
    public Sequence(int size){
        this.items = new Object[size];
    }


    public void show(){
        System.out.println("test");
    }
    public void add(Object o){
        if(next < items.length){
            items[next++] = o;
        }
    }

    private class SequenceSelector implements  Selector{
        private int i = 0;


        @Override
        public boolean end() {
            return i == items.length;
        }

        @Override
        public Object current() {
            return items[i];
        }

        @Override
        public void next() {
            if(i < items.length){
                i ++ ;
            }
        }

        @Override
        public Sequence outer() {
            return Sequence.this;
        }


    }

    public Selector selector(){
        return new SequenceSelector();
    }


    public static void main(String[] args) {
        Sequence sequence = new Sequence(10);
        for (int i = 0; i < 10; i++) {
            sequence.add(Integer.toString(i));
        }
        Selector selector = sequence.selector();
        while (!selector.end()){
            System.out.println(selector.current() + " ");
            selector.next();
        }
        Sequence sequence1 = (Sequence) selector.outer();
        sequence1.show();
    }
}
