package com.yao.thkinginjava.dishizhang;

/**
 * Created by y on 2017/9/18.
 */
public class Parcel11 {
    private static class ParcelContents implements  Contents{
        private int i = 11;
        @Override
        public int value() {
            return i;
        }
    }

    protected static class ParcelDestination implements Destination{
        private String label;

        private ParcelDestination(String whereTo){
            this.label =  whereTo;
        }
        @Override
        public String readLabel() {
            return label;
        }

        public static void f(){}
        static int x = 20;
        static  class AnotherLevel{
            public static void f(){

            }

            static int x = 10;
        }
    }


    public static Contents contents(){
        return  new ParcelContents();
    }

    public static Destination destination(String to){
        return new ParcelDestination(to);
    }

    public static void main(String[] args) {
        Contents contents =  new ParcelContents();
        System.out.println(contents.value());
      /*  Contents contents = contents();
        Destination destination = destination("伤害 ");
        System.out.println(contents.value());
        System.out.println(destination.readLabel());*/
    }
}
