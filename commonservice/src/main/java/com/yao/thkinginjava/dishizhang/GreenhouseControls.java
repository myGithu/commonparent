package com.yao.thkinginjava.dishizhang;

/**
 * Created by ruijie on 2017/9/19.
 */
public class GreenhouseControls extends Controller{
    private boolean light = false;
    public class LightOn extends  Event{

        public LightOn(long dalayTime){
            super(dalayTime);
        }
        @Override
        public void action() {
            light = true;
        }

        public String toString(){
            return "Light is on";
        }
    }

    public class LightOff extends  Event{
        public LightOff(long delayTime){
            super(delayTime);
        }
        @Override
        public void action() {
                light = false;
        }

        @Override
        public String toString() {
            return "Light is off";
        }
    }

    private boolean water = false;
    public class WaterOn extends Event{
        public WaterOn(long delayTime){
            super(delayTime);
        }
        @Override
        public void action() {
            water = true;
        }

        public String toString(){
            return  "Greenhouse water is on";
        }
    }

    public class WaterOff extends Event{

        public WaterOff(long delayTime){
            super(delayTime);
        }

        @Override
        public void action() {
                water = false;
        }

        @Override
        public String toString() {
            return "Greenhouse water is off";
        }
    }

    private String thermostat = "Day";

    public class ThermostatNinght extends  Event{

        public ThermostatNinght(long delayTime){
            super(delayTime);
        }

        @Override
        public void action() {
            thermostat = "Night";
        }

        @Override
        public String toString() {
            return "Thermostat on night setting";
        }
    }

    public class ThermostatDay extends  Event{
        public ThermostatDay(long delayTime){
            super(delayTime);
        }

        @Override
        public void action() {
            thermostat = "Day";
        }

        @Override
        public String toString() {
            return "Thermostat on day setting";
        }
    }

    public class Bell extends Event{
        public Bell(long delayTime){
            super(delayTime);
        }

        @Override
        public void action() {

        }
    }

    public class Restart extends  Event{
        private Event[] eventList;

        public Restart(long delayTime ,Event[] eventList){
            super(delayTime);
            this.eventList = eventList;
            for (Event event : eventList) {
                addEvent(event);
            }
        }

        @Override
        public void action() {
            for (Event event : eventList) {
                event.start();
                addEvent(event);
            }
            start();
            addEvent(this);
        }

        public String toString(){
            return "Restarting system";
        }
    }

    public static class Terminate extends  Event{
        public Terminate(long delayTime){
            super(delayTime);
        }
        @Override
        public void action() {
          System.exit(0);
        }

        @Override
        public String toString() {
            return "Termianting";
        }
    }
}
