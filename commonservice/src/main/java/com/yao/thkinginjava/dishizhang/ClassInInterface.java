package com.yao.thkinginjava.dishizhang;

/**
 * Created by y on 2017/9/18.
 */
public interface ClassInInterface {
    ClassInInterface howdy();

    class Test implements  ClassInInterface{

        @Override
        public ClassInInterface howdy() {
            return new Test();
        }
    }
}
