package com.yao.thkinginjava.dishizhang;

/**
 * Created by ruijie on 2017/9/16.
 * java 8 不是final也行
 */
public class Parcel9 {
    public Destination destination(final String dest){
        return  new Destination() {
            private String lable = dest;
            @Override
            public String readLabel() {
                return lable;
            }
        };
    }

    public static void main(String[] args) {
        Parcel9 parcel9 = new Parcel9();
        System.out.println(parcel9.destination("湖南123").readLabel());
    }
}
