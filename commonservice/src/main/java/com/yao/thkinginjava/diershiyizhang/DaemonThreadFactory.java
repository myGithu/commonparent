package com.yao.thkinginjava.diershiyizhang;

import java.util.concurrent.ThreadFactory;

/**
 * Created by ruijie on 2017/11/4.
 */
public class DaemonThreadFactory implements ThreadFactory {
    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
        return t;
    }
}
