package com.yao.thkinginjava.diershiyizhang;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by ruijie on 2017/11/4.
 */
public class DaemoFromFactory implements  Runnable {
    @Override
    public void run() {
        while (true){
            try {
                TimeUnit.MILLISECONDS.sleep(100);
                System.out.println(Thread.currentThread() + "  " + this);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ExecutorService exec =
                Executors.newCachedThreadPool(new com.yao.thkinginjava.diershiyizhang.DaemonThreadFactory());
        for (int i = 0; i < 10; i++) {
            exec.execute(new DaemoFromFactory());
        }

        System.out.println("All daemons started");
        TimeUnit.MILLISECONDS.sleep(500);
    }
}
