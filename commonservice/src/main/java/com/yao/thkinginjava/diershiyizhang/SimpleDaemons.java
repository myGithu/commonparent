package com.yao.thkinginjava.diershiyizhang;

import java.util.concurrent.TimeUnit;

/**
 * Created by ruijie on 2017/11/4.
 */
public class SimpleDaemons implements Runnable{

    @Override
    public void run() {
        while (true){
            try {
                TimeUnit.MILLISECONDS.sleep(100);
                System.out.println(Thread.currentThread() + " " + this);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 10; i++) {
           Thread daemo =  new Thread(new SimpleDaemons());
            daemo.setDaemon(true);
            daemo.start();
        }
        System.out.println("All daemons started ");
        TimeUnit.MILLISECONDS.sleep(175);
    }
}
