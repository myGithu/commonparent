package com.yao.thkinginjava.dishibazhang;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by ruijie on 2017/10/27.
 */
public class GetChannel {
    private static final int BSIZE = 1024;

    public static void main(String[] args) throws IOException {
        FileChannel fc = new FileOutputStream("D:/data.txt").getChannel();
        fc.write(ByteBuffer.wrap("D:/some.txt".getBytes()));
        fc.close();
    }
}
