package com.yao.thkinginjava.dishibazhang.zhuangshiqi;

/**
 * Created by ruijie on 2017/10/25.
 *
 * 定义被装饰者
 *
 */

public interface Human {
    void wearClothes();

    void walkToWhere();
}
