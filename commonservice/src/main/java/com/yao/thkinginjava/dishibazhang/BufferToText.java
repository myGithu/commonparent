package com.yao.thkinginjava.dishibazhang;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

/**
 * Created by ruijie on 2017/10/27.
 */
public class BufferToText {
    private static final int BSIZE = 1024;

    public static void main(String[] args) throws IOException {
        FileChannel fc = new FileOutputStream("e:/data.txt").getChannel();
        fc.write(ByteBuffer.wrap("some.txt".getBytes()));
        fc.close();
        fc = new FileInputStream("e:/data.txt").getChannel();
        ByteBuffer buff = ByteBuffer.allocate(BSIZE);
        fc.read(buff);
        buff.flip();
        System.out.println(buff.asCharBuffer());
        buff.rewind();
        String encodeing = System.getProperty("file.encoding");
        System.out.println("Decoded using " + encodeing + " : " + Charset.forName(encodeing).decode(buff));
    }
}
