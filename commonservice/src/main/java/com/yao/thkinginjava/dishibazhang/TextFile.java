package com.yao.thkinginjava.dishibazhang;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ruijie on 2017/10/26.
 */
public class TextFile extends ArrayList<String> {
    public static String read(String fileName) {
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader in = new BufferedReader(
                    new FileReader(
                            new File(fileName).getAbsoluteFile()
                    )
            );
            try {
                String s;
                while ((s = in.readLine()) != null) {
                    sb.append(s);
                    sb.append("\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                in.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return sb.toString();
    }

    public static void write(String fileName, String text) {
        try {
            PrintWriter out = new PrintWriter(new File(fileName).getAbsoluteFile());
            try {
                out.print(text);
            } finally {
                out.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public TextFile(String fileName,String splitter){
        super(Arrays.asList(read(fileName).split(splitter)));
        if(get(0).equals("")){
            remove(0);
        }
    }

    public TextFile(String fileName){
        this(fileName, "\n");
    }

    public void write(String fileName){
        try {
            PrintWriter out = new PrintWriter(new File(fileName).getAbsoluteFile());
            try {
                for (String text : this) {
                    out.print(text);
                }
            } finally {
                out.close();
            }
        } catch (IOException e) {
            throw  new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        String file = read("C:/Users/ruijie/Desktop/ShowMethods.java");
        System.out.println(file);
        write("d:/test.txt",file);
    }

}
