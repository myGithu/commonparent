package com.yao.thkinginjava.dishibazhang;

import java.io.*;

/**
 * Created by ruijie on 2017/10/26.
 */
public class StoringAndRecoveringData {
    public static void main(String[] args) throws IOException {
        DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("D:/data.txt")));
        out.writeDouble(3.1415926);
        out.writeUTF("That was pi");
        out.writeDouble(1.41413);
        out.writeUTF("Square root of 2");
        out.close();

        DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream("D:/data.txt")));
        System.out.println(in.readDouble());
    }
}
