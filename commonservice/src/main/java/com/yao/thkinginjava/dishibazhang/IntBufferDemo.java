package com.yao.thkinginjava.dishibazhang;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

/**
 * Created by ruijie on 2017/10/27.
 */
public class IntBufferDemo {
    private static final int BSIZE = 1024;

    public static void main(String[] args) {
        ByteBuffer bb = ByteBuffer.allocate(BSIZE);

        IntBuffer ib = bb.asIntBuffer();

        ib.put(new int[]{11,42,47,99,143,81,1016});
        System.out.println(ib.get(3));
        ib.put(3,10000);
        System.out.println(ib.get(3));
        ib.flip();

        while (ib.hasRemaining()){
            int i = ib.get();
            System.out.println(i);
        }
    }
}
