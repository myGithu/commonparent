package com.yao.thkinginjava.dishibazhang.zhuangshiqi;

/**
 * Created by ruijie on 2017/10/25.
 *
 * 定义装饰者
 *
 */
public abstract class Decorator implements Human {
    private Human human;

    public Decorator(Human human){
        this.human = human;
    }

    @Override
    public void wearClothes() {
        human.wearClothes();
    }

    @Override
    public void walkToWhere() {
        human.walkToWhere();
    }
}
