package com.yao.thkinginjava.dishibazhang;

import java.io.PrintWriter;

/**
 * Created by ruijie on 2017/10/27.
 */
public class ChangeSystemOut {
    public static void main(String[] args) {
        PrintWriter out = new PrintWriter(System.out,true);
        out.println("Hello world");
        out.close();
    }
}
