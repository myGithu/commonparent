package com.yao.thkinginjava.dishibazhang;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by ruijie on 2017/10/26.
 */
public class BufferedInputFile {
    public static String read(File fileName) throws IOException {
        BufferedReader in  = new BufferedReader(new FileReader(fileName));
        String s;
        StringBuilder result = new StringBuilder();

        while (((s = in.readLine()) != null)){
                result.append(s);
                result.append("\n");
        }
        in.close();
        return result.toString();
    }

    public static  String read(String fileName) throws IOException {
        return  read(new File(fileName));
    }


    public static void main(String[] args) {
        try {
            System.out.println(read("C:/Users/ruijie/Desktop/ShowMethods.java"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
