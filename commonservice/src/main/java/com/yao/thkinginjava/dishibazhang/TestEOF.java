package com.yao.thkinginjava.dishibazhang;

import java.io.*;

/**
 * Created by ruijie on 2017/10/26.
 */
public class TestEOF {
    public static void main(String[] args) {
        DataInputStream in = null;
        try {
            in = new DataInputStream(new BufferedInputStream(new FileInputStream("C:/Users/ruijie/Desktop/ShowMethods.java")));
            while (in.available() != 0) {
                System.out.println((char) in.readByte());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
