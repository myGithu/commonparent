package com.yao.thkinginjava.dishibazhang;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by ruijie on 2017/10/27.
 */
public class Echo {
    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String s;
        while (((s = in.readLine()) != null) && (s.length() != 0)){
            System.out.println(s);
        }
        in.close();
    }
}
