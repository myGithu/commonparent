package com.yao.thkinginjava.dishibazhang;

import java.io.IOException;
import java.io.StringReader;

/**
 * Created by ruijie on 2017/10/26.
 */
public class MemoryInput {
    public static void main(String[] args) {
        try {
            StringReader in = new StringReader(BufferedInputFile.read("C:/Users/ruijie/Desktop/ShowMethods.java"));
            int c;
            while ((c = in.read()) != -1){
                System.out.println((char) c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
