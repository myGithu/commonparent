package com.yao.lottery;

/**
 * Created by ruijie on 2017/5/16.
 * 抽奖商品基本类
 */
public class Awrad {
    private String id;//商品编号
    private String name;//商品名字
    private float probability;//抽奖概率
    private int count;//商品数量
    private int index;//返回的奖品下标

    public Awrad(String id, float probability, int count) {
        this.id = id;
        this.probability = probability;
        this.count = count;
    }

    public Awrad(String id, float probability, int count, int index) {
        this.id = id;
        this.probability = probability;
        this.count = count;
        this.index = index;
    }

    public Awrad(String id, String name, float probability, int count, int index) {
        this.id = id;
        this.name = name;
        this.probability = probability;
        this.count = count;
        this.index = index;
    }

    public Awrad() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getProbability() {

        return probability;
    }

    public void setProbability(float probability) {
        this.probability = probability;
    }

    public int getCount() {

        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Awrad{" +
                "id='" + id + '\'' +
                ", probability=" + probability +
                ", count=" + count +
                '}';
    }
}
