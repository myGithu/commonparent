package com.yao.lottery;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

/**
 * Created by ruijie on 2017/5/16.
 * 抽奖概率算法
 */
public class AwardLottery {
    Logger logger = Logger.getLogger(AwardLottery.class.getName());
    public static Awrad lottery(List<Awrad> awrads) {
        float totalPro = 0f;//总的概率区间
        List<Float> proSection = new ArrayList<Float>();
        //初始化list
        proSection.add(0f);
        //遍历所有的奖品，设置概率区间，总的概率为每个概率区间的总和
        for (Awrad awrad : awrads) {
            totalPro += awrad.getProbability()  * 10 * awrad.getCount();
            proSection.add(totalPro);
        }
        //获取总概率区间的随机数
        Random random = new Random();
        float randomPro = (float)random.nextInt((int) totalPro);
       // System.out.println( "随机数：" + randomPro);
        //判断随机数在哪个奖品的概率区间
        for (int i = 0, size = proSection.size(); i < size; i++) {
            if (randomPro >= proSection.get(i) && randomPro < proSection.get(i + 1)){
                return  awrads.get(i);
            }
        }
        return  null;
    }

    public static void main(String[] args) {
        List<Awrad> awards = new ArrayList<Awrad>();
        Awrad awrad = null;
        awards.add(new Awrad("a1",0.001f,10,0));
        awards.add(new Awrad("a2",0.15f,50,1));
        awards.add(new Awrad("a3",0.3f,100,2));
        awards.add(new Awrad("a4",0.08f,30,3));
        awards.add(new Awrad("a5",0.1f,40,4));
        awards.add(new Awrad("a6",0.08f,30,5));
        awards.add(new Awrad("a7",1.0f,20,6));
        awards.add(new Awrad("谢谢参与",3.0f,0,7));
        for (int i = 0; i < 10; i++) {
            awrad =  AwardLottery.lottery(awards);
            System.out.println("恭喜您，抽到了：" + awrad.getIndex());
            System.out.println("恭喜您，抽到了：" + awrad.getId());
            System.out.println("----------------------------------------");
        }


    }
}


























