/**
 * Created by ruijie on 2017/6/1.
 */

/*服务器地址*/
const SERVER_URL = "http://www.shilesoft.com/common-web";
//const SERVER_URL = "http://localhost/common-web";
/*根据地址栏url中的key获取value*/
function getParam(paramKey) {
    var reg = new RegExp("(^|\\?|&)"+ paramKey +"=([^&]*)(\\s|&|$)", "i");
    if (reg.test(location.href))
        return decodeURI(RegExp.$2.replace(/\+/g, " "));
    return "";
}
/**
 * 判断字符串或者数组是否为空
 * @param obj
 * @returns {boolean}
 */
function isEmpty(obj){
    if(obj instanceof  Array){
        if(obj.length <= 0 || null == obj){
            return true;
        }
    }
    if(obj instanceof String || typeof obj  == 'string'){
        if('' == obj || null == obj || undefined == obj){
            return true;
        }
    }
    return false;
}
Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}