<%@ page contentType="text/html;charset=utf-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script type="text/javascript" src="<c:url value="/static/js/jquery-3.2.1.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/static/js/public.js" />"> </script>
    <script type="text/javascript">
        $(function () {
            initLogin();
        });
        /*
        * 获得微信登录的Code
        *
        * */
        function  getTenLoginCode() {
            //获得微信code的接口
            var url = SERVER_URL + "login/getTenLoginCode";
            //该方法通过指定URL替换当前缓存在历史里（客户端）的项目，因此当使用replace方法之后，你不能通过“前进”和“后退”来访问已经被替换的URL。
            parent.location.replace(url);
        }

        /**
         * 登录初始化
         */
        function initLogin() {
            //获得地址栏的state参数的value
            var state =  getParam("state");
            if(state){
                //获得地址栏的code参数的value
                var code = getParam("code");
                //微信登录
                TenLogin(code);
            }
        }

        /**
         *
         * 微信登录
         */
        function TenLogin(code) {
            
        }

    </script>
</head>
<body>
    <button id="login" onclick="getTenLoginCode();">登录</button>
</body>
</html>