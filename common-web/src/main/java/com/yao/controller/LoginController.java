package com.yao.controller;



import com.yao.service.tenlogin.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Created by ruijie on 2017/6/1.
 */
@Controller
public class LoginController {
        @Autowired
        private LoginService loginService;

        /**
         * 跳转登录页面
         * @return
         */
        @RequestMapping(method = RequestMethod.GET,value = "/login")
        public String toLogin(){
                return "login";
        }

        /**
         * 获取微信登录授权Code
         * @param response
         */
        @RequestMapping(method = RequestMethod.GET,value = "/getTenLoginCode")
        public void getTenLoginCode(HttpServletResponse response){
                try {
                        //重定向至微信授权接口
                        response.sendRedirect(loginService.getTenLoginCode());
                } catch (IOException e) {
                        e.printStackTrace();
                }
        }

        /**
         * 跳转至登录授权页面
         * @return
         */
        @RequestMapping(method = RequestMethod.GET,value = "/toAuthorize")
        public  String toAuthorize(){
                return "authorize";
        }

}
